# ontomap-wegovnow
This the OnToMap and OTM Logger project of the WeGovNow EU Project (Grant agreement No [693514](https://cordis.europa.eu/project/rcn/200261/factsheet/en)). 
To learn more about this project visit [http://ontomap.gitlab.io/wegovnow/](http://ontomap.gitlab.io/wegovnow/).
