import com.google.inject.AbstractModule;
import play.Logger;
import support.controllers.MappingTranslationController;

/**
 * This class is a play module that is executed at the startup of the application.
 * The {@link MappingTranslationController} is loaded and declared Singleton to cache a mapping between application properties
 * and OnToMap properties.
 * @see MappingTranslationController
 */


public class MappingModule extends AbstractModule {

    @Override
    protected void configure() {
        Logger.debug("Binding MappingModule...");
        bind(MappingTranslationController.class).asEagerSingleton();
    }
}
