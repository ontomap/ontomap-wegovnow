import com.google.inject.AbstractModule;
import play.Logger;
import support.controllers.ApplicationsController;

/**
 * This class is a play module that is executed at the startup of the application.
 * The {@link ApplicationsController} is loaded and declared Sigleton to retrieve the Application name of the partners from LiquidFeedBack server and
 * create a cache map to make faster all the requests.
 * @see ApplicationsController
 *
 */
public class ApplicationsModule extends AbstractModule {

    @Override
    protected void configure() {
        Logger.debug("Binding ApplicationsModule...");
        bind(ApplicationsController.class).asEagerSingleton();
    }
}
