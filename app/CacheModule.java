import com.google.inject.AbstractModule;
import play.Logger;
import support.controllers.ApplicationsController;
import support.controllers.CacheController;


/**
 * This class is a play module that is executed at the startup of the application.
 * The {@link CacheController} is loaded  and declared Sigleton to cache all the the feature from the TripleStore and make faster the
 * Instances API.
 * @see CacheController
 */
public class CacheModule extends AbstractModule {

    @Override
    protected void configure() {
        Logger.debug("Binding CacheModule...");
        bind(CacheController.class).asEagerSingleton();
    }
}
