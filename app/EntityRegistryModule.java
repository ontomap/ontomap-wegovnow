import com.google.inject.AbstractModule;
import play.Logger;
import support.controllers.CacheController;
import support.controllers.EntityRegistryController;


/**
 * This class is a play module that is executed at the startup of the application.
 * The {@link EntityRegistryController} is loaded and declared Sigleton to cache a map of association between a couples of <Application, Url of Feature>
 * @see EntityRegistryController
 */

public class EntityRegistryModule extends AbstractModule {

    @Override
    protected void configure() {
        Logger.debug("Binding EntityRegistryModule...");
        bind(EntityRegistryController.class).asEagerSingleton();
    }
}
