package support;

import com.fasterxml.jackson.databind.JsonNode;

import java.util.Comparator;

public class FeatureComparator implements Comparator<JsonNode> {
    @Override
    public int compare(JsonNode f1, JsonNode f2) {
        return f1.get("id").asText().compareTo(f2.get("id").asText());
    }
}
