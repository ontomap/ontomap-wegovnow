package support;

import com.esri.core.geometry.Geometry;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.util.Map;

public class ConceptResult {
    private ObjectNode featureCollection;
    private Map<String, Geometry> geometryMap;

    public ConceptResult(Map<String, Geometry> geometryMap, ObjectNode featureCollection) {
        this.geometryMap = geometryMap;
        this.featureCollection = featureCollection;
    }

    public ObjectNode getFeatureCollection() {
        return featureCollection;
    }

    public void setFeatureCollection(ObjectNode featureCollection) {
        this.featureCollection = featureCollection;
    }

    public Map<String, Geometry> getGeometryMap() {
        return geometryMap;
    }

    public void setGeometryMap(Map<String, Geometry> geometryMap) {
        this.geometryMap = geometryMap;
    }
}
