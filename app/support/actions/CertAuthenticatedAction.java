package support.actions;

import controllers.ResponseController;
import play.mvc.Http;
import play.mvc.Result;
import play.mvc.With;
import support.controllers.AuthenticationController;

import javax.inject.Inject;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

/**
 * This class implements a Play Action used to determine which API methods require certificate authentication.
 */
public class CertAuthenticatedAction extends play.mvc.Action.Simple {

    private AuthenticationController authenticationController;
    private ResponseController responseController;

    @Inject
    public CertAuthenticatedAction(AuthenticationController authenticationController, ResponseController responseController) {
        super();
        this.authenticationController = authenticationController;
        this.responseController = responseController;
    }

    @Override
    public CompletionStage<Result> call(Http.Context ctx) {
        String application = authenticationController.checkAuthentication(ctx.request());
        if (application == null) {
            return CompletableFuture.completedFuture(responseController.missingCertificate());
        }
        ctx.args.put("application", application);
        //If the application != null the authentication occurred through certificate so not by the token.
        ctx.args.put("tokenAuth", false);
        return delegate.call(ctx);
    }

    @With(CertAuthenticatedAction.class)
    @Target({ElementType.TYPE, ElementType.METHOD})
    @Retention(RetentionPolicy.RUNTIME)
    public @interface CertificateAuthentication {}
}
