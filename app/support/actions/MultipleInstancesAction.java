package support.actions;

import controllers.ResponseController;
import play.mvc.Http;
import play.mvc.Result;
import play.mvc.With;
import support.controllers.AuthenticationController;

import javax.inject.Inject;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

/**
 * This class implements a Play Action used to determine which API methods support multiple OTM instances.
 */
public class MultipleInstancesAction extends play.mvc.Action.Simple {

    private AuthenticationController authenticationController;
    private ResponseController responseController;

    @Inject
    public MultipleInstancesAction(AuthenticationController authenticationController, ResponseController responseController) {
        super();
        this.authenticationController = authenticationController;
        this.responseController = responseController;
    }

    @Override
    public CompletionStage<Result> call(Http.Context ctx) {
        String instance = authenticationController.getOTMInstance(ctx.request());
        if (instance == null) {
            return CompletableFuture.completedFuture(responseController.internalServerError2(ResponseController.NO_INSTANCE));
        }
        ctx.args.put("instance", instance);
        return delegate.call(ctx);
    }

    @With(MultipleInstancesAction.class)
    @Target({ElementType.TYPE, ElementType.METHOD})
    @Retention(RetentionPolicy.RUNTIME)
    public @interface MultipleInstances {}
}
