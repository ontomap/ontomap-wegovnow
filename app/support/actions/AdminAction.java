package support.actions;

import controllers.ResponseController;
import play.Configuration;
import play.Environment;
import play.mvc.Http;
import play.mvc.Result;
import play.mvc.With;
import support.controllers.AuthenticationController;

import javax.inject.Inject;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;


/**
 * This class implements a Play Action used to determine which API methods require admin privileges; application.conf contains
 * the list of admin-enabled certificates.
 */
public class AdminAction extends play.mvc.Action.Simple {

    private AuthenticationController authenticationController;
    private ResponseController responseController;
    private Configuration configuration;
    private Environment environment;

    @Inject
    public AdminAction(AuthenticationController authenticationController, ResponseController responseController,
                       Configuration configuration, Environment environment) {
        super();
        this.authenticationController = authenticationController;
        this.responseController = responseController;
        this.configuration = configuration;
        this.environment = environment;
    }


    @Override
    public CompletionStage<Result> call(Http.Context ctx) {
        String application = authenticationController.checkAuthentication(ctx.request());
        if (application == null) {
            return CompletableFuture.completedFuture(responseController.missingCertificate());
        }
        //Get The List of all Admin Application from application.conf file
        List<String> adminCommonNames = configuration.getStringList("ontomap.admin.cn");
        if (!environment.isDev() && !adminCommonNames.contains(application)) {
            return CompletableFuture.completedFuture(forbidden("Forbidden")); // TODO: message
        }
        ctx.args.put("application", application);
        return delegate.call(ctx);
    }

    @With(AdminAction.class)
    @Target({ElementType.TYPE, ElementType.METHOD})
    @Retention(RetentionPolicy.RUNTIME)
    public @interface AdminAuthentication {}
}
