package support.actions;

import controllers.ResponseController;
import joptsimple.internal.Strings;
import play.mvc.Http;
import play.mvc.Result;
import play.mvc.With;
import support.controllers.AccessTokenMongoController;
import support.controllers.AuthenticationController;

import javax.inject.Inject;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

/**
 * This class implements a Play Action used to determine which API methods support read-only tokend authentication, useful for
 * situations in which using a client certificate is not feasible.
 */
public class TokenAuthenticatedAction extends play.mvc.Action.Simple {

    private AuthenticationController authenticationController;
    private ResponseController responseController;
    private AccessTokenMongoController accessTokenMongoController;

    @Inject
    public TokenAuthenticatedAction(AuthenticationController authenticationController, ResponseController responseController,
                                    AccessTokenMongoController accessTokenMongoController) {
        super();
        this.authenticationController = authenticationController;
        this.responseController = responseController;
        this.accessTokenMongoController = accessTokenMongoController;
    }

    @Override
    public CompletionStage<Result> call(Http.Context ctx) {
        String application = authenticationController.checkAuthentication(ctx.request());
        if (application == null) {
            //If the application == null the certficate is not valid and the token is now checked
            String token = ctx.request().getQueryString("token");
            application = accessTokenMongoController.verifyToken(token);
            if (application == null)
                return CompletableFuture.completedFuture(responseController.noCertOrToken());
            //The token is valid and the flag is setted.
            ctx.args.put("tokenAuth", true);
        }else{
            //The certificate is valid so we don't need to check the token
            ctx.args.put("tokenAuth", false);
        }
        ctx.args.put("application", application);
        return delegate.call(ctx);
    }

    @With(TokenAuthenticatedAction.class)
    @Target({ElementType.TYPE, ElementType.METHOD})
    @Retention(RetentionPolicy.RUNTIME)
    public @interface TokenAuthentication {}
}
