package support.controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.kevinsawicki.http.HttpRequest;
import com.mongodb.MongoTimeoutException;
import com.mongodb.bulk.BulkWriteResult;
import com.mongodb.client.*;
import com.mongodb.client.model.*;
import org.bson.BsonDocument;
import org.bson.Document;
import play.Configuration;
import play.Logger;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * This class contains the internal methods used to manage the names linked to each application.
 */
@Singleton
public class ApplicationsController {
    private Configuration configuration;
    private MongoDBController mongoDBController;

    // ReadWriteLock used to grant write access to only one thread, the ApplicationsController is created as Singleton
    private ReadWriteLock lock;
    private Map<String, String> applicationsMap;

    @Inject
    public ApplicationsController(Configuration configuration, MongoDBController mongoDBController) {
        this.configuration = configuration;
        this.mongoDBController = mongoDBController;
        lock = new ReentrantReadWriteLock();
        applicationsMap = new HashMap<>();
        retrieveAppNames();
    }

    /**
     * Retrieves the application names from the URLs in application.conf and saves them in the DB and in memory, in order to grant fast access.
     */
    public void retrieveAppNames() {
        lock.writeLock().lock();
        initMap();
        List<String> urlList = configuration.getStringList("ontomap.appnames.domains");
        List<Document> docs = new ArrayList<>();
        Logger.info("Retrieving application names...");
        try {
            for (String url : urlList) {
                ObjectMapper om = new ObjectMapper();
                HttpRequest req = HttpRequest.get(url);
                Logger.info("{} {}: {}", req.method(), req.url().toString(), req.code()); // todo: error handling (only HTTP 200 responses should be accepted
                JsonNode res = om.readTree(req.body());
                for (JsonNode app : res.withArray("result")) {
                    Document doc = new Document();
                    doc.put("cert_common_name", app.get("cert_common_name").asText());
                    doc.put("name", app.get("name").asText());
                    docs.add(doc);
                    applicationsMap.put(app.get("cert_common_name").asText(), app.get("name").asText());
                }
            }
            saveOnDB(docs);
        }
        catch (MongoTimeoutException e) {
            Logger.error("MongoTimeoutException in retrieveAppNames (writing on DB):", e);
        }
        catch (HttpRequest.HttpRequestException e) {
            Logger.error("One or more endpoints unreachable, retrieving app names from DB");
            try {
                getFromDB();
            }
            catch (MongoTimeoutException e1) {
                Logger.error("MongoTimeoutException in retrieveAppNames (retrieving from DB):", e);
            }
        }
        catch (IOException e) {
            Logger.error("IOException in retrieveAppNames:", e);
        }
        finally {
            lock.writeLock().unlock();
        }
        Logger.debug(applicationsMap.toString());
    }

    /**
     * Retrieves the name of the specified application from memory. If no name is present, returns the domain name of the application.
     * @param cn The domain name of the application whose name is requested
     * @return The name of the specified application
     */
    public String getAppName(String cn) {
        lock.readLock().lock();
        String name = applicationsMap.getOrDefault(cn, cn);
        lock.readLock().unlock();
        return name;
    }

    /**
     * Re-initializes the data structure used to store the application names in memory.
     */
    private void initMap() {
        applicationsMap.clear();
        applicationsMap.put(configuration.getString("ontomap.application.domain"), "OnToMap");
    }

    /**
     * Saves a list of MongoDB documents containing CN-name entries on DB. If an entry with a specific CN already exists,
     * it is overwritten.
     * @param docs A list of MongoDB documents
     * @throws MongoTimeoutException If a problem occurred while connecting to the database
     */
    private void saveOnDB(List<Document> docs) throws MongoTimeoutException {
        MongoDatabase database = mongoDBController.getDatabase();
        MongoCollection<Document> collection = database.getCollection(configuration.getString("ontomap.logger.mongodb.applications.collection"));
        List<WriteModel<Document>> writeList = new ArrayList<>();
        for (Document d : docs) {
            writeList.add(new ReplaceOneModel<>(
                    Filters.eq("cert_common_name", d.getString("cert_common_name")),
                    d,
                    new ReplaceOptions().upsert(true)));
        }
        try(ClientSession dbSession = mongoDBController.getSession()) {
            dbSession.startTransaction();
            try {
                BulkWriteResult res = collection.bulkWrite(dbSession,writeList);
                dbSession.commitTransaction();
            }catch (Exception e){
                dbSession.abortTransaction();
            }finally {
                dbSession.close();
            }
        }
    }

    /**
     * Retrieves the list of CN-name associations from DB and stores them in memory.
     * @throws MongoTimeoutException If a problem occurred while connecting to the database
     */
    private void getFromDB() throws MongoTimeoutException {
        MongoDatabase database = mongoDBController.getDatabase();
        MongoCollection<BsonDocument> collection = database.getCollection(configuration.getString("ontomap.logger.mongodb.applications.collection"), BsonDocument.class);
        FindIterable<BsonDocument> findIterable = collection.find();
        initMap();
        try(MongoCursor<BsonDocument> cursor = findIterable.iterator()) {
            while(cursor.hasNext()) {
                BsonDocument doc = cursor.next();
                applicationsMap.put(doc.getString("cert_common_name").getValue(), doc.getString("name").getValue());
            }
        }
    }

}
