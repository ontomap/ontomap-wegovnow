package support.controllers;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.common.base.Joiner;
import com.google.common.base.Stopwatch;
import com.google.common.base.Strings;
import org.apache.jena.ontology.OntModel;
import org.apache.jena.ontology.OntModelSpec;
import org.apache.jena.query.*;
import org.apache.jena.rdf.model.*;
import org.apache.jena.reasoner.Reasoner;
import org.apache.jena.reasoner.ReasonerRegistry;
import play.Configuration;
import play.Environment;
import play.Logger;
import support.QueryHelper;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * This class contains the methods used for interacting with the ontology.
 */
@Singleton
public class OntologyController {

    public static final String RDF_TYPE = "http://www.w3.org/1999/02/22-rdf-syntax-ns#type";
    public static final String RDFS_LABEL = "http://www.w3.org/2000/01/rdf-schema#label";
    public static final String RDFS_COMMENT = "http://www.w3.org/2000/01/rdf-schema#comment";
    public static final String RDFS_DOMAIN = "http://www.w3.org/2000/01/rdf-schema#domain";
    public static final String RDFS_RANGE = "http://www.w3.org/2000/01/rdf-schema#range";
    public static final String RDFS_SUBCLASS = "http://www.w3.org/2000/01/rdf-schema#subClassOf";

    private Configuration configuration;
    private Environment environment;
    private QueryHelper queryHelper;
    private Lock lock;

    @Inject
    public OntologyController(Configuration configuration, Environment environment, QueryHelper queryHelper) throws FileNotFoundException {
        this.configuration = configuration;
        this.environment = environment;
        this.queryHelper = queryHelper;
        lock = new ReentrantLock();
        Stopwatch timer = Stopwatch.createUnstarted();
        boolean showTimers = configuration.getBoolean("ontomap.debug.showtimers");
        Logger.info("Reading ontology...");
        // when the application starts (and the class in instantiated) the ontology is read in order to cache it
        timer.start();
        getOntologyModel();
        timer.stop();
        if (showTimers) Logger.debug("Read ontology: {}", timer.toString());
        timer.reset();
    }

    /**
     * Reads the ontology from file
     * @return The OntModel representing the ontology
     * @throws FileNotFoundException If the ontology file could not be found
     */
    public OntModel getOntologyModel() throws FileNotFoundException {
        String baseURI = configuration.getString("ontomap.ontology.baseuri");
        OntModel ontologyModel = ModelFactory.createOntologyModel(OntModelSpec.OWL_DL_MEM);
        File ontologyFile = environment.getFile(configuration.getString("ontomap.ontology.path"));
        lock.lock();
        try {
            ontologyModel.read(new FileInputStream(ontologyFile), baseURI, null);
        }
        finally {
            lock.unlock();
        }
        return ontologyModel;
    }

    /**
     * Returns an ObjectNode containing all the information about a specific ontology concept. If lang != null, all the information
     * is returned in the language specified, if present
     * @param concept The concept to retrieve
     * @param lang If not null, the label/description of the concept are returned in the language selected, if they are available
     * @return The ObjectNode containing the information about the concept; null if the concept doesn't exist
     * @throws FileNotFoundException If the ontology file could not be found
     */
    public ObjectNode getOntologyConcept(String concept, String lang) throws FileNotFoundException {
        String baseURI = configuration.getString("ontomap.ontology.baseuri");
        OntModel ontologyModel = getOntologyModel();
        Resource conceptResource = ResourceFactory.createResource(baseURI + concept);

        // get all statements with the concept as subject
        StmtIterator resourceAsSubjectIterator = ontologyModel.listStatements(conceptResource, null, (RDFNode) null);
        if (!resourceAsSubjectIterator.hasNext()) return null;
        ObjectMapper om = new ObjectMapper();
        ObjectNode result = om.createObjectNode();
        result.put("uri", baseURI + concept);
        ArrayNode relations = result.putArray("relations");
        ObjectNode labelObject = om.createObjectNode();
        ObjectNode descriptionObject = om.createObjectNode();
        while (resourceAsSubjectIterator.hasNext()) {
            Statement statement = resourceAsSubjectIterator.nextStatement();
            if (statement.getPredicate().getURI().equals(RDFS_LABEL)) { // labels
//                Logger.debug("Label: {}",  statement.getObject().asLiteral().getString());
                if (Strings.isNullOrEmpty(lang)) {
                    labelObject.put(statement.getObject().asLiteral().getLanguage(), statement.getObject().asLiteral().getString());
                }
                else if (lang.equals(statement.getObject().asLiteral().getLanguage()))
                    result.put("label", statement.getObject().asLiteral().getString());
            }
            else if (statement.getPredicate().getURI().equals(RDFS_COMMENT)) { // comments
                if (Strings.isNullOrEmpty(lang)) {
                    descriptionObject.put(statement.getObject().asLiteral().getLanguage(), statement.getObject().asLiteral().getString());
                }
                else if (lang.equals(statement.getObject().asLiteral().getLanguage()))
                    result.put("description", statement.getObject().asLiteral().getString());
            }
            else if (!"SchemaThing".equals(concept) // subclass relations
                    && statement.getPredicate().getURI().equals(RDFS_SUBCLASS)
                    && statement.getObject().asResource().getURI().startsWith(baseURI)) {
                relations.add(
                        om.createObjectNode()
                        .put("subject", conceptResource.getURI())
                        .put("object", statement.getObject().asResource().getURI())
                        .put("predicate", "subClassOf")
                );
            }
            else if (statement.getPredicate().getURI().startsWith(baseURI) && statement.getObject().isResource()) { // other relations
                relations.add(
                        om.createObjectNode()
                                .put("subject", conceptResource.getURI())
                                .put("object", statement.getObject().asResource().getURI())
                                .put("predicate", statement.getPredicate().getURI().replace(baseURI, ""))
                );
            }
        }

        // get all statements with the concept as object
        StmtIterator resourceAsObjectIterator = getOntologyModel().listStatements(null, null, conceptResource);
        while (resourceAsObjectIterator.hasNext()) {
            Statement statement = resourceAsObjectIterator.nextStatement();
            if (statement.getPredicate().getURI().equals(RDFS_SUBCLASS)) { // subclass relations
                relations.add(
                        om.createObjectNode()
                                .put("subject", statement.getSubject().getURI())
                                .put("object", conceptResource.getURI())
                                .put("predicate", "subClassOf")
                );
            }
            else if (statement.getPredicate().getURI().startsWith(baseURI)) { // other relations
//                Logger.debug("{} {} {}", statement.getSubject().getURI(), statement.getPredicate().getURI(), statement.getObject().asResource().getURI());
                relations.add(
                        om.createObjectNode()
                                .put("subject", statement.getSubject().getURI())
                                .put("object", conceptResource.getURI())
                                .put("predicate", statement.getPredicate().getURI().replace(baseURI, ""))
                );
            }
        }
        // if no lang is specified, insert all languages
        if (Strings.isNullOrEmpty(lang)) {
            result.set("label", labelObject);
            result.set("description", descriptionObject);
        }
        return result;
    }

    /**
     * Returns an ObjectNode containing the information about all the concepts in the ontology
     * @param lang If not null, the label/description of the concept are returned in the language selected, if they are available
     * @param relations If true, the information about the relations between concepts is returned
     * @return
     * @throws FileNotFoundException
     */
    public ObjectNode getAllConcepts(String lang, boolean relations) throws FileNotFoundException {
        String baseURI = configuration.getString("ontomap.ontology.baseuri");
        OntModel ontologyModel = getOntologyModel();
        ObjectMapper om = new ObjectMapper();
        ObjectNode result = om.createObjectNode();
        ArrayNode conceptsArray = result.putArray("concepts");
        ArrayNode relationsArray = om.createArrayNode();
        if (relations) result.set("relations", relationsArray);

        Reasoner reasoner = ReasonerRegistry.getTransitiveReasoner();
        InfModel infModel = ModelFactory.createInfModel(reasoner, ontologyModel);

        Property subClassProperty = ResourceFactory.createProperty(RDFS_SUBCLASS);
        Resource schemaThingResource = ResourceFactory.createResource(baseURI + "SchemaThing");

        ResIterator resIterator = infModel.listResourcesWithProperty(subClassProperty, schemaThingResource);
        while (resIterator.hasNext()) {
            Resource resource = resIterator.nextResource();
            StmtIterator stmtIterator = ontologyModel.listStatements(resource, null, (RDFNode) null);
            ObjectNode conceptObject = om.createObjectNode();
            ObjectNode labelObject = om.createObjectNode();
            ObjectNode descriptionObject = om.createObjectNode();
            conceptObject.put("uri", resource.getURI());
            while (stmtIterator.hasNext()) {
                Statement statement = stmtIterator.nextStatement();
                if (statement.getPredicate().getURI().equals(RDFS_LABEL)) { // labels
                    if (Strings.isNullOrEmpty(lang)) {
                        labelObject.put(statement.getObject().asLiteral().getLanguage(), statement.getObject().asLiteral().getString());
                    }
                    else if (lang.equals(statement.getObject().asLiteral().getLanguage()))
                        conceptObject.put("label", statement.getObject().asLiteral().getString());
                }
                else if (statement.getPredicate().getURI().equals(RDFS_COMMENT)) { // comments
                    if (Strings.isNullOrEmpty(lang)) {
                        descriptionObject.put(statement.getObject().asLiteral().getLanguage(), statement.getObject().asLiteral().getString());
                    }
                    else if (lang.equals(statement.getObject().asLiteral().getLanguage()))
                        conceptObject.put("description", statement.getObject().asLiteral().getString());
                }
                else if (relations
                        && !statement.getSubject().getURI().equals(baseURI + "SchemaThing")
                        && statement.getPredicate().getURI().equals(RDFS_SUBCLASS)
                        && statement.getObject().asResource().getURI().startsWith(baseURI)) {
                    relationsArray.add(
                            om.createObjectNode()
                                    .put("subject", statement.getSubject().getURI())
                                    .put("object", statement.getObject().asResource().getURI())
                                    .put("predicate", "subClassOf")
                    );
                }
                else if (relations
                        && statement.getPredicate().getURI().startsWith(baseURI)
                        && statement.getObject().isResource()) {
                    relationsArray.add(
                            om.createObjectNode()
                                    .put("subject", statement.getSubject().getURI())
                                    .put("object", statement.getObject().asResource().getURI())
                                    .put("predicate", statement.getPredicate().getURI().replace(baseURI, ""))
                    );
                }
            }
            if (Strings.isNullOrEmpty(lang)) {
                conceptObject.set("label", labelObject);
                conceptObject.set("description", descriptionObject);
            }
            conceptsArray.add(conceptObject);
        }
        return result;
    }

    public Set<String> getSubConceptsSet(String concept) throws FileNotFoundException {
        String baseURI = configuration.getString("ontomap.ontology.baseuri");
        Set<String> concepts = new LinkedHashSet<>();
        String query = "SELECT ?class WHERE {?class rdfs:subClassOf* ?concept}";
        OntModel ontologyModel = getOntologyModel();
        ParameterizedSparqlString pss = queryHelper.getParameterizedQuery(query);
        pss.setIri("concept", baseURI + concept);
        Query q = pss.asQuery();
        QueryExecution queryExecution = QueryExecutionFactory.create(q, ontologyModel);
        ResultSet resultSet = queryExecution.execSelect();
        while(resultSet.hasNext()) {
            QuerySolution querySolution = resultSet.next();
            if (querySolution.contains("class"))
                concepts.add(querySolution.getResource("class").getURI().replace(baseURI, ""));
        }
        return concepts;
    }

    public Set<String> getPropertiesSet() throws FileNotFoundException {
        String baseURI = configuration.getString("ontomap.ontology.baseuri");
        Set<String> properties = new LinkedHashSet<>();
        String query = "SELECT DISTINCT ?prop WHERE {?prop rdf:type ?t. VALUES (?t) {(owl:DatatypeProperty) (owl:ObjectProperty)} }";
        OntModel ontologyModel = getOntologyModel();
        ParameterizedSparqlString pss = queryHelper.getParameterizedQuery(query);
        Query q = pss.asQuery();
        QueryExecution queryExecution = QueryExecutionFactory.create(q, ontologyModel);
        ResultSet resultSet = queryExecution.execSelect();
        while(resultSet.hasNext()) {
            QuerySolution querySolution = resultSet.next();
            if (querySolution.contains("prop")) {
                String prop = querySolution.getResource("prop").getURI();
                if (prop.startsWith(baseURI))
                    properties.add(prop.replace(baseURI, ""));
            }
        }
        return properties;
    }

    public static String getConceptValuesString(Set<String> concepts) {
        List<String> conceptsURIs = new ArrayList<>();
        for (String c : concepts) {
            conceptsURIs.add(String.format("<%s>", c));
        }
        return Joiner.on(" ").join(conceptsURIs);
    }
}
