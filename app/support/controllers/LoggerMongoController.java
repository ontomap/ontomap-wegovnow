package support.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.common.base.Splitter;
import com.google.common.base.Strings;
import com.google.common.primitives.Doubles;
import com.mongodb.*;
import com.mongodb.bulk.BulkWriteResult;
import com.mongodb.bulk.BulkWriteUpsert;
import com.mongodb.client.*;
import com.mongodb.client.ClientSession;
import com.mongodb.client.model.*;
import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;
import com.mongodb.gridfs.CLI;
import com.mongodb.session.*;
import controllers.ResponseController;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.jena.ext.com.google.common.collect.Streams;
import org.bson.*;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import play.Configuration;
import play.Logger;
import play.mvc.Result;
import support.BsonSerializer;
import support.CacheKey;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.print.Doc;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.stream.Collectors;

import static play.mvc.Results.ok;

/**
 * This class contains the methods used for inserting and retrieving data from the Logger.
 */
@Singleton
public class LoggerMongoController {

    public static final String APPLICATION_FIELD = "application";
    public static final String EVENT_LIST_FIELD = "event_list";
    public static final String ACTIVITY_OBJECTS_FIELD = "activity_objects";
    public static final String PROPERTIES_FIELD = "properties";
    public static final String EXTERNAL_URL_FIELD = "external_url";
    public static final String HIDDEN_FIELD = "hidden";
    public static final String VISIBILITY_DETAILS_FIELD = "visibility_details";
    public static final String HAS_TYPE_FIELD = "hasType";
    public static final String ADDITIONAL_PROPERTIES_FIELD = "additionalProperties";
    public static final String UNIT_FIELD = "unit";
    public static final String VALUE_FIELD = "value";
    public static final String REFERENCES_FIELD = "references";
    public static final String REFERENCES_FEATURE_FIELD = "references.feature";
    public static final String TIMESTAMP_FIELD = "timestamp";
    public static final String FEATURE_FIELD = "feature";
    public static final String DETAILS_FIELD = "details";
    public static final String ACTOR_FIELD = "actor";
    public static final String ACTIVITY_OBJECTS_GEOMETRY_FIELD = "activity_objects.geometry";
    public static final String GEO_OBJECTS_FEATURE_GEOMETRY_FIELD = "references.feature.geometry";
    public static final String ACTIVITY_TYPE_FIELD = "activity_type";
    public static final String ACTIVITY_OBJECTS_PROPERTIES_HAS_TYPE_FIELD = "activity_objects.properties.hasType";
    public static final String GEO_OBJECTS_FEATURE_PROPERTIES_HAS_TYPE_FIELD = "references.feature.properties.hasType";
    public static final String ACTIVITY_OBJECTS_PROPERTIES_EXTERNAL_URL_FIELD = "activity_objects.properties.external_url";
    public static final String GEO_OBJECTS_EXTERNAL_URL_FIELD = "references.external_url";
    public static final String HIDDEN_LIST_FIELD = "hidden_list";

    public static final String MODERATION = "activity_objects.properties.additionalProperties.moderation";


    public static final String CURRENT_VISIBILITY_FIELD = "current_visibility";
    public static final String ACTIVITY_OBJECTS_CURRENT_VISIBILITY = "activity_objects.$.current_visibility";
    public static final String VISIBLE = "visible";
    public static final String HIDDEN = "hidden";

    public static final String FEATURES_FIELD = "features";
    public static final String PROPERTIES_EXTERNAL_URL_FIELD = "properties.external_url";
    public static final String PROPERTIES_TIMESTAMP_FIELD = "properties.timestamp";
    public static final String VISIBILITY_LAST_UPDATE_TIMESTAMP_FIELD = "visibility_last_update_timestamp";
    public static final String GEOMETRY_FIELD = "geometry";
    public static final String PROPERTIES_HAS_TYPE_FIELD = "properties.hasType";
    public static final String INSTANCE_FIELD = "instance";

    private Configuration configuration;
    private ResponseController responseController;
    private MongoDBController mongoDBController;
    private MappingTranslationController mappingTranslationController;
    private OntologyController ontologyController;
    private ApplicationsController applicationsController;
    private SubscriptionsMongoController subscriptionsMongoController;

    // since Mongo doesn't have multi-collection transactions yet, a ReadWriteLock is used to ensure that write operations can run without interference
    private ReadWriteLock lock;

    @Inject
    public LoggerMongoController(Configuration configuration, ResponseController responseController,
                                 MongoDBController mongoDBController, MappingTranslationController mappingTranslationController,
                                 OntologyController ontologyController, ApplicationsController applicationsController,
                                 SubscriptionsMongoController subscriptionsMongoController) {
        this.configuration = configuration;
        this.responseController = responseController;
        this.mongoDBController = mongoDBController;
        this.mappingTranslationController = mappingTranslationController;
        this.ontologyController = ontologyController;
        this.applicationsController = applicationsController;
        this.subscriptionsMongoController = subscriptionsMongoController;

        lock = new ReentrantReadWriteLock(true);
    }

    /**
     * Inserts an event into the logger.
     * @param key The idempotency key. Can be null
     * @param application The application performing the request
     * @param instance The domain name of the OTM instance
     * @param requestBody A JsonNode containing the request body of the request
     * @return A Play result representing a success/failure response
     * @throws JsonProcessingException If a problem occurred while parsing JSON data
     * @throws MongoTimeoutException If a problem occurred while connecting to the database
     */
    public Result insertEvents(String key, String application, String instance, JsonNode requestBody) throws JsonProcessingException, MongoTimeoutException {
        ObjectMapper om = new ObjectMapper();
        MongoDatabase database = mongoDBController.getDatabase();
        MongoCollection<Document> collection = database.getCollection(configuration.getString("ontomap.logger.mongodb.collection"));
        // event analysis
        List<Document> documentList = new ArrayList<>();
        ArrayNode elementsToChange = om.createArrayNode();
        Iterator<JsonNode> eventsIter = requestBody.withArray(EVENT_LIST_FIELD).elements();
        List<JsonNode> activityObjects = new ArrayList<>();

        // if this is true, the html_snippet field is sanitized (to prevent e.g. XSS exploits)
        boolean validateHtml = configuration.getBoolean("ontomap.application.html_snippet_validation");

        // iterating over the events in the request body
        while (eventsIter.hasNext()) {
            JsonNode eventJson = eventsIter.next();

            // todo: activity_type validation

            // activity_objects validation
            // this set is used to check if an event contains non-unique external urls
            Set<String> urls = new HashSet<>();
            if (eventJson.get(ACTIVITY_OBJECTS_FIELD) != null) {

                // iterating over each activity_object
                for (Iterator<JsonNode> activityObjectsIterator = eventJson.withArray(ACTIVITY_OBJECTS_FIELD).elements(); activityObjectsIterator.hasNext();) {
                    JsonNode activityObject = activityObjectsIterator.next();

                    // check if external url is not duplicated
                    String extUrl = activityObject.get(PROPERTIES_FIELD).get(EXTERNAL_URL_FIELD).asText();
                    if (!urls.add(extUrl)) return responseController.duplicatedActivityObject();

                    // translation
                    ObjectNode translatedProps = om.createObjectNode();
                    String applicationConcept = activityObject.path(PROPERTIES_FIELD).path(HAS_TYPE_FIELD).asText();
                    String translatedConcept = mappingTranslationController.getConceptMapping(instance, application, applicationConcept);

                    // if the concept mapping doesn't exist abort
                    if (Strings.isNullOrEmpty(translatedConcept))
                        return responseController.missingConceptMapping(applicationConcept);
                    translatedProps.put(HAS_TYPE_FIELD, translatedConcept);

                    for (Iterator<Map.Entry<String, JsonNode>> propertiesIterator = activityObject.get(PROPERTIES_FIELD).fields(); propertiesIterator.hasNext();) {
                        Map.Entry<String, JsonNode> property = propertiesIterator.next();
                        switch (property.getKey()) {
                            case HAS_TYPE_FIELD: // already inserted
                                break;
                            case ADDITIONAL_PROPERTIES_FIELD: //these fields aren't translated
                            case EXTERNAL_URL_FIELD:
                                translatedProps.set(property.getKey(), property.getValue());
                                break;
                            case "html_snippet": // sanitizing html if required
                                if (validateHtml) {
                                    String cleanHtml = Jsoup.clean(property.getValue().asText(), Whitelist.relaxed());
                                    Logger.debug(cleanHtml);
                                    translatedProps.put(property.getKey(), cleanHtml);
                                }
                                else translatedProps.set(property.getKey(), property.getValue());
                                break;
                            default: // all other properties are translated; if a mapping is missing abort
                                String translatedProperty = mappingTranslationController.getPropertyMapping(instance, application, applicationConcept, property.getKey());
                                if (Strings.isNullOrEmpty(translatedProperty))
                                    return responseController.missingPropertyMapping(property.getKey());
                                String unit = mappingTranslationController.getPropertyUnit(instance, application, applicationConcept, property.getKey());
                                if (unit == null)
                                    translatedProps.set(translatedProperty, property.getValue());
                                else {
                                    translatedProps.set(translatedProperty,
                                            om.createObjectNode().put(UNIT_FIELD, unit).set(VALUE_FIELD, property.getValue()));
                                }
                        }
                    }

                    // checking property names for invalid characters (MongoDB restrictions)
                    String invalid = checkPropertyNames(translatedProps);
                    if (!Strings.isNullOrEmpty(invalid))
                        return responseController.invalidProperty(invalid);

                    //set the current visibility of the feature
                    String visibility = isFeatureHidden(instance,application,extUrl) ? HIDDEN : VISIBLE;
                    ((ObjectNode) activityObject).put(CURRENT_VISIBILITY_FIELD, visibility);

                    // storing activity_objects for the "features" collection for later (a copy is created to leave the events intact)
                    ((ObjectNode) activityObject).set(PROPERTIES_FIELD, translatedProps);
                    ObjectNode objectCopy = ((ObjectNode) activityObject).deepCopy();
                    objectCopy.put(APPLICATION_FIELD, application);
                    objectCopy.with(PROPERTIES_FIELD).put(TIMESTAMP_FIELD, eventJson.get(TIMESTAMP_FIELD).longValue());
                    objectCopy.put(VISIBILITY_LAST_UPDATE_TIMESTAMP_FIELD, eventJson.get(TIMESTAMP_FIELD).longValue());
                    activityObjects.add(objectCopy);
                }
            }

            // checking property names of "details" field
            if (eventJson.get(DETAILS_FIELD) != null) {
                String invalid = checkPropertyNames(eventJson.get(DETAILS_FIELD));
                if (!Strings.isNullOrEmpty(invalid))
                    return responseController.invalidProperty(invalid);
            }

            // visibility details
            JsonNode hiddenList = eventJson.get(VISIBILITY_DETAILS_FIELD);
            if (hiddenList != null) { // if a hiddenlist is present...
                long timestamp = eventJson.get(TIMESTAMP_FIELD).asLong();
                // ...iterate over its elements and fill the list with the changes to execute
                for(Iterator<JsonNode> hiddenListIterator = hiddenList.elements(); hiddenListIterator.hasNext();) {
                    JsonNode element = hiddenListIterator.next();
                    elementsToChange.add(
                            om.createObjectNode()
                                    .put(TIMESTAMP_FIELD, timestamp)
                                    .put(EXTERNAL_URL_FIELD, element.get(EXTERNAL_URL_FIELD).asText())
                                    .put(HIDDEN_FIELD, element.get(HIDDEN_FIELD).asBoolean())
                    );
//                        setHidden(application, element.get(EXTERNAL_URL_FIELD).asText(), timestamp, element.get(HIDDEN_FIELD).asBoolean());
//                        Logger.debug("{}: {} ({})", element.get("external_url").asText(), element.get("hidden").asBoolean(), timestamp);
                }
            }

            // at this point the event is ok, converting to BSON
            ((ObjectNode) eventJson).put(APPLICATION_FIELD, application);
            ((ObjectNode) eventJson).put(INSTANCE_FIELD, instance);
            Document doc = Document.parse(om.writeValueAsString(eventJson));
            documentList.add(doc);
        }
//        Logger.debug("Key: {}", key);

        // writing on DB
        lock.writeLock().lock();
        try(ClientSession dbSession = mongoDBController.getSession()) {

            dbSession.startTransaction();
            try {
                // checking idempotent requests: if REPLAY or CONFLICT, don't actually write on DB
                switch (checkIdempotency(key, application, instance, requestBody)) {
                    case REPLAY:
                        return responseController.successInsertEvents(documentList.size());
                    case CONFLICT:
                        return responseController.idempotencyConflict();
                }
                // writing events ("events" collection)
                collection.insertMany(dbSession,documentList); // todo: catch exceptions, bulkwrite list?

                // updating features ("features" collection)
                updateFeatures(instance, activityObjects, false,dbSession);

                // updating references events ("events" collection)
                updateReferences(instance,dbSession);

                // applying visibility changes ("features" collection)
                for (Iterator<JsonNode> hiddenIterator = elementsToChange.elements(); hiddenIterator.hasNext();) {
                    JsonNode element = hiddenIterator.next();
                    setFeaturesVisibility(instance, application, element.get(EXTERNAL_URL_FIELD).asText(), element.get(TIMESTAMP_FIELD).asLong(), element.get(HIDDEN_FIELD).asBoolean(),dbSession);
                }

                // writing idempotency key
               writeIdempotency(key, application, instance, requestBody,dbSession);

               dbSession.commitTransaction();

            } catch (Exception e){
                dbSession.abortTransaction();
                throw e;
            }finally {
                dbSession.close();
            }
        }
        finally {
            lock.writeLock().unlock();
        }
        // checking subscriptions and send notifications if necessary
        subscriptionsMongoController.checkEvents(instance, documentList);

        return responseController.successInsertEvents(documentList.size());
    }

    /**
     * Retrieves a list of events based on the specified filters. The events returned are sorted by their timestamp, in
     * inverse chronological order.
     * @param instance The domain name of the OTM instance
     * @param actor If not null, only events from this actor are retrieved
     * @param startTime If not null, only events more recent than this timestamp are retrieved
     * @param endTime If not null, only events more older than this timestamp are retrieved
     * @param application If not null, only events from this application are retrieved
     * @param boundingBoxString A bounding box string, see {@link TripleStoreController#boundingBoxToWkt(String)}. If not null,
     *                          only events with activity_objects (or references to them) having geometries in this area are retrieved
     * @param activityType If not null, only events having this activity type are retrieved
     * @param concept If not null, only events with activity_objects which are instances of this concept are retrieved
     * @param externalUrl If not null, only events with activity_objects with this external url are retrieved
     * @param referenceConcept If not null, only events containing references to activity_objects which are instances of this concept are retrieved
     * @param referenceExternalUrl If not null, only events containing references to activity_objects with this external url are retrieved
     * @param limit If > 0 (and with page > -1) sets the maximum number of results to return
     * @param page If > -1, sets the results page to return. The page size is set using the limit parameter
     * @param excludeFeatures If true, the activity_objects field of each event is not returned
     * @param subConcepts If true and a concept filter is specified, also the subconcepts of the specified concept are used to filter the events
     * @param aggregate If true, returns a list of events having at most one activity_object; see {@link #retrieveAggregatedEvents(List,boolean, int, int)}
     * @return The ObjectNode containing the list of events
     * @throws FileNotFoundException If the ontology file could not be found
     * @throws MongoTimeoutException If a problem occurred while connecting to the database
     */
    public ObjectNode retrieveEvents(String instance, boolean tokenAuth,long actor, long startTime, long endTime, String application, String boundingBoxString,
                                     String activityType, String concept, String externalUrl,
                                     String referenceConcept, String referenceExternalUrl,
                                     int limit, int page, boolean excludeFeatures, boolean subConcepts, boolean aggregate,String visibility, HashMap<String,String> additionalProperties)
            throws FileNotFoundException, MongoTimeoutException {

        MongoDatabase database = mongoDBController.getDatabase();
        MongoCollection<BsonDocument> collection = database.getCollection(configuration.getString("ontomap.logger.mongodb.collection"), BsonDocument.class);

        FindIterable<BsonDocument> findIterable = collection.find().sort(Sorts.descending(TIMESTAMP_FIELD));
        if (excludeFeatures) findIterable.projection(Projections.exclude("references.feature", ACTIVITY_OBJECTS_FIELD, INSTANCE_FIELD));
        else findIterable.projection(Projections.exclude("references.feature", INSTANCE_FIELD));
        List<Bson> filters = new ArrayList<>();

        filters.add(Filters.eq(INSTANCE_FIELD, instance));

        if (actor != -1) {
            filters.add(Filters.eq(ACTOR_FIELD, actor));
        }

        if (startTime != -1) {
            filters.add(Filters.gte(TIMESTAMP_FIELD, startTime));
        }

        if (endTime != -1) {
            filters.add(Filters.lte(TIMESTAMP_FIELD, endTime));
        }

        if (!Strings.isNullOrEmpty(application)) {
            filters.add(Filters.eq(APPLICATION_FIELD, application));
        }

        if (!Strings.isNullOrEmpty(boundingBoxString)) {
            List<Double> coordinates = TripleStoreController.boundingBoxToCoords(boundingBoxString);
            if (coordinates == null) {
                throw new IllegalArgumentException("boundingbox");
            }
            filters.add(Filters.or(
                    Filters.geoWithin(ACTIVITY_OBJECTS_GEOMETRY_FIELD, coordinatesToPolygon(coordinates)),
                    Filters.geoWithin(GEO_OBJECTS_FEATURE_GEOMETRY_FIELD, coordinatesToPolygon(coordinates))
            ));
        }

        if (!Strings.isNullOrEmpty(activityType)) {
            filters.add(Filters.eq(ACTIVITY_TYPE_FIELD, activityType));
        }

        if (!Strings.isNullOrEmpty(concept)) {
            if (!subConcepts)
                filters.add(Filters.eq(ACTIVITY_OBJECTS_PROPERTIES_HAS_TYPE_FIELD, concept));
            else {
                Set<String> concepts = ontologyController.getSubConceptsSet(concept);
                List<Bson> conceptFilters = new ArrayList<>();
                for (String c : concepts) {
                    conceptFilters.add(Filters.eq(ACTIVITY_OBJECTS_PROPERTIES_HAS_TYPE_FIELD, c));
                }
                filters.add(Filters.or(conceptFilters));
            }
        }

        if (!Strings.isNullOrEmpty(referenceConcept)) {
            if (!subConcepts)
                filters.add(Filters.eq(GEO_OBJECTS_FEATURE_PROPERTIES_HAS_TYPE_FIELD, referenceConcept));
            else {
                Set<String> concepts = ontologyController.getSubConceptsSet(referenceConcept);
                List<Bson> conceptFilters = new ArrayList<>();
                for (String c : concepts) {
                    conceptFilters.add(Filters.eq(GEO_OBJECTS_FEATURE_PROPERTIES_HAS_TYPE_FIELD, c));
                }
                filters.add(Filters.or(conceptFilters));
            }
        }

        if (!Strings.isNullOrEmpty(externalUrl)) {
            filters.add(Filters.eq(ACTIVITY_OBJECTS_PROPERTIES_EXTERNAL_URL_FIELD, externalUrl));
        }

        if (!Strings.isNullOrEmpty(referenceExternalUrl)) {
            filters.add(Filters.eq(GEO_OBJECTS_EXTERNAL_URL_FIELD, referenceExternalUrl));
        }

        if(!Strings.isNullOrEmpty(visibility)){
            filters.add(Filters.eq("activity_objects.current_visibility", Boolean.parseBoolean(visibility) ? VISIBLE : HIDDEN));
        }

        if(additionalProperties.size()>0){
            List<Bson> additionalFilters = new ArrayList<>();
            for(String key : additionalProperties.keySet()){
                String value = additionalProperties.get(key).toLowerCase();
                if(value.compareTo("true")==0 || value.compareTo("false")==0) {
                      additionalFilters.add(Filters.eq("activity_objects.properties.additionalProperties."+key,Boolean.parseBoolean(value)));
                }
                else if(StringUtils.isNumeric(value)){
                    additionalFilters.add(Filters.eq("activity_objects.properties.additionalProperties."+key,Double.parseDouble(value)));
                }
                additionalFilters.add(Filters.eq("activity_objects.properties.additionalProperties."+key,additionalProperties.get(key)));
                filters.add(Filters.or(additionalFilters));
            }
        }

        if (aggregate) {
            return retrieveAggregatedEvents(filters,tokenAuth, limit, page);
        }

        if (!filters.isEmpty())
            findIterable.filter(Filters.and(filters));

        // pagination
        if (page > -1) {
            int defaultPageSize = configuration.getInt("ontomap.application.defaultpagesize");
            int pageSize  = (limit > 0 && limit <= 500) ? limit : defaultPageSize;
            findIterable.limit(pageSize);
            findIterable.skip(pageSize * page);
        }

        ObjectMapper om = new ObjectMapper();
        SimpleModule sm = new SimpleModule();
        sm.addSerializer(BsonDocument.class, new BsonSerializer());
        om.registerModule(sm);
        ObjectNode result = om.createObjectNode();
        result.putArray(EVENT_LIST_FIELD);

        lock.readLock().lock();
        try (MongoCursor<BsonDocument> cursor = findIterable.iterator()) {
            while (cursor.hasNext()) {
                ObjectNode node = om.valueToTree(cursor.next());
                node.set("id", node.get("_id")); // todo: rename field using query?
                node.remove("_id");
                if(tokenAuth){
                   cleanEventForTokenRequest(node,false);
                }
                node.put("applicationName", applicationsController.getAppName(node.get("application").asText()));
                result.withArray(EVENT_LIST_FIELD).add(node);
            }
            return result;
        }
        finally {
            lock.readLock().unlock();
        }

    }

    /**
     * Given an event ID, returns the event having said ID, or null if it doesn't exist
     * @param instance The domain name of the OTM instance
     * @param eventId The ID of the event
     * @return An ObjectNode containing the event
     * @throws MongoTimeoutException If a problem occurred while connecting to the database
     */
    public ObjectNode retrieveSingleEvent(String instance,boolean tokenAuth, String eventId) throws MongoTimeoutException {
        MongoDatabase database = mongoDBController.getDatabase();
        MongoCollection<BsonDocument> collection = database.getCollection(configuration.getString("ontomap.logger.mongodb.collection"), BsonDocument.class);
        ObjectMapper om = new ObjectMapper();
        SimpleModule sm = new SimpleModule();
        sm.addSerializer(BsonDocument.class, new BsonSerializer());
        om.registerModule(sm);
        lock.readLock().lock();
        try {
            // if id is not valid, IllegalArgumentException is thrown
            ObjectId objectId = new ObjectId(eventId);
            BsonDocument eventBson = collection.find(new Document().append("_id", objectId).append(INSTANCE_FIELD, instance))
                    .projection(Projections.exclude("references.feature", INSTANCE_FIELD))
                    .first();
            if (eventBson == null) return null;
            ObjectNode event = om.valueToTree(eventBson);
            event.set("id", event.get("_id"));
            event.remove("_id");
            if(tokenAuth){
                cleanEventForTokenRequest(event,false);
            }
            event.put("applicationName", applicationsController.getAppName(event.get("application").asText()));
            return event;
        }
        catch (IllegalArgumentException e) {
            // id not valid, returning null
            return null;
        }
        finally {
            lock.readLock().unlock();
        }
    }

    /**
     * Retrieves a list of events based on the specified filters. The events returned are sorted by their timestamp, in
     * inverse chronological order. The events returned contain at most one activity_object; the activity_objects field is
     * not an Array of objects but an Object itself. If an event contains more than one activity_objects, the same event is returned once for
     * each activity_object. For each pair external url/application, only the most recent event concerning that activity_object is returned,
     * but if the most recent activity_type of said event is "object_removed", the event is excluded from the result (since it represents the removal
     * of an object).
     * @param matchFilters The filter list
     * @param limit If > 0 (and with page > -1) sets the maximum number of results to return
     * @param page If > -1, sets the results page to return. The page size is set using the limit parameter
     * @return The ObjectNode containing the list of events
     */
    private ObjectNode retrieveAggregatedEvents(List<Bson> matchFilters,boolean tokenAuth, int limit, int page) {
        MongoDatabase database = mongoDBController.getDatabase();
        MongoCollection<BsonDocument> collection = database.getCollection(configuration.getString("ontomap.logger.mongodb.collection"), BsonDocument.class);

        List<Bson> pipeline = new ArrayList<>();

//        matchFilters.add(Filters.not(Filters.eq("activity_type", "object_removed")));
        // adding filters
        if (!matchFilters.isEmpty()) pipeline.add(Aggregates.match(Filters.and(matchFilters)));

        // "unpacking" activity_objects array
        pipeline.add(Aggregates.unwind("$activity_objects"));

        pipeline.add(Aggregates.sort(Sorts.descending("timestamp")));


        // grouping events by
        pipeline.add(new Document(
                "$group",
                new Document("_id", new Document().append("application", "$application").append("external_url", "$activity_objects.properties.external_url"))
                .append("event", new Document("$first", "$$ROOT"))
        ));
//        pipeline.add(Aggregates.group("$activity_objects.properties.external_url $application", Accumulators.first("event", "$$ROOT")));
        pipeline.add(Aggregates.replaceRoot("$event"));

        // excluding events with type "object_removed"
        pipeline.add(Aggregates.match(Filters.not(Filters.eq("activity_type", "object_removed"))));
        pipeline.add(Aggregates.sort(Sorts.descending("timestamp")));
        if (page > -1) {
            int defaultPageSize = configuration.getInt("ontomap.application.defaultpagesize");
            int pageSize  = (limit > 0 && limit <= 500) ? limit : defaultPageSize;
            pipeline.add(Aggregates.skip(pageSize * page));
            pipeline.add(Aggregates.limit(pageSize));
        }

        ObjectMapper om = new ObjectMapper();
        SimpleModule sm = new SimpleModule();
        sm.addSerializer(BsonDocument.class, new BsonSerializer());
        om.registerModule(sm);
        ObjectNode result = om.createObjectNode();
        result.putArray(EVENT_LIST_FIELD);

        AggregateIterable<BsonDocument> aggregateIterable = collection.aggregate(pipeline);

        lock.readLock().lock();
        try (MongoCursor<BsonDocument> cursor = aggregateIterable.iterator()) {
            while (cursor.hasNext()) {
                ObjectNode node = om.valueToTree(cursor.next());
                node.set("id", node.get("_id"));
                node.remove("_id");
                if(tokenAuth){
                    cleanEventForTokenRequest(node,true);
                }
                node.put("applicationName", applicationsController.getAppName(node.get("application").asText()));
                result.withArray(EVENT_LIST_FIELD).add(node);
            }
            return result;
        }
        finally {
            lock.readLock().unlock();
        }

    }

    /**
     * Returns the list of the hidden objects managed by a specific application.
     * @param application The application managing the objects
     * @param instance The domain name of the OTM instance
     * @return The list of the hidden objects
     * @throws MongoTimeoutException If a problem occurred while connecting to the database
     */
    public ObjectNode getHiddenList(String application, String instance) throws MongoTimeoutException {
        MongoDatabase database = mongoDBController.getDatabase();
        MongoCollection<BsonDocument> collection = database.getCollection(configuration.getString("ontomap.logger.mongodb.features.collection"), BsonDocument.class);

        Document filter = new Document();
        filter.put(APPLICATION_FIELD, application);
        filter.put(INSTANCE_FIELD, instance);
        filter.put(HIDDEN_FIELD, true);
        ObjectMapper om = new ObjectMapper();
        ObjectNode result = om.createObjectNode();
        result.putArray(HIDDEN_LIST_FIELD);
        FindIterable<BsonDocument> findIterable = collection.find(filter);
        lock.readLock().lock();
        try (MongoCursor<BsonDocument> cursor = findIterable.iterator()) {
            while (cursor.hasNext()) {
                result.withArray(HIDDEN_LIST_FIELD).add(cursor.next().get(PROPERTIES_FIELD).asDocument().get(EXTERNAL_URL_FIELD).asString().getValue());
            }
        }
        finally {
            lock.readLock().unlock();
        }
        return result;
    }

    /**
     * Returns the count of the concept instances from the Logger which satisfy the specified filters.
     * @param instance The domain name of the OTM instance
     * @param application If not null, only concept instances from this application are retrieved
     * @param boundingBoxString A bounding box string, see {@link TripleStoreController#boundingBoxToWkt(String)}. If not null,
     *                          only concept instances located in this area are retrieved
     * @param distanceString A distance string, see {@link #distancePairFromString(String)}. If not null, only concept instances
     *                       located in the circle specified are retrieved
     * @param concepts If not null, only instances of the specified concepts are retrieved
     * @return The count of the concept instances
     * @throws IllegalArgumentException If boundingBoxString or distanceString are in a wrong format
     * @throws MongoTimeoutException If a problem occurred while connecting to the database
     */
    public int getFeaturesCount(String instance, String application, String boundingBoxString, String distanceString, Set<String> concepts)
            throws IllegalArgumentException, IOException, MongoTimeoutException {
        List<ObjectNode> result = getFeatures(instance, application, boundingBoxString, distanceString, concepts, true, true);
        return result.size();
    }

    /**
     * Returns a list of concept instances from the Logger, based on the specified filters.
     * @param instance The domain name of the OTM instance
     * @param application If not null, only concept instances from this application are retrieved
     * @param boundingBoxString A bounding box string, see {@link TripleStoreController#boundingBoxToWkt(String)}. If not null,
     *                          only concept instances located in this area are retrieved
     * @param distanceString A distance string, see {@link #distancePairFromString(String)}. If not null, only concept instances
     *                       located in the circle specified are retrieved
     * @param concepts If not null, only instances of the specified concepts are retrieved
     * @param descriptions If true, includes the complete set of properties for each concept instance
     * @param geometries If true, includes the geometry for each concept instance
     * @return The list of the concept instances
     * @throws IllegalArgumentException If boundingBoxString or distanceString are in a wrong format
     * @throws MongoTimeoutException If a problem occurred while connecting to the database
     */
    public List<ObjectNode> getFeatures(String instance, String application, String boundingBoxString, String distanceString, Set<String> concepts, boolean descriptions, boolean geometries)
            throws IllegalArgumentException, MongoTimeoutException {

        ObjectMapper om = new ObjectMapper();
        SimpleModule sm = new SimpleModule();
        sm.addSerializer(BsonDocument.class, new BsonSerializer());
        om.registerModule(sm);

        if (!configuration.getBoolean("ontomap.logger.enabled")) {
            return new ArrayList<>();
        }

        MongoDatabase database = mongoDBController.getDatabase();
        MongoCollection<BsonDocument> collection = database.getCollection(configuration.getString("ontomap.logger.mongodb.features.collection"), BsonDocument.class);

        FindIterable<BsonDocument> findIterable = collection.find()
                .projection(Projections.exclude("_id", HIDDEN_FIELD, VISIBILITY_LAST_UPDATE_TIMESTAMP_FIELD, INSTANCE_FIELD));
        List<Bson> filters = new ArrayList<>();

        filters.add(Filters.eq(INSTANCE_FIELD, instance));
        filters.add(Filters.not(Filters.exists(HIDDEN_FIELD)));

        if (!Strings.isNullOrEmpty(application)) {
            filters.add(Filters.eq(APPLICATION_FIELD, application));
        }

        if (!Strings.isNullOrEmpty(boundingBoxString)) {
            List<Double> coordinates = TripleStoreController.boundingBoxToCoords(boundingBoxString);
            if (coordinates == null) {
                throw new IllegalArgumentException("boundingbox");
            }
            filters.add(Filters.geoWithin(GEOMETRY_FIELD, coordinatesToPolygon(coordinates)));
        }

        if (!Strings.isNullOrEmpty(distanceString)) {
            Pair<Bson, Double> distancePair = distancePairFromString(distanceString);
            if (distancePair == null) {
                throw new IllegalArgumentException("distance");
            }
            filters.add(Filters.near(GEOMETRY_FIELD, distancePair.getLeft(), distancePair.getRight(), null));
        }

        if (concepts != null && !concepts.isEmpty()) {
            List<Bson> conceptFilters = new ArrayList<>();
            for (String concept : concepts) {
                conceptFilters.add(Filters.eq(PROPERTIES_HAS_TYPE_FIELD, concept));
            }
            filters.add(Filters.or(conceptFilters));
        }

        if (!filters.isEmpty()) findIterable.filter(Filters.and(filters));

        List<ObjectNode> result = new ArrayList<>();
        lock.readLock().lock();
        try (MongoCursor<BsonDocument> cursor = findIterable.iterator()) {
            while (cursor.hasNext()) {
                ObjectNode feature = om.valueToTree(cursor.next());
                feature.set("id", feature.path(PROPERTIES_FIELD).path(EXTERNAL_URL_FIELD));
                result.add(feature);
            }
        }
        finally {
            lock.readLock().unlock();
        }

        for (ObjectNode f : result) {
            f.put("applicationName", applicationsController.getAppName(f.get("application").asText()));
            if (!geometries) f.putNull(GEOMETRY_FIELD);
            if (!descriptions) {
                ObjectNode newProperties = om.createObjectNode();
//                    newProperties.set("label", f.path("properties").get("label"));
                newProperties.set(EXTERNAL_URL_FIELD, f.path(PROPERTIES_FIELD).get(EXTERNAL_URL_FIELD));
                newProperties.set(HAS_TYPE_FIELD, f.path(PROPERTIES_FIELD).get(HAS_TYPE_FIELD));
                f.set(PROPERTIES_FIELD, newProperties);
            }
        }

        return result;
    }

    /**
     * Deletes events from the logger. Events can be deleted based on ID (a single event), an actor (all events from that actor)
     * or source application (all events from that application). Only one filter can be specified. When an event containing an
     * activity_object is deleted from the "events" collection, the "features" collection is updated accordingly, with the
     * latest version of the features after the deletion of the events.
     * @param instance The domain name of the OTM instance
     * @param actor If > -1, deletes all events from that actor
     * @param id If not null, deletes the event having that ID
     * @param sourceApplication If not null, deletes the event sent by the specified application
     * @return A Play result representing a success/failure response
     * @throws JsonProcessingException If a problem occurred while parsing JSON data
     * @throws IllegalArgumentException If not exactly one filter is specified
     */
    public Result deleteUserEvents(String instance, long actor, String id, String sourceApplication) throws JsonProcessingException, IllegalArgumentException {
        MongoDatabase database = mongoDBController.getDatabase();

        // we need to delete documents from 2 collections
        MongoCollection<BsonDocument> eventsCollection = database.getCollection(configuration.getString("ontomap.logger.mongodb.collection"), BsonDocument.class);
        MongoCollection<Document> featuresCollection = database.getCollection(configuration.getString("ontomap.logger.mongodb.features.collection"));

        try (ClientSession dbSession = mongoDBController.getSession()) {

            dbSession.startTransaction();

            // iterable used to find which documents must be updated/deleted from the "features" collection
            FindIterable<BsonDocument> objectsToUpdateIterable = eventsCollection.find(dbSession).projection(Projections.include("activity_objects.properties.external_url", APPLICATION_FIELD));
            ObjectMapper om = new ObjectMapper();
            SimpleModule sm = new SimpleModule();
            sm.addSerializer(BsonDocument.class, new BsonSerializer());
            om.registerModule(sm);

            Document eventFilter = new Document();
            eventFilter.put(INSTANCE_FIELD, instance);
            if (actor != -1) eventFilter.put(ACTOR_FIELD, actor);
            if (!Strings.isNullOrEmpty(id)) eventFilter.put("_id", new ObjectId(id));
            if (!Strings.isNullOrEmpty(sourceApplication)) eventFilter.put(APPLICATION_FIELD, sourceApplication);

            // checking that only one filter is active before deleting events
            if (eventFilter.size() != 2) throw new IllegalArgumentException();

            objectsToUpdateIterable.filter(eventFilter);
            Set<Pair<String, String>> objectsSet = new HashSet<>();
            long featuresDeletedCount;
            long eventsDeletedCount;
            lock.writeLock().lock();
            try {
                // getting app/extUrl of features to update
                try (MongoCursor<BsonDocument> cursor = objectsToUpdateIterable.iterator()) {
                    while (cursor.hasNext()) {
                        BsonDocument event = cursor.next();
                        String application = event.getString(APPLICATION_FIELD).getValue();
                        if (event.containsKey(ACTIVITY_OBJECTS_FIELD)) {
                            BsonArray actObjsArray = event.getArray(ACTIVITY_OBJECTS_FIELD);
                            objectsSet.addAll(
                                    actObjsArray.stream() // using a stream to create a list of app/exturl pairs
                                            .map(val -> Pair.of(application, val.asDocument().getDocument(PROPERTIES_FIELD).getString(EXTERNAL_URL_FIELD).getValue()))
                                            .collect(Collectors.toList()));
                        }
                    }
                }

                HashMap<CacheKey, JsonNode> updateMap = new HashMap<>();
                // for each feature to update, get the most recent version of that feature, excluding the event to delete
                for (Pair<String, String> p : objectsSet) {
                    FindIterable<BsonDocument> featureIterable = eventsCollection.find(dbSession);
                    List<Bson> featFilters = new ArrayList<>();
                    featFilters.add(Filters.eq(INSTANCE_FIELD, instance));
                    featFilters.add(Filters.eq(APPLICATION_FIELD, p.getLeft()));
                    featFilters.add(Filters.eq(ACTIVITY_OBJECTS_PROPERTIES_EXTERNAL_URL_FIELD, p.getRight()));

                    // excluding the event to be deleted from the search
                    if (actor != -1) featFilters.add(Filters.not(Filters.eq(ACTOR_FIELD, actor)));
                    if (!Strings.isNullOrEmpty(id)) featFilters.add(Filters.not(Filters.eq("_id", new ObjectId(id))));
                    if (!Strings.isNullOrEmpty(sourceApplication))
                        featFilters.add(Filters.not(Filters.eq(APPLICATION_FIELD, sourceApplication)));

                    featureIterable.filter(Filters.and(featFilters));

                    // sorting by timestamp (most recent event first) and getting the first result (which will be the most recent)
                    featureIterable.sort(Sorts.descending(TIMESTAMP_FIELD));
                    BsonDocument event = featureIterable.first();
                    if (event != null) {
                        JsonNode activityObjectsJson = om.valueToTree(event.get(ACTIVITY_OBJECTS_FIELD));

                        // using a stream to get the right activity_object (an event could contain more than one of them)
                        JsonNode feature = Streams.stream(activityObjectsJson.iterator())
                                .filter(entry -> p.getRight().equals(entry.get(PROPERTIES_FIELD).get(EXTERNAL_URL_FIELD).textValue()))
                                .findFirst().get();
                        //adding some required fields to the feature
                        ((ObjectNode) feature)
                                .put(APPLICATION_FIELD, event.getString(APPLICATION_FIELD).getValue())
                                .with(PROPERTIES_FIELD).put(TIMESTAMP_FIELD, event.getNumber(TIMESTAMP_FIELD).longValue());
                        updateMap.put(new CacheKey(p.getLeft(), p.getRight()), feature);
                    } else updateMap.put(new CacheKey(p.getLeft(), p.getRight()), null);
                }
//            updateMap.entrySet().forEach(val -> Logger.debug(val.toString()));


                // deleting events
                DeleteResult deleteResult = eventsCollection.deleteMany(dbSession, eventFilter);
                eventsDeletedCount = deleteResult.getDeletedCount();
//            Logger.debug(deleteResult.toString());

                List<JsonNode> updateList = new ArrayList<>();
                featuresDeletedCount = 0;

                // for each feature to update, either update it with the appropriate version
                for (Map.Entry<CacheKey, JsonNode> e : updateMap.entrySet()) {
                    if (e.getValue() == null) {
                        Document deleteFilter = new Document()
                                .append(INSTANCE_FIELD, instance)
                                .append(APPLICATION_FIELD, e.getKey().getKeys()[0])
                                .append(PROPERTIES_EXTERNAL_URL_FIELD, e.getKey().getKeys()[1]);
                        deleteResult = featuresCollection.deleteOne(dbSession, deleteFilter);
                        featuresDeletedCount += deleteResult.getDeletedCount();
//                    Logger.debug("{}: {}",e.getKey(), res.toString());
                    } else {
                        updateList.add(e.getValue());
//                    Logger.debug("Update {}", e.toString());
                    }
                }
                updateFeatures(instance, updateList, true, dbSession);

                dbSession.commitTransaction();

            } catch (Exception e) {
                dbSession.abortTransaction();
                throw e;
            } finally {
                dbSession.close();
                lock.writeLock().unlock();
            }
        return ok(om.createObjectNode()
                .put("status", "ok")
                .put("events_deleted", eventsDeletedCount)
                .put("features_deleted", featuresDeletedCount));
        }
    }

    /**
     * Checks the status of a request including an idempotency key. If an idempotency key is not provided, the requested
     * operation can always be executed; if a key is provided, the request can be executed only if the same key hasn't been
     * used in the previous 24 hours. If a key is reused, the request is considered a replay if the request body is the same
     * as the previous one; if not, the request is considered in conflict with the previous one.
     * @param key The idempotency key
     * @param application The application performing the request
     * @param instance The domain name of the OTM instance
     * @param requestBody The body of the request
     * @return {@link IdempotencyResult#SUCCESS} if the requested operation can be executed; {@link IdempotencyResult#REPLAY}
     * if the same request has already been sent; {@link IdempotencyResult#CONFLICT} if the requested operation can't be executed
     */
    private IdempotencyResult checkIdempotency(String key, String application, String instance, JsonNode requestBody) {
        if (key == null) return IdempotencyResult.SUCCESS;
        MongoDatabase database = mongoDBController.getDatabase();
        MongoCollection<BsonDocument> collection = database.getCollection(configuration.getString("ontomap.logger.mongodb.idempotency.collection"), BsonDocument.class);
        Document filter = new Document()
                .append("key", key)
                .append("application", application)
                .append("instance", instance);
        BsonDocument entry = collection.find().filter(filter).first();
        if (entry == null) return IdempotencyResult.SUCCESS;
        ObjectMapper om = new ObjectMapper();
        SimpleModule sm = new SimpleModule();
        sm.addSerializer(BsonDocument.class, new BsonSerializer());
        om.registerModule(sm);
        ObjectNode payload = (ObjectNode) om.valueToTree(entry).get("payload");
//        Logger.debug("{}, {}", payload.hashCode(), payload.toString());
//        Logger.debug("{}, {}", requestBody.hashCode(), requestBody.toString());
        if (payload.hashCode() == requestBody.hashCode()) return IdempotencyResult.REPLAY; // todo: equals?
        else return IdempotencyResult.CONFLICT;
    }

    /**
     * Writes on DB a record for storing a request performed with an idempotency key. Records older than 24 hours are automatically
     * purged from DB (index on date field with expire option)
     * @param key The idempotency key
     * @param application The application performing the request
     * @param instance The domain name of the OTM instance
     * @param requestBody The body of the request
     * @param dbSession The client session of the db transaction
     * @throws MongoWriteException If a problem occurred while connecting to the database
     */
    private void writeIdempotency(String key, String application, String instance, JsonNode requestBody,ClientSession dbSession) throws MongoWriteException {
        if (key == null) return;
        MongoDatabase database = mongoDBController.getDatabase();
        MongoCollection<Document> collection = database.getCollection(configuration.getString("ontomap.logger.mongodb.idempotency.collection"));
        Document entry = new Document()
                .append("key", key)
                .append("application", application)
                .append("instance", instance)
                .append("date", new Date())
                .append("payload", Document.parse(requestBody.toString()));
        collection.insertOne(dbSession,entry);
    }

    /**
     * An enum representing the possible results of an idempotent request
     */
    public enum IdempotencyResult {
        SUCCESS, REPLAY, CONFLICT
    }

    /**
     * Returns a BsonDocument representing a single feature from the logger, which is identified by its source application
     * and its external url. Returns null if no such feature exists.
     * @param instance The domain name of the OTM instance
     * @param application The source application of the feature
     * @param externalUrl The external url of the feature
     * @param timestamp if > 0, retrieves the feature only if it is older than this timestamp
     * @return The BsonDocument representing the feature, if it is found; null otherwise
     * @throws MongoTimeoutException If a problem occurred while connecting to the database
     */
    private BsonDocument getSingleFeature(String instance, String application, String externalUrl, long timestamp) throws MongoTimeoutException {
        MongoDatabase database = mongoDBController.getDatabase();
        MongoCollection<BsonDocument> collection = database.getCollection(configuration.getString("ontomap.logger.mongodb.collection"), BsonDocument.class);
        List<Bson> filters = new ArrayList<>();
        filters.add(Filters.eq(INSTANCE_FIELD, instance));
        filters.add(Filters.eq(APPLICATION_FIELD, application));
        filters.add(Filters.eq(ACTIVITY_OBJECTS_PROPERTIES_EXTERNAL_URL_FIELD, externalUrl));
        if (timestamp > 0) {
            filters.add(Filters.lte(TIMESTAMP_FIELD, timestamp));
        }
        FindIterable<BsonDocument> findIterable =
                collection.find(Filters.and(filters))
                        .projection(Projections.elemMatch(ACTIVITY_OBJECTS_FIELD, Filters.eq(PROPERTIES_EXTERNAL_URL_FIELD, externalUrl)))
                        .sort(Sorts.descending(TIMESTAMP_FIELD))
                        .limit(1);
        try (MongoCursor<BsonDocument> cursor = findIterable.iterator()) {
            if (!cursor.hasNext()) return null;
            return cursor.next().get(ACTIVITY_OBJECTS_FIELD).asArray().get(0).asDocument();
        }
    }

    /**
     * Updates the collection "features" with the list provided. If forceUpdate is set to false, a feature is updated only if the replacement provided
     * is newer than the one currently in the collection.
     * @param instance The domain name of the OTM instance
     * @param features A List of JsonNodes, each representing a feature
     * @param forceUpdate If true, the features are updated regardless of their timestamp.
     * @param dbSession The client session of the db transaction
     * @throws MongoTimeoutException If a problem occurred while connecting to the database
     * @throws JsonProcessingException If a problem occurred while parsing JSON data
     */
    private void updateFeatures(String instance, List<JsonNode> features, boolean forceUpdate,ClientSession dbSession) throws MongoTimeoutException, JsonProcessingException {
        if (features.isEmpty()) return;
        MongoDatabase database = mongoDBController.getDatabase();
        MongoCollection<Document> featuresCollection =
                database.getCollection(configuration.getString("ontomap.logger.mongodb.features.collection"));//.withWriteConcern(WriteConcern.UNACKNOWLEDGED);
        ObjectMapper om = new ObjectMapper();
        List<WriteModel<Document>> writeList = new ArrayList<>();
        // for each feature provided, create an Update operation
        //for each feature if exist i'll update it and have a lesser timestamp (so it's older) we update this it.
        for (JsonNode feature : features) {
            List<Bson> filters = new ArrayList<>();
            filters.add(Filters.eq(INSTANCE_FIELD, instance));
            filters.add(Filters.eq(APPLICATION_FIELD, feature.get(APPLICATION_FIELD).asText()));
            filters.add(Filters.eq(PROPERTIES_EXTERNAL_URL_FIELD, feature.path(PROPERTIES_FIELD).path(EXTERNAL_URL_FIELD).asText()));
            if (!forceUpdate) filters.add(Filters.lte(PROPERTIES_TIMESTAMP_FIELD, feature.path(PROPERTIES_FIELD).path(TIMESTAMP_FIELD).asLong()));
            Document featureBson = Document.parse(om.writeValueAsString(feature));
            List<Bson> updates = new ArrayList<>();
            updates.add(Updates.set(GEOMETRY_FIELD, featureBson.get(GEOMETRY_FIELD)));
            updates.add(Updates.set(PROPERTIES_FIELD, featureBson.get(PROPERTIES_FIELD)));
            writeList.add(new UpdateOneModel<>(Filters.and(filters), Updates.combine(updates)));
        }
        /*
        * If the feature not exist this will create it.
        * Why there is not a unique for-cycle?
        * There are cases where some application need to insert old events contain this feature so the timestamp associated will be lesser than the one we have.
        * The filter created as before will not found the feature (that are in the db) because of the timestamp and the system will try to insert the old version so
        * it will throw an error due to the fact the feature exist. Because we are in a transaction this error will fail the insert of all the events! And we don't want this.
        * The for-cycle below don't check the timestamp and only insert if the feature not exist.
        */
        for (JsonNode feature : features) {
            List<Bson> filters = new ArrayList<>();
            filters.add(Filters.eq(INSTANCE_FIELD, instance));
            filters.add(Filters.eq(APPLICATION_FIELD, feature.get(APPLICATION_FIELD).asText()));
            filters.add(Filters.eq(PROPERTIES_EXTERNAL_URL_FIELD, feature.path(PROPERTIES_FIELD).path(EXTERNAL_URL_FIELD).asText()));
            Document featureBson = Document.parse(om.writeValueAsString(feature));
            List<Bson> updates = new ArrayList<>();
            updates.add(Updates.setOnInsert(GEOMETRY_FIELD, featureBson.get(GEOMETRY_FIELD)));
            updates.add(Updates.setOnInsert(PROPERTIES_FIELD, featureBson.get(PROPERTIES_FIELD)));
            updates.add(Updates.setOnInsert(APPLICATION_FIELD, featureBson.get(APPLICATION_FIELD)));
            updates.add(Updates.setOnInsert(INSTANCE_FIELD, instance));
            updates.add(Updates.setOnInsert("type", "Feature"));
            updates.add(Updates.setOnInsert(VISIBILITY_LAST_UPDATE_TIMESTAMP_FIELD, feature.path(PROPERTIES_FIELD).path(TIMESTAMP_FIELD).asLong()));
            writeList.add(new UpdateOneModel<>(Filters.and(filters), Updates.combine(updates), new UpdateOptions().upsert(true)));
        }

        featuresCollection.bulkWrite(dbSession,writeList, new BulkWriteOptions().ordered(false));
    }

    /**
     * Updates the references for the newly inserted events.
     * The reference of the feature is added to the event, why? In order to simplify the some API search like by "reference concept"
     * @param instance The domain name of the OTM instance
     * @throws MongoTimeoutException If a problem occurred while connecting to the database
     */
    private void updateReferences(String instance,ClientSession dbSession) throws MongoTimeoutException {
        MongoDatabase database = mongoDBController.getDatabase();
        MongoCollection<BsonDocument> collection = database.getCollection(configuration.getString("ontomap.logger.mongodb.collection"), BsonDocument.class);
        List<Bson> filters = new ArrayList<>();
        filters.add(Filters.eq(INSTANCE_FIELD, instance));
        filters.add(Filters.exists(REFERENCES_FIELD));
        // only the events without a feature in the references field are to be updated
        filters.add(Filters.not(Filters.exists(REFERENCES_FEATURE_FIELD)));
        FindIterable<BsonDocument> findIterable = collection.find(dbSession,Filters.and(filters));
        List<WriteModel<BsonDocument>> writeList = new ArrayList<>();

        // iterate over the events to update
        try(MongoCursor<BsonDocument> cursor = findIterable.iterator()) {
            while (cursor.hasNext()) {
                BsonDocument event = cursor.next();
                boolean edited = false;
                if (event.isArray(REFERENCES_FIELD)) {

                    // for each reference...
                    for (BsonValue bsonValue : event.getArray(REFERENCES_FIELD)) {
                        BsonDocument reference = bsonValue.asDocument();
                        // ...check if it has to be updated...
                        if (reference.get(FEATURE_FIELD) == null) {
                            // ...and get the right version of the feature
                            BsonDocument feature = getSingleFeature(
                                    instance,
                                    reference.getString(APPLICATION_FIELD).getValue(),
                                    reference.getString(EXTERNAL_URL_FIELD).getValue(), event.getNumber(TIMESTAMP_FIELD).longValue());

                            if (feature != null) {
                                reference.put(FEATURE_FIELD, feature);
                                edited = true;
                            }
                        }
                    }
                    // if a change occurred, schedule the update of the event
                    if (edited) {
                        writeList.add(new ReplaceOneModel<>(Filters.eq("_id", event.get("_id")), event));
                    }
                }

            }
        }
        // if there are events to update, perform the operation
        if (!writeList.isEmpty()) collection.bulkWrite(dbSession,writeList);
    }

    /**
     * Sets the hidden state of a specific feature.
     *
     * @param instance The domain name of the OTM instance
     * @param application The source application of the feature
     * @param externalUrl The external url of the feature
     * @param timestamp The timestamp of the event
     * @param hidden The hidden state to set
     * @param dbSession The client session of the db transaction
     * @throws MongoTimeoutException If a problem occurred while connecting to the database
     */
    private void setFeaturesVisibility(String instance, String application, String externalUrl, long timestamp, boolean hidden, ClientSession dbSession) throws MongoTimeoutException {
        MongoDatabase database = mongoDBController.getDatabase();
        MongoCollection<Document> featuresCollection = database.getCollection(configuration.getString("ontomap.logger.mongodb.features.collection"));

        List<Bson> filters = new ArrayList<>();
        filters.add((Filters.eq(INSTANCE_FIELD, instance)));
        filters.add(Filters.eq(APPLICATION_FIELD, application));
        filters.add(Filters.eq(PROPERTIES_EXTERNAL_URL_FIELD, externalUrl));
        filters.add(Filters.lte(VISIBILITY_LAST_UPDATE_TIMESTAMP_FIELD, timestamp));

        Bson update = (hidden) ? Updates.set(HIDDEN_FIELD, true) : Updates.unset(HIDDEN_FIELD);
        UpdateResult res = featuresCollection.updateMany(dbSession,Filters.and(filters), Updates.combine(update, Updates.set(VISIBILITY_LAST_UPDATE_TIMESTAMP_FIELD, timestamp)));
//        Logger.debug(res.toString());

        //check if some row was modified: the application may not be the owner of the feature.
        // if the application it's the owner of the feature, I update all the events containing this feature
        if(res.getModifiedCount()>0) {
            filters.clear();
            filters.add((Filters.eq(INSTANCE_FIELD, instance)));
            filters.add(Filters.eq(ACTIVITY_OBJECTS_PROPERTIES_EXTERNAL_URL_FIELD, externalUrl));

            MongoCollection<BsonDocument> eventCollection = database.getCollection(configuration.getString("ontomap.logger.mongodb.collection"), BsonDocument.class);
            String visibility = hidden ? HIDDEN : VISIBLE;
            eventCollection.updateMany(dbSession,Filters.and(filters), Updates.set(ACTIVITY_OBJECTS_CURRENT_VISIBILITY, visibility));
        }

    }

    /**
     * Check in the feature is hidden or visible
     * @param instance      The domain name of the OTM instance
     * @param application   he source application of the feature
     * @param externalUrl   the external url of the feature
     * @return              <i>true</i> if the feature is hidden, <i>false</i> if not
     */
    private boolean isFeatureHidden(String instance,String application,String externalUrl) throws MongoTimeoutException {
        MongoDatabase database = mongoDBController.getDatabase();
        MongoCollection<Document> featuresCollection = database.getCollection(configuration.getString("ontomap.logger.mongodb.features.collection"));

        List<Bson> filters = new ArrayList<>();
        filters.add((Filters.eq(INSTANCE_FIELD, instance)));
        filters.add(Filters.eq(APPLICATION_FIELD, application));
        filters.add(Filters.eq(PROPERTIES_EXTERNAL_URL_FIELD, externalUrl));
        FindIterable<Document> found = featuresCollection.find(Filters.and(filters));
        Document feature = found.first();
        if(feature== null)
            return false;
        return (Boolean) found.first().getOrDefault("hidden",false);
    }

    /**
     * This metod is used to make the "visibility_status" update retroactive.
     * It was used to make consistent the db.
     */
    /*public void updateFeatureVisiblityIntoEvents(){
        MongoDatabase database = mongoDBController.getDatabase();
        MongoCollection<Document> featuresCollection = database.getCollection(configuration.getString("ontomap.logger.mongodb.features.collection"));
        MongoCollection<Document> eventCollection = database.getCollection(configuration.getString("ontomap.logger.mongodb.collection"));

        lock.writeLock().lock();
        try(ClientSession dbSession = mongoDBController.getSession()) {

            dbSession.startTransaction();
            List<Bson> filters = new ArrayList<>();

            FindIterable<Document> findIterable = featuresCollection.find(dbSession);

            try (MongoCursor<Document> cursor = findIterable.iterator()) {
                while (cursor.hasNext()) {
                    Document feature = cursor.next();

                    String visibility = (Boolean) feature.getOrDefault(HIDDEN, false) ? HIDDEN : VISIBLE;
                    String instance = feature.getString(INSTANCE_FIELD);
                    String externalUrl = ((Document) feature.get(PROPERTIES_FIELD)).getString(EXTERNAL_URL_FIELD);

                    filters.clear();
                    filters.add((Filters.eq(INSTANCE_FIELD, instance)));
                    filters.add(Filters.eq(ACTIVITY_OBJECTS_PROPERTIES_EXTERNAL_URL_FIELD, externalUrl));
                    UpdateResult res = eventCollection.updateMany(dbSession,Filters.and(filters), Updates.set(ACTIVITY_OBJECTS_CURRENT_VISIBILITY, visibility));
                }

                dbSession.commitTransaction();

            } catch (Exception e){
                dbSession.abortTransaction();
                throw e;
            }finally {
                dbSession.close();
            }
        } finally {
            lock.writeLock().unlock();
        }
    }*/



    /**
     * Checks if all the properties of a JsonNode or Array respect some requirements (for now, a property name must not start with
     * "$" and must not contain a dot). This is a recursive method.
     * @param node The JsonNode to analyze
     * @return the name of the first invalid property name found, or null if all the property names are valid.
     */
    private String checkPropertyNames(JsonNode node) { // returns name of the first invalid property (null if all valid)
        if (node == null) return null;
        if (node.isObject()) {
            for(Iterator<Map.Entry<String, JsonNode>> iterator = node.fields(); iterator.hasNext();) {
                Map.Entry<String, JsonNode> entry = iterator.next();
                if (entry.getKey().startsWith("$") || entry.getKey().contains("."))
                    return entry.getKey();
                String invalid = checkPropertyNames(entry.getValue());
                if (!Strings.isNullOrEmpty(invalid)) return invalid;
            }
        }
        else if(node.isArray()) {
            for(Iterator<JsonNode> iterator = node.elements(); iterator.hasNext();) {
                String invalid = checkPropertyNames(iterator.next());
                if (!Strings.isNullOrEmpty(invalid)) return invalid;
            }
        }
        return null;
    }

    /**
     * Transforms a list of coordinates into a BasicDBObject representing a polygon.
     * @param coordinates A list of coordinates; see {@link TripleStoreController#boundingBoxToCoords(String)}
     * @return A BasicDBObject representing a polygon
     */
    private static BasicDBObject coordinatesToPolygon(List<Double> coordinates) {
        BasicDBObject geometry = new BasicDBObject();
        geometry.put("type", "Polygon");

        BasicDBList coordsList = new BasicDBList();
        coordsList.add(new BasicDBList());
        coordsList.add(new BasicDBList());
        coordsList.add(new BasicDBList());
        coordsList.add(new BasicDBList());
        coordsList.add(new BasicDBList());

        ((BasicDBList) coordsList.get(0)).add(coordinates.get(2));
        ((BasicDBList) coordsList.get(0)).add(coordinates.get(1));

        ((BasicDBList) coordsList.get(1)).add(coordinates.get(0));
        ((BasicDBList) coordsList.get(1)).add(coordinates.get(1));

        ((BasicDBList) coordsList.get(2)).add(coordinates.get(0));
        ((BasicDBList) coordsList.get(2)).add(coordinates.get(3));

        ((BasicDBList) coordsList.get(3)).add(coordinates.get(2));
        ((BasicDBList) coordsList.get(3)).add(coordinates.get(3));

        ((BasicDBList) coordsList.get(4)).add(coordinates.get(2));
        ((BasicDBList) coordsList.get(4)).add(coordinates.get(1));

        geometry.put("coordinates", new BasicDBList());
        ((BasicDBList) geometry.get("coordinates")).add(coordsList);
        return geometry;
    }

    /**
     * Reads a distance string and returns a point/distance pair representing a circle.
     * @param distanceString  A string representing the center coordinates and radius (in meters) of a circle.
     *                        Example: "7.4,45.1,200"
     * @return A Pair composed by a Bson object (the circle center) and a Double (the circle radius)
     */
    private static Pair<Bson, Double> distancePairFromString(String distanceString) {
        if (Strings.isNullOrEmpty(distanceString)) return null;
        List<String> coords = Splitter.on(",").splitToList(distanceString);
        if (coords.size() != 3) return null;
        Double lng = Doubles.tryParse(coords.get(0));
        Double lat = Doubles.tryParse(coords.get(1));
        Double distance = Doubles.tryParse(coords.get(2));
        if (lng == null || lat == null || distance == null) return null;
        BsonDocument geometry = new BsonDocument();
        geometry.put("type", new BsonString("Point"));
        BsonArray coordsArray = new BsonArray();
        coordsArray.add(new BsonDouble(lng));
        coordsArray.add(new BsonDouble(lat));
        geometry.put("coordinates", coordsArray);
        return Pair.of(geometry, distance);
    }

    /**
     * If an application wants events using TOKEN instead of the certificate validation, the system will
     * "clean" all the events from the private data like the actor, description.
     *
     * @param event the event to clean
     * @param aggregate true if it's an aggregate search
     */
    private void cleanEventForTokenRequest(ObjectNode event,boolean aggregate){
        List<String> availableDetails =  Arrays.asList("name");
        if(event.has("actor")){
            event.put("actor", (String) null);
        }
        if(event.has(ACTIVITY_OBJECTS_FIELD)){
            if(aggregate){
                JsonNode act_obj = event.get(ACTIVITY_OBJECTS_FIELD);
                cleanFeatureForTokenRequest(act_obj);
            }
            else for(JsonNode act_obj : event.get(ACTIVITY_OBJECTS_FIELD)){
                    cleanFeatureForTokenRequest(act_obj);
            }
        }
        if(event.has(REFERENCES_FIELD)){
            event.remove(REFERENCES_FIELD);
        }
        if(event.has(DETAILS_FIELD)){
            for (Iterator<String> detail = event.get(DETAILS_FIELD).fieldNames(); detail.hasNext(); ) {
                String detailKey = detail.next();
                if(!availableDetails.contains(detailKey)){
                    ((ObjectNode)event.get(DETAILS_FIELD)).put(detailKey, (String)null);
                }
            }
        }
        if(event.has(VISIBILITY_DETAILS_FIELD)){
            event.remove(VISIBILITY_DETAILS_FIELD);
        }
    }

    /**
     * If an application wants events using TOKEN instead of the certificate validation, the system will
     * "clean" all the events from the private data like the actor, description.
     *
     * @param act_obj the feature to change
     */
    private void cleanFeatureForTokenRequest(JsonNode act_obj){
        List<String> availableProps = Arrays.asList(ADDITIONAL_PROPERTIES_FIELD,"hasType","hasAddress","hasName", "external_url");
        List<String> availableAdditionalProps=  Arrays.asList("name","address","zoom_level");
        if(act_obj.has(PROPERTIES_FIELD)) {
            JsonNode props = act_obj.get(PROPERTIES_FIELD);
            for (Iterator<String> it = props.fieldNames(); it.hasNext(); ) {
                String key = it.next();
                if(!availableProps.contains(key)){
                    ((ObjectNode)props).put(key, (String)null);
                }
            }
            if (props.has(ADDITIONAL_PROPERTIES_FIELD)) {
                for (Iterator<String> addProp = props.get(ADDITIONAL_PROPERTIES_FIELD).fieldNames(); addProp.hasNext(); ) {
                    String keyAdds = addProp.next();
                    if(!availableAdditionalProps.contains(keyAdds)){
                        ((ObjectNode)props.get(ADDITIONAL_PROPERTIES_FIELD)).put(keyAdds, (String)null);
                    }
                }
            }
        }
    }
}
