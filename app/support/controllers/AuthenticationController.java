package support.controllers;

import com.google.common.base.Splitter;
import joptsimple.internal.Strings;
import play.Environment;
import play.Logger;
import play.mvc.Http;

import javax.inject.Inject;
import javax.naming.InvalidNameException;
import javax.naming.ldap.Rdn;
import java.util.List;

/**
 * This class contains the internal methods used to verify the authentication status of the clients performing the requests.
 * In order to verify the client authentication, two request headers, which are set by the reverse proxy, are used.
 * The headers checked by this class are setted by nginx checking if the application has a certificate
 */
public class AuthenticationController {

    private Environment environment;

    @Inject
    public AuthenticationController(Environment environment) {
        this.environment = environment;
    }

    /**
     * Checks the authentication status of an HTTP request.
     * @param request The HTTP request to verify
     * @return The CN of the client if successfully authenticated; null otherwise.
     */
    public String checkAuthentication(Http.Request request) {
        // if in dev environment, uses the X-Ontomap-Application to set the application. Defaults to ontomap.eu
        // this appened becouse in local dev you don't have nginx to set headers
        if (environment.isDev())  {
            String application = request.getHeader("X-Ontomap-Application");
            return (Strings.isNullOrEmpty(application))
                    ? "ontomap.eu"
                    : application;
        }
        // Auth status; "SUCCESS" if a valid client certificate is used.
        // this flag is the response of $ssl_client_verify: http://nginx.org/en/docs/http/ngx_http_ssl_module.html#var_ssl_client_verify
        String authStatus = request.getHeader("X-Ontomap-AuthStatus");
        // Distinguished Name from the client certificate
        // this flag is the response of $ssl_client_s_dn: http://nginx.org/en/docs/http/ngx_http_ssl_module.html#var_ssl_client_s_dn
        String authDN = request.getHeader("X-Ontomap-DN");
        if (authStatus == null || !authStatus.equalsIgnoreCase("SUCCESS") || authDN == null) return null;

        // The CN field is extracted
        // this becouse X-Ontomap-DN is like ""/DC=Demo/OU=Demo/OU=Demo/CN=ontomap.eu"" but we need only the CN part
        List<String> stringList = Splitter.on("/").splitToList(authDN);
        try {
            for (String s : stringList) {
                Rdn rdn = new Rdn(s);
                if (rdn.size() > 0 && rdn.getType().equalsIgnoreCase("cn")) {
                    return (String) rdn.getValue();
                }
            }
            return null;
        }
        catch (InvalidNameException e) {
            Logger.error("InvalidNameException in checkAuthentication: ", e);
            return null;
        }
    }

    /**
     * Returns the domain name of the OTM instance managing the request.
     * @param request The HTTP request to verify
     * @return The domain name of the OTM instance managing the request.
     */
    public String getOTMInstance(Http.Request request) {
        if (environment.isDev()) {
            String instance = request.getHeader("X-Ontomap-Instance");
            return (Strings.isNullOrEmpty(instance))
                    ? "api.ontomap.eu"
                    : instance;
        }
        return request.getHeader("X-Instance");
    }
}
