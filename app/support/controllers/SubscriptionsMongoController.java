package support.controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.mongodb.MongoClient;
import com.mongodb.MongoException;
import com.mongodb.client.*;
import com.mongodb.client.model.Projections;
import com.mongodb.client.model.ReplaceOptions;
import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;
import org.bson.BsonDocument;
import org.bson.Document;
import org.bson.types.ObjectId;
import play.Configuration;
import play.Logger;
import play.libs.Json;
import play.libs.ws.WSClient;
import play.libs.ws.WSResponse;
import support.BsonSerializer;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.ExecutionException;

public class SubscriptionsMongoController {

    @Inject
    private Configuration configuration;

    @Inject
    private MongoDBController mongoDBController;

    @Inject
    private WSClient ws;

    /**
     * Verifies if one or more subscriptions for a specific event. A subscription is composed by an event source application
     * and activity type and an URL; when an event matching the subscription is submitted a POST request is performed to the
     * specified URL.
     *
     * @param instance The OTM instance
     * @param application The events source application
     * @param activityType The events activity type
     * @return A list of all URLs to wich a POST has to be sent. If no subscription for the requested event exists, an empty list is returned.
     */
    public List<String> verifySubscription(String instance, String application, String activityType) {
        MongoDatabase database = mongoDBController.getDatabase();
        MongoCollection<BsonDocument> collection = database.getCollection(configuration.getString("ontomap.logger.mongodb.subscriptions.collection"), BsonDocument.class);
        List<String> result = new ArrayList<>();

        FindIterable<BsonDocument> findIterable = collection.find(new Document().append("instance", instance).append("application", application).append("activity_type", activityType));
        findIterable.projection(Projections.include("url"));
        try (MongoCursor<BsonDocument> cursor = findIterable.iterator()) {
            while (cursor.hasNext()) {
                BsonDocument subscription = cursor.next();
                result.add(subscription.getString("url").getValue());
            }
        }
        return result;
    }

    public void checkEvents(String instance, List<Document> events) {
        for (Document event : events) {
            List<String> urls = verifySubscription(instance, event.getString("application"), event.getString("activity_type"));
            for (String url : urls) {
                sendNotification(event, url);
            }
        }
    }

    public boolean sendNotification(Document document, String url) {
        JsonNode event = Json.newObject()
                .put("id", document.getObjectId("_id").toHexString())
                .put("application", document.getString("application"))
                .put("activity_type", document.getString("activity_type"))
                .put("timestamp", document.getLong("timestamp")); // url
        CompletionStage<WSResponse> csResponse = ws.url(url).setRequestTimeout(5000).post(event);
        try {
            int statusCode = csResponse.thenApply(WSResponse::getStatus).toCompletableFuture().get();
            if (statusCode == 200) {
                Logger.debug("Request OK");
                return true;
            }
            else {
                Logger.debug("Request not OK");
                return false;
            }
        }
        catch (InterruptedException e) {
            Logger.error("InterruptedException", e);
            return false;
        }
        catch (ExecutionException e) {
            Logger.error("ExecutionException", e);
            return false;
        }
    }

    /**
     * Returns the subscriptions associated with a specific application in a specific OTM instance.
     * @param instance The OTM instance
     * @param sourceApplication The source application
     * @return An ObjectNode containing an array of objects, each representing a subscription
     */
    public ObjectNode getSubscriptions(String instance, String sourceApplication) {
        ObjectMapper om = new ObjectMapper();
        SimpleModule sm = new SimpleModule();
        sm.addSerializer(BsonDocument.class, new BsonSerializer());
        om.registerModule(sm);
        MongoDatabase database = mongoDBController.getDatabase();
        MongoCollection<BsonDocument> collection = database.getCollection(configuration.getString("ontomap.logger.mongodb.subscriptions.collection"), BsonDocument.class);
        FindIterable<BsonDocument> findIterable = collection.find(new Document().append("instance", instance).append("source_application", sourceApplication));
        findIterable.projection(Projections.exclude("instance", "source_application"));
        ObjectNode result = om.createObjectNode();
        result.withArray("subscriptions");
        try (MongoCursor<BsonDocument> cursor = findIterable.iterator()) {
            while (cursor.hasNext()) {
                ObjectNode sub = om.valueToTree(cursor.next());
                sub.set("id", sub.get("_id"));
                sub.remove("_id");
                result.withArray("subscriptions").add(sub);
            }
        }
//        Logger.debug(result.toString());
        return result;
    }

    /**
     * Adds/updates a subscription from a specific application and relative to a specific OTM instance. At most one subscription
     * from an application to events coming from a specific app and with a specific type can exist. If a subscription is submitted
     * and one with the same application/activity type coming from the same app already exists, it is overwritten.
     * @param instance The OTM instance
     * @param application The events application
     * @param activityType The events activity type
     * @param sourceApplication The application which performs the subscription
     * @param url The URL to which the notifications are to be sent
     * @return True if the subscription is successfully saved, false otherwise
     */
    public ObjectNode addSubscription(String instance, String application, String activityType, String sourceApplication, String url) {
        MongoDatabase database = mongoDBController.getDatabase();
        MongoCollection<Document> collection = database.getCollection(configuration.getString("ontomap.logger.mongodb.subscriptions.collection"));
        ObjectMapper om = new ObjectMapper();
        SimpleModule sm = new SimpleModule();
        sm.addSerializer(BsonDocument.class, new BsonSerializer());
        om.registerModule(sm);
        Document subscription = new Document()
                .append("instance", instance)
                .append("application", application)
                .append("activity_type", activityType)
                .append("source_application", sourceApplication)
                .append("url", url);
        try(ClientSession dbSession = mongoDBController.getSession()) {
            dbSession.startTransaction();
            try {
                UpdateResult res = collection.replaceOne(dbSession,
                        new Document()
                                .append("instance", instance)
                                .append("application", application)
                                .append("source_application", sourceApplication)
                                .append("activity_type", activityType),
                        subscription,
                        new ReplaceOptions().upsert(true)
                );
                dbSession.commitTransaction();
                if (res.getModifiedCount() < 1 && res.getUpsertedId() == null) return null;
                Document resDoc = collection.find(subscription).first();
                BsonDocument resBson = resDoc.toBsonDocument(BsonDocument.class, MongoClient.getDefaultCodecRegistry());
                ObjectNode resJson = om.valueToTree(resBson);
                resJson.set("id", resJson.get("_id"));
                resJson.remove("_id");
                resJson.remove("instance");
                resJson.remove("source_application");
                return resJson;
            } catch (MongoException e) {
                dbSession.abortTransaction();
                return null;
            } finally {
                dbSession.close();
            }
        }
    }

    /**
     * Deletes a subscription.
     * @param instance The OTM instance
     * @param application The events application
     * @param id The event id
     * @return True if a subscription was deleted, false otherwise.
     */
    public boolean deleteSubscription(String instance, String application, String id) {
        MongoDatabase database = mongoDBController.getDatabase();
        MongoCollection<Document> collection = database.getCollection(configuration.getString("ontomap.logger.mongodb.subscriptions.collection"));
        ObjectId objectId;

        try {
            objectId = new ObjectId(id);
        }
        catch (IllegalArgumentException e) {
            Logger.error("id problem");
            return false;
        }
        Document filter = new Document()
                .append("instance", instance)
                .append("source_application", application)
                .append("_id", objectId);

        try(ClientSession dbSession = mongoDBController.getSession()){
            dbSession.startTransaction();
            try {
                DeleteResult res = collection.deleteOne(dbSession, filter);
                dbSession.commitTransaction();
                return res.getDeletedCount() > 0;
            }catch (MongoException e){
                dbSession.abortTransaction();
                return false;
            }
            finally {
                dbSession.close();
            }
        }
    }
}
