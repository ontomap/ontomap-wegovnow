package support.controllers;

import com.esri.core.geometry.Geometry;
import com.esri.core.geometry.OperatorExportToGeoJson;
import com.esri.core.geometry.OperatorImportFromWkt;
import com.esri.core.geometry.WktImportFlags;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.common.base.Joiner;
import com.google.common.base.Splitter;
import com.google.common.base.Stopwatch;
import com.google.common.base.Strings;
import com.google.common.primitives.Doubles;
import org.apache.jena.query.*;
import org.apache.jena.rdf.model.*;
import org.apache.jena.sparql.engine.http.QueryExceptionHTTP;
import play.Configuration;
import play.Logger;
import support.ConceptResult;
import support.QueryHelper;

import javax.inject.Inject;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;

public class TripleStoreController {

    private Configuration configuration;
    private OntologyController ontologyController;
    private QueryHelper queryHelper;

    public static final String RDF_TYPE = "http://www.w3.org/1999/02/22-rdf-syntax-ns#type";
    public static final String GEOSPARQL_DEFAULT_GEOMETRY = "http://www.opengis.net/ont/geosparql#defaultGeometry";
    public static final String GEOSPARQL_AS_WKT = "http://www.opengis.net/ont/geosparql#asWKT";
    public static final String RDFS_LABEL = "http://www.w3.org/2000/01/rdf-schema#label";
    public static final String RDFS_COMMENT = "http://www.w3.org/2000/01/rdf-schema#comment";



//    public static final String WKT_CRS = "<http://www.opengis.net/def/crs/OGC/1.3/CRS84> ";
    public static final String CRSRegex = "<http:\\/\\/www\\.opengis\\.net\\/def\\/crs\\/.+> ";

    @Inject
    public TripleStoreController(Configuration configuration, OntologyController ontologyController, QueryHelper queryHelper) {
        this.configuration = configuration;
        this.ontologyController = ontologyController;
        this.queryHelper = queryHelper;
    }

    public ConceptResult getConceptInstances(String appInstance, Set<String> concepts, boolean descriptions,
                                             boolean geom, String boundingBoxString,
                                             String validFrom, String validTo,
                                             String lang, int limit, int page,
                                             boolean saveGeometries) throws FileNotFoundException {
        String baseURI = configuration.getString("ontomap.ontology.baseuri");
        String endpoint = configuration.getString("ontomap.triplestore.sparqlendpoint");
        String ontomapDomain = configuration.getString("ontomap.application.domain");
        int defaultPageSize = configuration.getInt("ontomap.application.defaultpagesize");
        boolean showTimers = configuration.getBoolean("ontomap.debug.showtimers");

        Stopwatch timer = Stopwatch.createUnstarted();

        String sparqlQueryString =
                "DESCRIBE ?i WHERE {\n" +
                        "    ?i a ?class.\n" +
                        "    ?i geo:defaultGeometry ?geom.\n" +
                        "    ?geom geo:asWKT ?wkt.\n" +
                        "    ?i <applicationInstance> ?instance.\n" +
                        "    VALUES ?class {%1$s}" +
                        "    %2$s\n" +
                        "}\n" +
                        "ORDER BY ASC(?i) %3$s\n";

        String filterString = generateFilterString(boundingBoxString, validFrom, validTo, getAppInstance(appInstance));

        String limitOffset = ""; // todo: delete (pagination done on InstancesController)
//        int pageSize;
//        if (page > -1) {
//            pageSize = (limit > 0 && limit <= 500) ? limit : defaultPageSize;
////            limitOffset = String.format("OFFSET %d LIMIT %d", page * pageSize, pageSize); // todo: delete (pagination done on InstancesController)
//        }

        sparqlQueryString = String.format(sparqlQueryString, OntologyController.getConceptValuesString(concepts), filterString, limitOffset);

        ParameterizedSparqlString pss = queryHelper.getParameterizedQuery(sparqlQueryString);
//        pss.setIri("class", baseURI + concept);

        Query query = pss.asQuery();
//        Logger.debug(query.toString());
        if (geom) query.addResultVar("geom");
        QueryExecution queryExecution = QueryExecutionFactory.sparqlService(endpoint, query);
        ObjectMapper om = new ObjectMapper();
        ObjectNode featureCollection = om.createObjectNode();
        featureCollection.put("type", "FeatureCollection");
        ArrayList<ObjectNode> features = new ArrayList<>();

        timer.start();
//        Logger.debug(query.toString());
        Model result = queryExecution.execDescribe(ontologyController.getOntologyModel());
        timer.stop();
        if (showTimers) Logger.info("Describe: {}", timer.toString());
        timer.reset();

        Property typeProperty = ResourceFactory.createProperty(RDF_TYPE);
        Property asWKTProperty = ResourceFactory.createProperty(GEOSPARQL_AS_WKT);
        Stopwatch geoTimer = Stopwatch.createUnstarted();
        Map<String, Geometry> geometryMap = null;
        if (saveGeometries) geometryMap = new HashMap<>();
        for (String concept: concepts) {
            RDFNode classNode = ResourceFactory.createResource(baseURI + concept);
            ResIterator resIterator = result.listResourcesWithProperty(typeProperty, classNode);
            timer.start();
            while (resIterator.hasNext()) {
                Resource instance = resIterator.nextResource();
                ObjectNode feature = om.createObjectNode();
                feature.put("type", "Feature");
                feature.put("id", instance.getURI());
                feature.put("application", ontomapDomain);
                feature.put("applicationName", "OnToMap");
                feature.putNull("geometry");
                ObjectNode properties = feature.putObject("properties");
                properties.put("external_url", getResourceURL(instance.getURI()));
                properties.put("hasType", concept);
                StmtIterator propIterator = result.listStatements(instance, null, (RDFNode) null);
                ObjectNode labelObject = om.createObjectNode();

                //properties
                while (propIterator.hasNext()) { // TODO: multiple values?
                    Statement statement = propIterator.nextStatement();
                    /*if (statement.getPredicate().getURI().equals(baseURI + "applicationInstance")) {
                        continue;
                    }*/ // todo: remove this parameter (but cachecontroller must receive it)

                    if (statement.getPredicate().getURI().equals(RDFS_LABEL)) {
                        if (Strings.isNullOrEmpty(lang)) {
                            labelObject.put(statement.getObject().asLiteral().getLanguage(), statement.getObject().asLiteral().getString());
                        }
                        else if (lang.equals(statement.getObject().asLiteral().getLanguage()))
                            properties.put("label", statement.getObject().asLiteral().getString());
                    }
                    else if (statement.getPredicate().getURI().equals(baseURI + "hasCurrentProvenance")) {
                        feature.put("provenance", statement.getObject().asResource().getURI());
                    }
                    else if (descriptions && statement.getObject().isLiteral()) {
                        properties.put(statement.getPredicate().getURI().replace(baseURI, ""), statement.getObject().asLiteral().getLexicalForm()); //todo: types
                    }
                    else if (geom && statement.getPredicate().getURI().equals(GEOSPARQL_DEFAULT_GEOMETRY)) {
                        NodeIterator geomIterator = result.listObjectsOfProperty(statement.getObject().asResource(), asWKTProperty);
                        geoTimer.start();
                        Geometry geometry = OperatorImportFromWkt.local().execute(
                                WktImportFlags.wktImportDefaults,
                                Geometry.Type.Unknown,
                                geomIterator.next().asLiteral().getString().replaceAll(CRSRegex, ""),
                                null);
                        JsonNode geoJSONgeometry = null;
                        try {
                            geoJSONgeometry = om.readTree(
                                    OperatorExportToGeoJson.local().execute(geometry));
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        geoTimer.stop();
                        if (saveGeometries) geometryMap.put(instance.getURI(), geometry);
                        feature.set("geometry", geoJSONgeometry);
                    }
                }
                if (Strings.isNullOrEmpty(lang)) properties.set("label", labelObject);
                features.add(feature);
            }
            timer.stop();
        }

        if (showTimers) Logger.info("ResIterator: {} (geometry processing: {})", timer.toString(), geoTimer.toString());
        timer.reset();
//                StringWriter sw = new StringWriter();
//                result.write(sw, "TURTLE", baseURI);
//                return ok(sw.toString());
//        Logger.info("Feature count (SPARQL): {}", features.size());
//        timer.start();
//        features.sort(new FeatureComparator());
//        timer.stop();
        featureCollection.withArray("features").addAll(features);
//        if (showTimers) Logger.info("Sorting: {}", timer.toString());
//        timer.reset();
        return new ConceptResult(geometryMap, featureCollection);
    }

    private static String generateFilterString(String boundingBoxString, String validFrom, String validTo, String instance) {
        String filterTemplate = "FILTER(%s)";
        ArrayList<String> filters = new ArrayList<>();

        if (!Strings.isNullOrEmpty(boundingBoxString)) {
            String bboxWkt = boundingBoxToWkt(boundingBoxString);
            if (!Strings.isNullOrEmpty(bboxWkt)) {
                filters.add(String.format("(geof:sfIntersects(?wkt, \"%s\"^^geo:wktLiteral))", bboxWkt));
//                filters.add(String.format("(bif:st_intersects(?wkt, bif:st_geomfromtext(\"%s\")))", bboxWkt));
            }
            else throw new IllegalArgumentException("boundingbox");
        }

        if (!Strings.isNullOrEmpty(instance)) {
            filters.add(String.format("?instance = \"%s\"", instance));
        }

        // TODO: validFrom, validTo

        if (!filters.isEmpty()) { // insert filters in query
            return String.format(filterTemplate, Joiner.on(" && ").join(filters));
        }
        else return "";
    }

    public static List<Double> boundingBoxToCoords(String boundingBox) {
        if (Strings.isNullOrEmpty(boundingBox)) return null;
        List<String> coords = Splitter.on(",").splitToList(boundingBox);
        ArrayList<Double> coordsList = new ArrayList<>();
        Double temp;
        for (String coord : coords) {
            temp = Doubles.tryParse(coord);
            if (temp == null) return null;
            coordsList.add(temp);
        }
        if (coordsList.size() != 4) return null;
        return coordsList;
    }

    public static String boundingBoxToWkt(String boundingBox) {
        int NE_LNG = 0;
        int NE_LAT = 1;
        int SW_LNG = 2;
        int SW_LAT = 3;

        if (Strings.isNullOrEmpty(boundingBox)) return null;
        List<Double> coordsList = boundingBoxToCoords(boundingBox);
        if (coordsList == null) return null;

        return "POLYGON((" +
                coordsList.get(SW_LNG) +
                " " +
                coordsList.get(NE_LAT) +
                "," +

                coordsList.get(NE_LNG) +
                " " +
                coordsList.get(NE_LAT) +
                "," +

                coordsList.get(NE_LNG) +
                " " +
                coordsList.get(SW_LAT) +
                "," +

                coordsList.get(SW_LNG) +
                " " +
                coordsList.get(SW_LAT) +
                "," +

                coordsList.get(SW_LNG) +
                " " +
                coordsList.get(NE_LAT) +
                "))";
    }

    private String getResourceURL(String resourceURI) throws NoSuchElementException {
        String baseURL = configuration.getString("ontomap.application.baseurl");
        String baseURI = configuration.getString("ontomap.ontology.baseuri");
        Iterator<String> resource = Splitter.on("/").split(resourceURI.replace(baseURI, "")).iterator();
        String concept = resource.next();
        String id = resource.next();
        return baseURL + controllers.api.v1.routes.InstancesController.getInstanceByID(concept, id,null).url();
    }

    public int getConceptInstancesCount(String appInstance, Set<String> concepts, String boundingBoxString, String validFrom, String validTo) {
        String baseURI = configuration.getString("ontomap.ontology.baseuri");
        String endpoint = configuration.getString("ontomap.triplestore.sparqlendpoint");
        String sparqlQueryString = String.format(
                "SELECT (COUNT (DISTINCT ?i) AS ?count) WHERE {\n" +
                        "    ?i a ?class.\n" +
                        "    ?i geo:defaultGeometry ?geom.\n" +
                        "    ?geom geo:asWKT ?wkt.\n" +
                        "    VALUES ?class {%1$s}" +
                        "    %2$s\n" +
                        "}\n",
                OntologyController.getConceptValuesString(concepts),
                generateFilterString(boundingBoxString, validFrom, validTo, getAppInstance(appInstance)));
        ParameterizedSparqlString pss = queryHelper.getParameterizedQuery(sparqlQueryString);
//        pss.setIri("class", baseURI + concept);
        Query query = pss.asQuery();
        QueryExecution queryExecution = QueryExecutionFactory.sparqlService(endpoint, query);
        ResultSet countRS = queryExecution.execSelect();
        if (countRS.hasNext()) {
            return countRS.next().getLiteral("count").getInt();
        }
        else throw new QueryExceptionHTTP(400);
    }

    public ObjectNode getInstanceByID(String concept, String id, String lang) throws IOException {
        String baseURI = configuration.getString("ontomap.ontology.baseuri");
        String endpoint = configuration.getString("ontomap.triplestore.sparqlendpoint");
        String resourceURI = baseURI + concept + "/" + id;
        String sparqlQueryString =
                "DESCRIBE ?i ?subj WHERE {\n" +
                        "    {\n" +
                        "        ?i geo:defaultGeometry ?geom.\n" +
                        "    } UNION {\n" +
                        "        ?subj ?p ?i.\n" +
                        "    }\n" +
                        "}\n";
        ParameterizedSparqlString pss = queryHelper.getParameterizedQuery(sparqlQueryString);
        pss.setIri("i", resourceURI);
        Query query = pss.asQuery();
        query.addResultVar("geom");
        QueryExecution queryExecution = QueryExecutionFactory.sparqlService(endpoint, query);
        Model result = queryExecution.execDescribe();
//        StringWriter sw = new StringWriter();
//        result.write(sw, "TURTLE", baseURI);
//        return ok(sw.toString());
        Resource resource = ResourceFactory.createResource(resourceURI);
        Property asWKTProperty = ResourceFactory.createProperty(GEOSPARQL_AS_WKT);
        ObjectMapper om = new ObjectMapper();
        ObjectNode labelObject = om.createObjectNode();
        ArrayNode relationsArray = om.createArrayNode();
        ObjectNode feature = om.createObjectNode();
        feature.put("id", resourceURI);
        ObjectNode properties = feature.putObject("properties");
        properties.put("external_url", getResourceURL(resourceURI));
        properties.put("hasType", concept);
        StmtIterator resourceAsSubjectIterator = result.listStatements(resource, null, (RDFNode) null);
        if (!resourceAsSubjectIterator.hasNext()) return null;
        while (resourceAsSubjectIterator.hasNext()) {
            Statement statement = resourceAsSubjectIterator.nextStatement();
            if (statement.getPredicate().getURI().equals(RDFS_LABEL)) {
                if (Strings.isNullOrEmpty(lang)) {
                    labelObject.put(statement.getObject().asLiteral().getLanguage(), statement.getObject().asLiteral().getString());
                }
                else if (lang.equals(statement.getObject().asLiteral().getLanguage()))
//                    feature.setProperty("label", statement.getObject().asLiteral().getString());
                    properties.put("label", statement.getObject().asLiteral().getString());
            }
            else if (statement.getPredicate().getURI().equals(baseURI + "applicationInstance")) {
                continue;
            }
            else if (statement.getPredicate().getURI().equals(baseURI + "hasCurrentProvenance")) {
                feature.put("provenance", statement.getObject().asResource().getURI());
            }
            else if (statement.getObject().isLiteral()) {
//                feature.setProperty(statement.getPredicate().getURI().replace(baseURI, ""), statement.getObject().asLiteral().getValue());
                properties.put(statement.getPredicate().getURI().replace(baseURI, ""), statement.getObject().asLiteral().getLexicalForm()); //todo: types
            }
            else if (statement.getPredicate().getURI().equals(GEOSPARQL_DEFAULT_GEOMETRY)) {
                NodeIterator geomIterator = result.listObjectsOfProperty(statement.getObject().asResource(), asWKTProperty);
                JsonNode geometry = om.readTree(
                        OperatorExportToGeoJson.local().execute(
                                OperatorImportFromWkt.local().execute(
                                        WktImportFlags.wktImportDefaults,
                                        Geometry.Type.Unknown,
//                                        geomIterator.next().asLiteral().getString().replace(WKT_CRS, ""),
                                        geomIterator.next().asLiteral().getString().replaceAll(CRSRegex, ""),
                                        null)
                        )
                );
                feature.set("geometry", geometry);
            }
            else if (statement.getObject().isURIResource()
                    && statement.getObject().asResource().getURI().startsWith(baseURI)
                    && statement.getPredicate().asResource().getURI().startsWith(baseURI)
                    && !statement.getPredicate().asResource().getURI().equals(baseURI + "hasCurrentProvenance")) {
//                    feature.setProperty(statement.getPredicate().getURI().replace(baseURI, ""), statement.getObject().asResource().getURI());
                relationsArray.add(om.createObjectNode()
                        .put("subject", resourceURI)
                        .put("predicate", statement.getPredicate().getURI().replace(baseURI, ""))
                        .put("object", statement.getObject().asResource().getURI()));
            }
        }
        StmtIterator resourceAsObjectIterator = result.listStatements(null, null, resource);
        while (resourceAsObjectIterator.hasNext()) {
            Statement statement = resourceAsObjectIterator.nextStatement();
            if (statement.getSubject().getURI().startsWith(baseURI)
                    && statement.getPredicate().getURI().startsWith(baseURI)) {
                relationsArray.add(om.createObjectNode()
                        .put("subject", statement.getSubject().asResource().getURI())
                        .put("predicate", statement.getPredicate().getURI().replace(baseURI, ""))
                        .put("object", resourceURI));
            }
        }
        if (Strings.isNullOrEmpty(lang)) properties.set("label", labelObject);


        // spatial relations
        sparqlQueryString =
                "SELECT ?intersects WHERE {\n" +
                        "    ?i geo:defaultGeometry ?g1.\n" +
                        "    ?intersects geo:defaultGeometry ?g2.\n" +
                        "    ?g1 geo:sfIntersects ?g2.\n" +
                        "    FILTER(?i != ?intersects)\n" +
                        "}\n" +
                        "ORDER BY ASC(?intersects)\n";
        pss = queryHelper.getParameterizedQuery(sparqlQueryString);
        pss.setIri("i", resourceURI);
        query = pss.asQuery();
        queryExecution = QueryExecutionFactory.sparqlService(endpoint, query);
        ResultSet spatialResultSet = queryExecution.execSelect();
        while (spatialResultSet.hasNext()) {
            QuerySolution querySolution = spatialResultSet.next();
            relationsArray.add(om.createObjectNode()
                    .put("subject", resourceURI)
                    .put("predicate", "intersects")
                    .put("object", querySolution.get("intersects").asResource().getURI()));
        }

        properties.set("relations", relationsArray);
        return feature;
    }

    public JsonNode getProvenanceByID(String id) { // todo: more properties
        String baseURI = configuration.getString("ontomap.ontology.baseuri");
        String endpoint = configuration.getString("ontomap.triplestore.sparqlendpoint");
        String resourceURI = baseURI + "Provenance/" + id;
        Resource provenance = ResourceFactory.createResource(resourceURI);
        String sparqlQueryString =
                "DESCRIBE ?p ?ods ?lineage WHERE {\n" +
                        "    {\n" +
                        "        ?p <hasOpenDataSource> ?ods.\n" +
                        "        ?p <hasLineage> ?lineage.\n" +
                        "    }\n" +
                        "}\n";
        ParameterizedSparqlString pss = queryHelper.getParameterizedQuery(sparqlQueryString);
        pss.setIri("p", resourceURI);
        Query query = pss.asQuery();
        QueryExecution queryExecution = QueryExecutionFactory.sparqlService(endpoint, query);
        Model result = queryExecution.execDescribe();
//        StringWriter sw = new StringWriter();
//        result.write(sw, "TURTLE", baseURI);
//        return sw.toString();

        if (!result.containsResource(provenance)) return null;
        Property hasOpenDataSourceProperty = ResourceFactory.createProperty(baseURI + "hasOpenDataSource");
        Property hasLineageProperty = ResourceFactory.createProperty(baseURI + "hasLineage");
        Property labelProperty = ResourceFactory.createProperty(RDFS_LABEL);
        Property commentProperty = ResourceFactory.createProperty(RDFS_COMMENT);
        Statement lineageStatement = result.getProperty(provenance, hasLineageProperty);
        Resource lineage = (lineageStatement == null) ? null : lineageStatement.getObject().asResource();
        Statement openDataSourceStatement = result.getProperty(provenance, hasOpenDataSourceProperty);
        Resource openDataSource = (openDataSourceStatement == null) ? null : openDataSourceStatement.getObject().asResource();
//        Logger.debug("lineage: {}, ods: {}", lineage, openDataSource);
        ObjectMapper om = new ObjectMapper();
        ObjectNode resultJson = om.createObjectNode();
        if (lineage != null)
            resultJson.put("lineage", result.getProperty(lineage, commentProperty).getString());
        if (openDataSource != null)
            resultJson.put("openDataSource", result.getProperty(openDataSource, labelProperty).getString());
        return resultJson;
    }

    public String getAppInstance(String domain) { // todo: extract into module
        if (Strings.isNullOrEmpty(domain)) return null;
        switch (domain) {
            case "torino.api.ontomap.eu":
                return "torino";
            case "sandona.api.ontomap.eu":
                return "sandona";
            case "southwark.api.ontomap.eu":
                return "southwark";
            default:
                return null;
        }
    }
}
