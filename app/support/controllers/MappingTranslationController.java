package support.controllers;

import com.mongodb.MongoTimeoutException;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.MongoIterable;
import org.bson.Document;
import play.Configuration;
import play.Environment;
import play.Logger;
import support.CacheKey;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

@Singleton
public class MappingTranslationController {

    public static final String APPLICATION_FIELD = "application";
    public static final String MAPPINGS_FIELD = "mappings";
    public static final String APP_CONCEPT_FIELD = "app_concept";
    public static final String ONTOMAP_CONCEPT_FIELD = "ontomap_concept";
    public static final String PROPERTIES_FIELD = "properties";
    public static final String APP_PROPERTY_FIELD = "app_property";
    public static final String ONTOMAP_PROPERTY_FIELD = "ontomap_property";
    public static final String UNIT_FIELD = "unit";
    private MongoDBController mongoDBController;
    private Configuration configuration;
    private Environment environment;


    private Map<CacheKey, String> conceptsMap;
    private Map<CacheKey, String> propertiesMap;
    private Map<CacheKey, String> unitsMap;

//    private Table<String, String, String> conceptsMappingTable;
//    private Table<String, String, Map<String, String>> propertiesMappingTable;
//    private Table<String, String, Map<String, String>> unitsMappingTable;
    private ReadWriteLock lock;

    @Inject
    public MappingTranslationController(MongoDBController mongoDBController, Configuration configuration, Environment environment) {
        this.mongoDBController = mongoDBController;
        this.configuration = configuration;
        this.environment = environment;

//        conceptsMappingTable = HashBasedTable.create();
//        propertiesMappingTable = HashBasedTable.create();
//        unitsMappingTable = HashBasedTable.create();

        conceptsMap = new HashMap<>();
        propertiesMap = new HashMap<>();
        unitsMap = new HashMap<>();
        lock = new ReentrantReadWriteLock();

        cacheMappings();

    }

    /**
     * Populates the caches from the DB
     */
    @SuppressWarnings("unchecked")
    public void cacheMappings() {
        Logger.info("(Re) building mapping cache...");
        lock.writeLock().lock();
        conceptsMap.clear();
        propertiesMap.clear();
        unitsMap.clear();
        MongoDatabase database = mongoDBController.getDatabase();
        MongoCollection<Document> collection = database.getCollection(configuration.getString("ontomap.logger.mongodb.mappings.collection"));
        MongoIterable<Document> findIterable = collection.find();

        // iterates over the mapping lists
        try(MongoCursor<Document> cursor = findIterable.iterator()) {
            while(cursor.hasNext()) {
                Document appMapping = cursor.next();
                String application = appMapping.getString(APPLICATION_FIELD);
                String instance = appMapping.getString("instance");
                List<Document> conceptMappings = (List<Document>) appMapping.get(MAPPINGS_FIELD);

                // for each mapping list, iterates over each concept mapping
                for (Document m : conceptMappings) {
                    String appConcept = m.getString(APP_CONCEPT_FIELD);
                    String otmConcept = m.getString(ONTOMAP_CONCEPT_FIELD);
                    conceptsMap.put(new CacheKey(instance, application, appConcept), otmConcept);
//                    Map<String, String> propertiesMap = new HashMap<>();
//                    Map<String, String> unitsMap = new HashMap<>();
                    if (m.containsKey(PROPERTIES_FIELD)) {
                        List<Document> propertiesMappings = (List<Document>) m.get(PROPERTIES_FIELD);

                        // for each concept mapping, iterates over its properties mapping
                        for(Document p : propertiesMappings) {
                            String appProperty = p.getString(APP_PROPERTY_FIELD);
                            String otmProperty = p.getString(ONTOMAP_PROPERTY_FIELD);
//                            propertiesMap.put(appProperty, otmProperty);
                            propertiesMap.put(new CacheKey(instance, application, appConcept, appProperty), otmProperty);
                            if (p.containsKey(UNIT_FIELD))
                                unitsMap.put(new CacheKey(instance, application, appConcept, appProperty), p.getString(UNIT_FIELD));
                        }
//
                    }
                }
            }
        }
        catch(MongoTimeoutException e) {
            Logger.error("MongoTimeoutException in cacheMappings:", e);
            return;
        }
        finally {
            lock.writeLock().unlock();
        }
//        Logger.debug(conceptsMappingTable.toString());
//        Logger.debug(propertiesMappingTable.toString());
        Logger.info("Mapping cache (re)built");
    }

    /**
     * Gets the translation of a specific concept for a specific application
     * @param instance The domain name of the OTM instance
     * @param application The application which sent the mapping
     * @param applicationConcept The concept to translate
     * @return The translation of the concept; null if no appropriate mapping is present
     */
    public String getConceptMapping(String instance, String application, String applicationConcept) {
        lock.readLock().lock();
        String mapping = conceptsMap.get(new CacheKey(instance, application, applicationConcept));
//        String mapping = conceptsMappingTable.get(application, applicationConcept);
        lock.readLock().unlock();
        return mapping;
    }

    /**
     * Gets the translation of a specific property for a specific application
     * @param instance The domain name of the OTM instance
     * @param application The application which sent the mapping
     * @param applicationConcept The concept mapping containing the property
     * @param applicationProperty The property to translate
     * @return The translation of the property; null if no appropriate mapping is present
     */
    public String getPropertyMapping(String instance, String application, String applicationConcept, String applicationProperty) {
        lock.readLock().lock();
//        Map<String, String> propertiesMap = propertiesMappingTable.get(application, applicationConcept);
//        if (propertiesMap == null) {
//            lock.readLock().unlock();
//            return null;
//        }
        String mapping = propertiesMap.get(new CacheKey(instance, application, applicationConcept, applicationProperty));
        lock.readLock().unlock();
        return mapping;
    }

    /**
     * Gets the measure unit of a specific property for a specific application, if it was specified in the mappings
     * @param instance The domain name of the OTM instance
     * @param application The application which sent the mapping
     * @param applicationConcept The concept mapping containing the property
     * @param applicationProperty The property
     * @return The measure unit of the property; null if no appropriate mapping is present
     */
    public String getPropertyUnit(String instance, String application, String applicationConcept, String applicationProperty) {
        lock.readLock().lock();
//        Map<String, String> unitsMap = unitsMappingTable.get(application, applicationConcept);
//        if (unitsMap == null) {
//            lock.readLock().unlock();
//            return null;
//        }
        String unit = unitsMap.get(new CacheKey(instance, application, applicationConcept, applicationProperty));
        lock.readLock().unlock();
        return unit;
    }
}
