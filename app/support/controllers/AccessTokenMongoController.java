package support.controllers;

import com.mongodb.MongoTimeoutException;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.ReplaceOptions;
import com.mongodb.client.result.UpdateResult;
import org.bson.BsonDocument;
import org.bson.BsonString;
import org.bson.Document;
import play.Configuration;

import javax.inject.Inject;
import java.util.Base64;
import java.util.UUID;

/**
 * This class contains the internal methods interacting with MongoDB which concern the generation of access tokens for read-only use of the API.
 * Using this token an application get retrieve events without using certificate so the request can be done from a javascript client.
 */
public class AccessTokenMongoController {
    private MongoDBController mongoDBController;
    private Configuration configuration;

    @Inject
    public AccessTokenMongoController(MongoDBController mongoDBController, Configuration configuration) {
        this.mongoDBController = mongoDBController;
        this.configuration = configuration;
    }

    /**
     * Returns the access token associated with a specific application. If a token for the specified application
     * doesn't exist, it is generated and returned.
     * @param application The domain name of the application for which the token is requested
     * @return The access token associated with the application
     * @throws MongoTimeoutException If a problem occurred while connecting to the database
     */
    public String getToken(String application) throws MongoTimeoutException {
        MongoDatabase database = mongoDBController.getDatabase();
        MongoCollection<BsonDocument> collection = database.getCollection(configuration.getString("ontomap.logger.mongodb.access_tokens.collection"), BsonDocument.class);
        String token;
        BsonDocument tokenEntry = collection.find(new Document().append("application", application)).first();
        if (tokenEntry == null) {
            token = generateToken(application);
        }
        else {
            token = tokenEntry.get("token").asString().getValue();
        }
        return token;
    }

    /**
     * Generates a new access token for a specific application and writes it to the database.
     * @param application The domain name of the application for which the new token is requested
     * @return The access token associated with the application
     * @throws MongoTimeoutException If a problem occurred while connecting to the database
     */
    public String generateToken(String application) throws MongoTimeoutException {
        MongoDatabase database = mongoDBController.getDatabase();
        MongoCollection<BsonDocument> collection = database.getCollection(configuration.getString("ontomap.logger.mongodb.access_tokens.collection"), BsonDocument.class);
        BsonDocument tokenEntry = new BsonDocument();
        String newToken;
        BsonDocument oldEntry;
        // generate random token and check if it already exists in the DB; if so, repeat
        do {
            newToken = Base64.getUrlEncoder().encodeToString(UUID.randomUUID().toString().getBytes());
            oldEntry = collection.find(new Document().append("token", newToken)).first();
        }
        while (oldEntry != null);
        tokenEntry.append("application", new BsonString(application));
        tokenEntry.append("token", new BsonString(newToken));
        UpdateResult res = collection.replaceOne(
                new Document().append("application", application),
                tokenEntry,
                new ReplaceOptions().upsert(true)
        );
        return newToken;
    }

    /**
     * Returns the domain name of the application associated with a specific token, if such association exists.
     * @param token The access token
     * @return The domain name of the application associated with the token if the token is valid, null otherwise
     * @throws MongoTimeoutException If a problem occurred while connecting to the database
     */
    public String verifyToken(String token) throws MongoTimeoutException {
        if (token == null) return null;
        MongoDatabase database = mongoDBController.getDatabase();
        MongoCollection<BsonDocument> collection = database.getCollection(configuration.getString("ontomap.logger.mongodb.access_tokens.collection"), BsonDocument.class);
        BsonDocument tokenEntry = collection.find(new Document().append("token", token)).first();
        if (tokenEntry == null) return null;
        return tokenEntry.get("application").asString().getValue();
    }
}
