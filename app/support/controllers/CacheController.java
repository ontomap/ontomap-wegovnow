package support.controllers;

import com.esri.core.geometry.*;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.common.base.Splitter;
import com.google.common.base.Stopwatch;
import com.google.common.base.Strings;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.primitives.Doubles;
import com.google.common.util.concurrent.UncheckedExecutionException;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.jena.query.*;
import org.json.JSONException;
import play.Configuration;
import play.Logger;
import support.ConceptResult;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.io.FileNotFoundException;
import java.io.UncheckedIOException;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * This class contains the internal methods used to manage the caching of instances from the Triple Store
 */
@Singleton
public class CacheController {

    private Configuration configuration;
    private TripleStoreController tripleStoreController;

    // cache for saving the geometry (converted from WKT to Geometry object) of each concept instance
    private Cache<String, Geometry> geomCache;
    // cache for saving FeatureCollections including all instances of a specific concept
    private Cache<String, ObjectNode> featureCache; // todo: cachekey (instance+class)
    private boolean showTimers;
    private ReentrantReadWriteLock lock;
    private Set<String> concepts;

    @Inject
    public CacheController(Configuration configuration, TripleStoreController tripleStoreController) {
        this.configuration = configuration;
        this.tripleStoreController = tripleStoreController;
        lock = new ReentrantReadWriteLock();
        boolean isCacheEnabled = this.configuration.getBoolean("ontomap.application.cache.enabled");
        concepts = new HashSet<>();
        showTimers = configuration.getBoolean("ontomap.debug.showtimers");
        // building cache at application start
        if (!isCacheEnabled) {
            Logger.info("Cache disabled, skipping initialization");
        }
        else {
            geomCache = CacheBuilder.newBuilder().build();
            featureCache = CacheBuilder.newBuilder().build();
            buildCache();
        }
    }

    /**
     * Asynchronously builds an in-memory cache containing all the instances present in the Triple Store.
     */
    public void buildCache() {
        Logger.info("(Re)building cache...");
        Stopwatch total = Stopwatch.createStarted();
        CompletableFuture<Boolean> initializeCacheFuture = CompletableFuture.supplyAsync(this::initializeCache);
        initializeCacheFuture.handle((res, e) -> {
            total.stop();
            if (e != null) {
                Logger.error("Exception during cache initialization:", e);
                return false;
            }
            else if (res) {
                if (showTimers) Logger.info("Cache initialized successfully ({})", total.toString());
                else Logger.info("Cache initialized successfully");
            }
            else Logger.info("Cache NOT initialized");
            return res;
        });
    }

    /**
     * Builds an in-memory cache containing all the instances present in the Triple Store.
     * @return True if the cache was successfully built, false otherwise.
     */
    private boolean initializeCache() {
        boolean initialized = false;
        if (lock.isWriteLocked()) {
            Logger.info("Cache building already in progress");
            return false;
        }
        lock.writeLock().lock();
        try {
            geomCache.invalidateAll();
            featureCache.invalidateAll();
            String baseURI = configuration.getString("ontomap.ontology.baseuri");
            String endpoint = configuration.getString("ontomap.triplestore.sparqlendpoint");
            Stopwatch timer = Stopwatch.createUnstarted();

            // SPARQL query for getting a list of the classes with associated instances
            String sparqlqueryString =
                    "SELECT DISTINCT ?class WHERE {?x a ?class.}";
            Query query = QueryFactory.create(sparqlqueryString);
            QueryExecution queryExecution = QueryExecutionFactory.sparqlService(endpoint, query);
            ResultSet resultSet = queryExecution.execSelect();
            while (resultSet.hasNext()) {
                QuerySolution querySolution = resultSet.next();
                String conceptURI = querySolution.get("class").asResource().getURI();
                if (conceptURI.startsWith(baseURI)) {
                    concepts.add(conceptURI);
                }
            }

            // for each concept/class retrieve all its istances, store resulting GeoJSON feature and geometry (for efficient bbox filtering)
            for (String conceptURI : concepts) {
                Set<String> conceptsSet = new HashSet<>();
                conceptsSet.add(conceptURI.replace(baseURI, ""));
                ConceptResult conceptResult = tripleStoreController.getConceptInstances(
                        null, conceptsSet, true, true, null, null, null, null, -1, -1, true);
                initialized = true;
                timer.start();
                featureCache.put(conceptURI, conceptResult.getFeatureCollection());
                for (Map.Entry<String, Geometry> geom : conceptResult.getGeometryMap().entrySet()) {
                    geomCache.put(geom.getKey(), geom.getValue());
                }
                timer.stop();
                if (showTimers) Logger.info("Caching {}: {}", conceptURI, timer.toString());
            }
            return initialized;
        }
        catch (FileNotFoundException e) {
            throw new UncheckedIOException(e);
        }
        catch (QueryException e) {
            throw new UncheckedExecutionException(e);
        }
        finally {
            lock.writeLock().unlock();
        }
    }

    /**
     * Returns all concept instances in cache which satisfy the specified filters.
     * @param instance Required, the domain name of the OTM instance
     * @param queryConcepts Required, a set of strings containing all the concepts/classes to retrieve
     * @param descriptions If true includes the complete set of properties for each concept instance
     * @param geom If true includes the geometry for each concept instance
     * @param boundingBoxString A bounding box string, see {@link TripleStoreController#boundingBoxToWkt(String)}
     * @param distanceString A distance string, see {@link #distancePairFromString(String)}
     * @param validFrom A date-time string (not currently supported)
     * @param validTo A date-time string (not currently supported)
     * @param lang A two letter language code; if present, the labels/descriptions are returned only in the selected language
     * @param limit Not used
     * @param page Not used
     * @return A ObjectNode containing a FeatureCollection with the selected features
     * @throws JSONException If a problem with the JSON elaboration occurred
     * @throws FileNotFoundException If the ontology file could not be found
     */
    public ObjectNode getConceptInstances(String instance, Set<String> queryConcepts, boolean descriptions,
                                          boolean geom, String boundingBoxString, String distanceString,
                                          String validFrom, String validTo,
                                          String lang, int limit, int page) throws JSONException, FileNotFoundException {
        String baseURI = configuration.getString("ontomap.ontology.baseuri");
        ObjectMapper om = new ObjectMapper();
        ObjectNode featureCollection = om.createObjectNode();
        featureCollection.put("type", "FeatureCollection");
        featureCollection.putArray("features");
        String instanceParam = tripleStoreController.getAppInstance(instance);
        boolean showTimers = configuration.getBoolean("ontomap.debug.showtimers");
        lock.readLock().lock();
        try {
            // for each concept try the cache; if no results, try the triple store (build cache first)
            for (String concept : queryConcepts) {
                if (concepts.contains(baseURI + concept)) {
                    ObjectNode conceptFeatures = featureCache.getIfPresent(baseURI + concept);
                    if (conceptFeatures == null) {
                        Logger.info("Cache miss, fetching from Triple Store...");
                        lock.readLock().unlock();
                        buildCache();
                        Set<String> conceptsSet = new HashSet<>();
                        conceptsSet.add(concept);
                        conceptFeatures = tripleStoreController.getConceptInstances(null, conceptsSet,
                                descriptions, geom, boundingBoxString, validFrom, validTo, lang, limit, page, false).getFeatureCollection();

                    }

                    // filtering by OTM instance todo: maybe a dedicate cache?
                    // gets all features, then removes those with non-matching instance
                    ArrayNode featuresArray = conceptFeatures.withArray("features").deepCopy();
                    for (Iterator<JsonNode> iter = featuresArray.iterator(); iter.hasNext(); ) {
                        ObjectNode feat = (ObjectNode) iter.next();
                        if (!Strings.isNullOrEmpty(instanceParam) && !instanceParam.equals(feat.with("properties").get("applicationInstance").asText())) {
                            iter.remove();
                        }
                        else
                            // remove instance field from properties
                            feat.with("properties").remove("applicationInstance");
                    }

                    featureCollection.withArray("features").addAll(featuresArray);
                }
            }

//            ObjectNode result = om.createObjectNode();
//            result.put("type", "FeatureCollection");
//            result.putArray("features").addAll(featureCollection.withArray("features").deepCopy());
            Logger.info("Total features: {}", featureCollection.withArray("features").size());


            Stopwatch timer = Stopwatch.createUnstarted();
            Stopwatch convertTimer = Stopwatch.createUnstarted();
            Stopwatch geofilterTimer = Stopwatch.createUnstarted();

            // geo filter: uses geometry cache to avoid conversions from GeoJSON to Geometry
            if (!Strings.isNullOrEmpty(boundingBoxString) || !Strings.isNullOrEmpty(distanceString)) {
                SpatialReference sr4326 = SpatialReference.create(4326);
                OperatorIntersects operatorIntersects = OperatorIntersects.local();
                OperatorDistance operatorDistance = OperatorDistance.local();
                OperatorProximity2D operatorProximity2D = OperatorProximity2D.local();
                Geometry boundingBox = null;
                Pair<Point, Double> distancePair = null;
                // creates a bbox Geometry for use with intersect
                if (!Strings.isNullOrEmpty(boundingBoxString)) {
                    String wktString = TripleStoreController.boundingBoxToWkt(boundingBoxString);
                    if (wktString == null) throw new IllegalArgumentException("boundingbox");
                    boundingBox = OperatorImportFromWkt.local().execute(
                            WktImportFlags.wktImportDefaults,
                            Geometry.Type.Unknown,
                            wktString,
                            null);
                    operatorIntersects.accelerateGeometry(boundingBox, sr4326, Geometry.GeometryAccelerationDegree.enumHot);
                }

                if (!Strings.isNullOrEmpty(distanceString)) {
                    distancePair = distancePairFromString(distanceString);
                    if (distancePair == null) throw new IllegalArgumentException("distance");
//                    operatorDistance.accelerateGeometry(distancePair.getLeft(), sr4326, Geometry.GeometryAccelerationDegree.enumHot);
                    operatorProximity2D.accelerateGeometry(distancePair.getLeft(), sr4326, Geometry.GeometryAccelerationDegree.enumHot);
                }

                timer.start();
                // for each feature verify if filters are satisfied; if not, remove from iterator
                for (Iterator<JsonNode> iterator = featureCollection.withArray("features").elements(); iterator.hasNext();) {
                    JsonNode f = iterator.next();
                    Geometry geometry = geomCache.getIfPresent(f.get("id").asText());
                    if (geometry == null) {
                        convertTimer.start();
                        geometry = OperatorImportFromGeoJson.local().execute(
                                GeoJsonImportFlags.geoJsonImportDefaults,
                                Geometry.Type.Unknown,
                                om.valueToTree(f).get("geometry").toString(),
                                null).getGeometry();
                        geomCache.put(f.get("id").asText(), geometry);
                        convertTimer.stop();
                    }
                    boolean accept = true;
                    geofilterTimer.start();
                    if (!Strings.isNullOrEmpty(boundingBoxString))
                        accept = operatorIntersects.execute(boundingBox, geometry, sr4326, null);
                    if (!Strings.isNullOrEmpty(distanceString)) {
                        double distance = GeometryEngine.geodesicDistanceOnWGS84(
                                distancePair.getLeft(),
                                operatorProximity2D.getNearestCoordinate(geometry, distancePair.getLeft(), true).getCoordinate());
                        accept = accept && (distance <= distancePair.getRight());
                    }
                    geofilterTimer.stop();
                    if (!accept) {
                        iterator.remove();
                    }
                }
                timer.stop();
                if (showTimers) Logger.info("Geometry processing (cache): {} (conversion: {}, geofilter: {})",
                        timer.toString(), convertTimer.toString(), geofilterTimer.toString());
            }

            // removing descriptions and/or geometries
            if (!descriptions || !geom || !Strings.isNullOrEmpty(lang)) {
                for (Iterator<JsonNode> iterator = featureCollection.withArray("features").elements(); iterator.hasNext();) {
                    ObjectNode f = (ObjectNode) iterator.next();
                    if (!Strings.isNullOrEmpty(lang)) {
                        String label = f.path("properties").path("label").findPath(lang).asText("");
                        if (!Strings.isNullOrEmpty(label)) f.with("properties").put("label", label);
                        else f.with("properties").remove("label");
                    }
                    if (!geom) f.putNull("geometry");
                    if (!descriptions) {
                        ObjectNode newProperties = om.createObjectNode();
                        newProperties.set("label", f.path("properties").get("label"));
                        newProperties.set("external_url", f.path("properties").get("external_url"));
                        newProperties.set("hasType", f.path("properties").get("hasType"));
                        f.set("properties", newProperties);
                    }
                }
            }

//        Logger.info("Feature count (cache): {}", result.withArray("features").size());
            return featureCollection;
        }
        finally {
            lock.readLock().unlock();
        }

    }

    /**
     * Returns the number of instances satisfying the specified filters.
     * @param instance Required, the domain name of the OTM instance
     * @param boundingBoxString A bounding box string, see {@link TripleStoreController#boundingBoxToWkt(String)}
     * @param distanceString A distance string, see {@link #distancePairFromString(String)}
     * @param validFrom A date-time string (not currently supported)
     * @param validTo A date-time string (not currently supported)
     * @return The number of instances satisfying the specified filters.
     * @throws JSONException If a problem with the JSON elaboration occurred
     * @throws FileNotFoundException If the ontology file could not be found
     */
    public int getConceptInstancesCount(String instance, Set<String> concepts, String boundingBoxString, String distanceString, String validFrom, String validTo) throws JSONException, FileNotFoundException {
        lock.readLock().lock();
        ObjectNode featureCollection;
        try {
            featureCollection = getConceptInstances(instance, concepts, true, true, boundingBoxString, distanceString, validFrom, validTo, null, -1, -1);
        }
        finally {
            lock.readLock().unlock();
        }
        return featureCollection.withArray("features").size();
    }

    /**
     * Returns a point/distance pair for use in the distance filter.
     * @param distanceString A string composed by 3 comma-separated floats; the first two floats represent the longitude/latitude
     * of the center of a circle, while the third the radius of the circle. The circle here defined is used for distance filtering
     * (only the instances within the circle are selected).
     * @return A point/double pair, representing the center of the circle and its radius.
     */
    private static Pair<Point, Double> distancePairFromString(String distanceString) {
        if (Strings.isNullOrEmpty(distanceString)) return null;
        List<String> coords = Splitter.on(",").splitToList(distanceString);
        if (coords.size() != 3) return null;
        Double lng = Doubles.tryParse(coords.get(0));
        Double lat = Doubles.tryParse(coords.get(1));
        Double distance = Doubles.tryParse(coords.get(2));
        if (lng == null || lat == null || distance == null) return null;
        return Pair.of(new Point(lng, lat), distance);
    }
}
