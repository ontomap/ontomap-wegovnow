package support.controllers;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.ClientSession;
import com.mongodb.client.MongoDatabase;
import com.mongodb.gridfs.CLI;
import play.Configuration;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * This class is used to provide a single instance of the database, to be used application-wide.
 */
@Singleton
public class MongoDBController {
    private MongoDatabase mongoDatabase;
    private  MongoClient mongoClient;

    private Configuration configuration;

    @Inject
    public MongoDBController(Configuration configuration) {
        this.configuration = configuration;
        mongoClient = new MongoClient(new MongoClientURI(configuration.getString("mongo.uri")));
        mongoDatabase = mongoClient.getDatabase(configuration.getString("mongo.database"));
    }

    /**
     *
     * @return database object
     */
    public MongoDatabase getDatabase() {
        return mongoDatabase;
    }

    /**
     * Start a new session to start a new transaction
     * @return a new session
     */
    public ClientSession getSession(){
        return mongoClient.startSession();
    }
}
