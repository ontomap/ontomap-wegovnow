package support;

import org.apache.jena.query.ParameterizedSparqlString;
import org.apache.jena.shared.PrefixMapping;
import play.Configuration;

import javax.inject.Inject;

public class QueryHelper {

    private Configuration configuration;

    @Inject
    public QueryHelper(Configuration configuration) {
        this.configuration = configuration;
    }

    public ParameterizedSparqlString getParameterizedQuery(String query) {
        String baseURI = configuration.getString("ontomap.ontology.baseuri");
        ParameterizedSparqlString pss = new ParameterizedSparqlString(query);
        pss.setBaseUri(baseURI);
        pss.setNsPrefixes(PrefixMapping.Extended);
        pss.setNsPrefix("geo", "http://www.opengis.net/ont/geosparql#");
        pss.setNsPrefix("geof", "http://www.opengis.net/def/function/geosparql/");
        pss.setNsPrefix("bif", "bif:");
        return pss;
    }

}
