import com.google.inject.AbstractModule;
import play.Logger;
import support.controllers.EntityRegistryController;
import support.controllers.LoggerMongoController;


/**
 * This class is a play module that is executed at the startup of the application.
 * The {@link LoggerMongoController} is loaded and declared Sigleton.
 * @see LoggerMongoController
 */

public class LoggerMongoModule extends AbstractModule {
    @Override
    protected void configure() {
        Logger.debug("Binding LoggerMongoModule...");
        bind(LoggerMongoController.class).asEagerSingleton();
    }
}
