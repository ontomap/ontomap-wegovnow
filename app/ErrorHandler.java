import com.google.common.base.Splitter;
import controllers.ResponseController;
import play.Configuration;
import play.Environment;
import play.api.OptionalSourceMapper;
import play.api.UsefulException;
import play.api.routing.Router;
import play.http.DefaultHttpErrorHandler;
import play.mvc.Http;
import play.mvc.Result;

import javax.inject.Inject;
import javax.inject.Provider;
import javax.inject.Singleton;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

@Singleton
public class ErrorHandler extends DefaultHttpErrorHandler {

    private ResponseController responseController;

    @Inject
    public ErrorHandler(Configuration configuration, Environment environment,
                        OptionalSourceMapper sourceMapper, Provider<Router> routes,
                        ResponseController responseController) {
        super(configuration, environment, sourceMapper, routes);
        this.responseController = responseController;
    }

    @Override
    protected CompletionStage<Result> onNotFound(Http.RequestHeader request, String message) {
        return CompletableFuture.completedFuture(responseController.pathNotFound(request.toString()));
    }

    @Override
    protected CompletionStage<Result> onOtherClientError(Http.RequestHeader request, int statusCode, String message) {
        switch(statusCode) {
            case 415:
                if ("Expected application/json".equals(message))
                    return CompletableFuture.completedFuture(responseController.wrongContentType());
            default:
                return super.onOtherClientError(request, statusCode, message);
        }
    }

    @Override
    protected CompletionStage<Result> onBadRequest(Http.RequestHeader request, String message) { // todo: malformed json (better)
//        Logger.debug("bad request: {}", message);
//        return super.onBadRequest(request, message);
        if (message.contains("Error decoding json body")) {
            List<String> msgList = Splitter.on("\n").splitToList(message);
//            Logger.debug("{}", msgList);
            String newMessage = msgList.get(0).replace(": com.fasterxml.jackson.core.JsonParseException", "")
                    + "; " + Splitter.on(";").splitToList(msgList.get(1)).get(1).replace("]", "");
            return CompletableFuture.completedFuture(responseController.malformedJsonRequest(newMessage));
        }
        return CompletableFuture.completedFuture(responseController.genericMalformedParameterError(message));
    }

    @Override
    protected CompletionStage<Result> onProdServerError(Http.RequestHeader request, UsefulException exception) {
        return CompletableFuture.completedFuture(responseController.unknownInternalServerError(exception.id));
    }
}
