package controllers.api.v1;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.github.fge.jsonschema.core.report.ProcessingMessage;
import com.github.fge.jsonschema.core.report.ProcessingReport;
import com.github.fge.jsonschema.main.JsonSchema;
import com.github.fge.jsonschema.main.JsonSchemaFactory;
import com.mongodb.MongoTimeoutException;
import com.mongodb.client.ClientSession;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Projections;
import com.mongodb.client.model.ReplaceOptions;
import com.mongodb.client.result.UpdateResult;
import controllers.ResponseController;
import org.bson.Document;
import play.Configuration;
import play.Environment;
import play.Logger;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import support.actions.CertAuthenticatedAction.CertificateAuthentication;
import support.actions.MultipleInstancesAction.MultipleInstances;
import support.controllers.MappingTranslationController;
import support.controllers.MongoDBController;
import support.controllers.OntologyController;

import javax.inject.Inject;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Iterator;
import java.util.Set;

/**
 * This class contains the methods concerning the insertion and retrieval of mapping rules pertaining to each application.
 */
public class MappingController extends Controller {

    private Environment environment;
    private ResponseController responseController;
    private MongoDBController mongoDBController;
    private Configuration configuration;
    private MappingTranslationController mappingTranslationController;
    private OntologyController ontologyController;

    @Inject
    public MappingController(Environment environment, ResponseController responseController,
                             MongoDBController mongoDBController, Configuration configuration,
                             MappingTranslationController mappingTranslationController, OntologyController ontologyController) {
        this.environment = environment;
        this.responseController = responseController;
        this.mongoDBController = mongoDBController;
        this.configuration = configuration;
        this.mappingTranslationController = mappingTranslationController;
        this.ontologyController = ontologyController;
    }

    /**
     * @apiDefine missing_concept_mapping
     * @apiError (Error 400 Bad Request) missing_concept_mapping There is no corresponding mapping for the concept indicated.
     */

    /**
     * @apiDefine missing_property_mapping
     * @apiError (Error 400 Bad Request) missing_property_mapping There is no corresponding mapping for the property indicated.
     */

    /**
     * @api {post} /logger/mappings Insert mapping rules
     * @apiName InsertMappingRules
     * @apiGroup Mapping APIs
     * @apiDescription Inserts into the logger the mapping rules for translating concepts and properties to the OnToMap terminology.<br/>
     * The request must have a <code>application/json</code> Content-Type and must include the JSON representation of the mapping rules in its body (see provided example).<br/>
     * Every request made to this endpoint must include a client certificate signed by LiquidFeedback.<br/>
     * The mapping rules consist in a list of concept mappings, which associate a concept in the application terminology
     * to one in the OnToMap terminology. For each concept it is possible to include a list of property mappings,
     * each one associating a property in the app terminology to one in the OTM terminology.<br/>
     * All the concept and property mapping must refer to concepts and properties in the OnToMap ontology.<br/>
     * When a property represents a measure, it is possible to include its measure unit, in order to avoid ambiguities.<br/>
     * When new mapping rules are submitted, any previous rules associated to the application performing the request are overwritten.
     *
     * @apiParamExample Insert mapping rules
     * POST https://api.ontomap.eu/api/v1/logger/mappings
     * {
     *   "mappings": [
     *     {
     *       "app_concept": "ChildCare",
     *       "ontomap_concept": "Kindergarten",
     *       "properties": [
     *         {
     *           "app_property": "denominazione",
     *           "ontomap_property": "hasName"
     *         },
     *         {
     *           "app_property": "indirizzo",
     *           "ontomap_property": "hasAddress"
     *         },
     *         {
     *           "app_property": "telefono",
     *           "ontomap_property": "hasPhoneNumber"
     *         },
     *         {
     *           "app_property": "retta_mensile",
     *           "ontomap_property": "hasMonthlyRate",
     *           "unit": "EUR"
     *         }
     *       ]
     *     },
     *     {
     *       "app_concept": "Ristoranti",
     *       "ontomap_concept": "Restaurant"
     *     }
     *   ]
     * }
     *
     * @apiUse malformed_json
     *
     * @apiUse wrong_content_type
     *
     * @apiUse invalid_certificate
     *
     * @apiUse too_many_requests
     *
     * @apiUse internal_server_error
     *
     * @apiUse missing_concept_mapping
     *
     * @apiUse missing_property_mapping
     *
     */

    @CertificateAuthentication
    @MultipleInstances
    public Result insertMappingRules() { // todo: move to MappingTranslationController
        String application = (String) ctx().args.get("application");
        String instance = (String) ctx().args.get("instance");
        if (!request().contentType().orElse("").equals(Http.MimeTypes.JSON)) {
            return responseController.wrongContentType();
        }
        MongoDatabase database = mongoDBController.getDatabase();
        MongoCollection<Document> collection = database.getCollection(configuration.getString("ontomap.logger.mongodb.mappings.collection"));
        JsonNode requestBody = request().body().asJson();
        if (requestBody == null) return responseController.malformedJsonRequest("");
        ObjectMapper om = new ObjectMapper();
        ProcessingReport processingReport;
        //TODO: check null request body
        try {
            JsonNode jsonSchemaNode = om.readTree(environment.getFile(configuration.getString("ontomap.logger.mappings.json_schema")));
            JsonSchema jsonSchema = JsonSchemaFactory.byDefault().getJsonSchema(jsonSchemaNode);
            processingReport = jsonSchema.validate(requestBody);
        }
        catch (IOException | ProcessingException e) {
            Logger.error("Exception in insertMappingRules:", e);
            return responseController.internalServerError2(ResponseController.PARSING_ERROR);
        }
        if (!processingReport.isSuccess()) { // request body not valid
            Iterator<ProcessingMessage> iter = processingReport.iterator();
            StringBuilder errorMessage = new StringBuilder();
            while (iter.hasNext()) {
                errorMessage.append(iter.next().toString());
            }
            return responseController.malformedJsonRequest(errorMessage.toString());
        }

        Set<String> conceptsSet;
        Set<String> propertySet;

        try {
            conceptsSet = ontologyController.getSubConceptsSet("SchemaThing");
            propertySet = ontologyController.getPropertiesSet();
        }
        catch (FileNotFoundException e) {
            Logger.error("FileNotFoundException in insertMappingRules:", e);
            return responseController.internalServerError2(ResponseController.ONTOLOGY_FILE_NOT_FOUND);
        }

        for(Iterator<JsonNode> mappingsIterator = requestBody.withArray("mappings").elements(); mappingsIterator.hasNext();) {
            JsonNode mapping = mappingsIterator.next();

            if (!conceptsSet.contains(mapping.get("ontomap_concept").asText()))
                return responseController.invalidConceptMapping(mapping.get("ontomap_concept").asText());

            if (mapping.get("properties") != null) {
                for (Iterator<JsonNode> propertiesIterator = mapping.withArray("properties").elements(); propertiesIterator.hasNext();) {
                    JsonNode property = propertiesIterator.next();
                    String otmProperty = property.get("ontomap_property").asText();
                    if (otmProperty.startsWith("$") || otmProperty.contains("."))
                        return responseController.invalidProperty(otmProperty);
                    if (!propertySet.contains(otmProperty))
                        return responseController.invalidPropertyMapping(otmProperty);
                }
            }
        }

        try(ClientSession dbSession = mongoDBController.getSession()) {
            dbSession.startTransaction();
            try {
                Document rulesBson = Document.parse(om.writeValueAsString(requestBody));
                rulesBson.put("application", application);
                rulesBson.put("instance", instance);
                UpdateResult res = collection.replaceOne(dbSession,
                        Filters.and(Filters.eq("application", application), Filters.eq("instance", instance)),
                        rulesBson, new ReplaceOptions().upsert(true));
                mappingTranslationController.cacheMappings(); // todo: check exceptions?

                dbSession.commitTransaction();
                return ok(getMappingRules(instance, application));
            } catch (JsonProcessingException e) {
                Logger.error("JsonProcessingException in insertMappingRules:", e);
                dbSession.abortTransaction();
                return responseController.internalServerError2(ResponseController.PARSING_ERROR);
            } catch (MongoTimeoutException e) {
                Logger.error("MongoTimeoutException in insertMappingRules:", e);
                dbSession.abortTransaction();
                return responseController.badGateway(ResponseController.MONGODB_CONNECTION_FAILED);
            } catch (IOException e) {
                Logger.error("IOException in insertMappingRules:", e);
                dbSession.abortTransaction();
                return responseController.internalServerError2(ResponseController.PARSING_ERROR);
            }finally {
                dbSession.close();
            }
        }

    }

    /**
     *
     * @api {get} /logger/mappings Retrieve mapping rules
     * @apiName RetrieveMappingRules
     * @apiGroup Mapping APIs
     * @apiDescription Returns a JSON Object containing a list of all the concept mappings associated with the application performing the request.<br/>
     * Every request made to this endpoint must include a client certificate signed by LiquidFeedback.
     * @apiParamExample Retrieve mapping rules
     * GET https://api.ontomap.eu/api/v1/logger/mappings
     * {
     *   "application": "app1.wegovnow.eu",
     *   "mappings": [
     *     {
     *       "app_concept": "ChildCare",
     *       "ontomap_concept": "Kindergarten",
     *       "properties": [
     *         {
     *           "app_property": "denominazione",
     *           "ontomap_property": "hasName"
     *         },
     *         {
     *           "app_property": "indirizzo",
     *           "ontomap_property": "hasAddress"
     *         },
     *         {
     *           "app_property": "telefono",
     *           "ontomap_property": "hasPhoneNumber"
     *         },
     *         {
     *           "app_property": "retta_mensile",
     *           "ontomap_property": "hasMonthlyRate",
     *           "unit": "EUR"
     *         }
     *       ]
     *     },
     *     {
     *       "app_concept": "Ristoranti",
     *       "ontomap_concept": "Restaurant"
     *     }
     *   ]
     * }
     *
     *
     * @apiUse invalid_certificate
     *
     * @apiError (Error 404 Not Found) mapping_not_found There is no mapping associated with the application yet.
     *
     * @apiUse too_many_requests
     *
     * @apiUse internal_server_error
     *
     */
    @CertificateAuthentication
    @MultipleInstances
    public Result retrieveMappingRules() {
        String application = (String) ctx().args.get("application");
        String instance = (String) ctx().args.get("instance");
//        if (application == null) return responseController.noCNInCertificate();
        try {
            JsonNode mappingRules = getMappingRules(instance, application);
            if (mappingRules == null) return responseController.missingMapping();
            return ok(mappingRules);
        }
        catch (MongoTimeoutException e) {
            Logger.error("MongoTimeoutException in retrieveMappingRules:", e);
            return responseController.badGateway(ResponseController.MONGODB_CONNECTION_FAILED);
        }
        catch (IOException e) {
            Logger.error("IOException in getMappingRules:", e);
            return responseController.internalServerError2(ResponseController.PARSING_ERROR);
        }
    }

    protected JsonNode getMappingRules(String instance, String application) throws MongoTimeoutException, IOException {
        MongoDatabase database = mongoDBController.getDatabase();
        MongoCollection<Document> collection = database.getCollection(configuration.getString("ontomap.logger.mongodb.mappings.collection"));
        ObjectMapper om = new ObjectMapper();
        Document mapping  = collection.find()
                .filter(Filters.and(Filters.eq("application", application), Filters.eq("instance", instance)))
                .projection(Projections.exclude("_id", "instance"))
                .first();
        if (mapping == null) return null;
        return om.readTree(mapping.toJson());
    }

}
