package controllers.api.v1;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.mongodb.MongoException;
import controllers.ResponseController;
import org.apache.commons.lang3.tuple.Pair;
import play.Configuration;
import play.Environment;
import play.Logger;
import play.mvc.Controller;
import play.mvc.Result;
import support.actions.CertAuthenticatedAction;
import support.actions.MultipleInstancesAction;
import support.controllers.EntityRegistryController;

import javax.inject.Inject;
import java.util.Set;

/**
 * This class contains the methods concerning the insertion and retrieval of associations between different data objects.
 */
@CertAuthenticatedAction.CertificateAuthentication
@MultipleInstancesAction.MultipleInstances
public class RegistryController extends Controller {

    private Environment environment;
    private ResponseController responseController;
    private Configuration configuration;
    private EntityRegistryController entityRegistryController;

    @Inject
    public RegistryController(Environment environment, ResponseController responseController, Configuration configuration,
                              EntityRegistryController entityRegistryController) {
        this.environment = environment;
        this.responseController = responseController;
        this.configuration = configuration;
        this.entityRegistryController = entityRegistryController;
    }

    public Result getAssociations(String individualApplication, String individualExternalUrl) {
        String application = (String) ctx().args.get("application");
        String instance = (String) ctx().args.get("instance");
        Set<Pair<String, String>> entitySet = entityRegistryController.getAssociations(instance, individualApplication, individualExternalUrl);
        if (entitySet == null) return responseController.missingInstance();
        ObjectMapper om = new ObjectMapper();
        ObjectNode result = om.createObjectNode();
//        result.putArray("list");
        for (Pair<String, String> entity : entitySet) {
            result.withArray("list").add(
                    om.createObjectNode().put("application", entity.getLeft()).put("external_url", entity.getRight())
            );
        }
        return ok(result);
    }

    public Result associateIndividual(String individualExternalUrl, String otherApplication, String otherExternalUrl) {
        // todo: log these requests, applications can associate only owned individuals
        String application = (String) ctx().args.get("application");
        String instance = (String) ctx().args.get("instance");
        try {
            boolean res = entityRegistryController.setAssociation(instance, application, individualExternalUrl, otherApplication, otherExternalUrl);
            if (res) return getAssociations(application,individualExternalUrl);
            else return responseController.alreadyAssociatedInstance();
        }catch(MongoException e){
            Logger.error("MongoException in associateIndividual:", e);
            return responseController.internalServerError2(ResponseController.MONGODB_CONNECTION_FAILED);
        }
    }

    public Result deleteAssociation(String individualExternalUrl) {
        // todo: log these requests, applications can delete only owned individuals
        String application = (String) ctx().args.get("application");
        String instance = (String) ctx().args.get("instance");
        boolean res = entityRegistryController.deleteAssociation(instance, application, individualExternalUrl);
        if (res) return ok("deleted association"); //todo: message
        else return responseController.missingInstance();
    }
}
