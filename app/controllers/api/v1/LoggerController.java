package controllers.api.v1;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.github.fge.jsonschema.core.report.ProcessingMessage;
import com.github.fge.jsonschema.core.report.ProcessingReport;
import com.github.fge.jsonschema.main.JsonSchema;
import com.github.fge.jsonschema.main.JsonSchemaFactory;
import com.mongodb.MongoException;
import com.mongodb.MongoTimeoutException;
import controllers.ResponseController;
import play.Configuration;
import play.Environment;
import play.Logger;
import play.http.HttpErrorHandler;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import support.actions.CertAuthenticatedAction;
import support.actions.MultipleInstancesAction;
import support.actions.TokenAuthenticatedAction;
import support.controllers.LoggerMongoController;

import javax.inject.Inject;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;

import static support.controllers.LoggerMongoController.INSTANCE_FIELD;

/**
 * This class contains the methods concerning the insertion and retrieval of events from the logger.
 */
public class LoggerController extends Controller {

    private Configuration configuration;
    private Environment environment;
    private ResponseController responseController;
    private LoggerMongoController loggerMongoController;

    @Inject
    public LoggerController(Configuration configuration, Environment environment, ResponseController responseController,
                            LoggerMongoController loggerMongoController) {
        this.configuration = configuration;
        this.environment = environment;
        this.responseController = responseController;
        this.loggerMongoController = loggerMongoController;
    }

    /**
     * @apiDefine wrong_content_type
     * @apiError (Error 415 Unsupported Media Type) wrong_content_type The request has an unexpected Content-Type.<br/>
     * The Content-Type for this request must be <code>application/json</code>.
     */

    /**
     * @apiDefine malformed_json
     * @apiError (Error 400 Bad Request) malformed_json The payload sent is not a valid JSON string or it doesn't adhere to the JSON Schema.
     */

    /**
     * @apiDefine too_many_requests
     * @apiError (Error 429 Too Many Requests) too_many_requests The API has received too many requests in a short time.
     * Exponential backoff is suggested for the retrying of the requests.
     */

    /**
     * @apiDefine invalid_certificate
     * @apiError (Error 401 Unauthorized) invalid_certificate The client certificate was not provided or it is invalid.
     */

    /**
     *
     * @api {post} /logger/events Insert user activity events
     * @apiName InsertEvents
     * @apiGroup Logger APIs
     * @apiDescription Creates and inserts into the logger one or more user activity events.<br/>
     * The request must have a <code>application/json</code> Content-Type and must include the JSON representation of the event(s) in its body (see provided example).<br/>
     * Every request made to this endpoint must include a client certificate signed by LiquidFeedback.<br/>
     * In order to be able to insert events into the logger, every application must insert its mapping rules into the logger
     * (see the <a href="#api-Mapping_APIs-InsertMappingRules">Insert mapping rules API</a>).
     *
     * @apiHeader (Request Headers) Idempotency-Key If specified, the request is considered idempotent, so it can be safely
     * retried in case of network errors without saving duplicated events, as long as the request is resubmitted using the same key.<br/>
     * The key is a client-generated random string; good examples of possible keys are
     * <a href="https://en.wikipedia.org/wiki/Universally_unique_identifier#Version_4_.28random.29" target="_blank">V4 UUIDs</a>.<br/>
     * Keys expire after 24 hours.<br/>
     * It is strongly advised to always include an Idempotency Key, so that the events can be resubmitted safely in case of network problems.
     *
     * @apiParamExample Request body (example)
     * POST https://api.ontomap.eu/api/v1/logger/events
     * {
     *   "event_list": [
     *     {
     *       "actor": 12345,
     *       "timestamp": 1485338648883,
     *       "activity_type": "object_created",
     *       "activity_objects": [
     *         {
     *           "type": "Feature",
     *           "geometry": {
     *             "type": "Point",
     *             "coordinates": [
     *               7.643340826034546,
     *               45.07558142970864
     *             ]
     *           },
     *           "properties": {
     *             "hasType": "Asili",
     *             "external_url": "http://app1.wegovnow.eu/Asili/1",
     *             "nome": "Asilo 1"
     *           }
     *         }
     *       ],
     *       "references": [
     *         {
     *           "application": "app1.wegovnow.eu",
     *           "external_url": "http://app1.wegovnow.eu/Asili/22"
     *         }
     *       ],
     *       "visibility_details": [
     *         {
     *           "external_url": "http://app1.wegovnow.eu/Asili/56",
     *           "hidden": true
     *         }
     *       ],
     *       "details": { ... }
     *     },
     *     { ... }
     *   ]
     * }
     *
     * @apiParam (Required attributes) {Int} actor <strong>Required.</strong> The UWUM ID of the user performing the action.
     * @apiParam (Required attributes) {Long} timestamp <strong>Required.</strong> The Unix timestamp (in ms) of the action.
     * @apiParam (Required attributes) {String} activity_type <strong>Required.</strong> The user activity type.
     * @apiParam (Attributes) {Array} [activity_objects] An Array of <a href="http://geojson.org/geojson-spec.html#feature-objects" target="_blank">GeoJSON Features</a>.<br/>
     * The feature properties must include a <code>hasType</code> field (the concept to which the feature belongs) and a
     * <code>external_url</code> field (deep link to the feature).
     * The remaining properties (as well as the feature concept) are expressed in the terminology of the client application,
     * and must be included in the mapping previously sent to the logger by the application.<br/>
     * The properties can contain a <code>additionalProperties</code> object: every field contained in this object will not be translated and stored as-is.<br/>
     * The same event must not contain multiple activity_objects having the same external_url.
     *
     * @apiParam (Attributes) {Array} [references] An Array of objects.<br/>
     * Every object contains three fields: two mandatory, <code>external_url</code> (String) and <code>application</code> (String) and one not <code>type</code> (String)(optional)
     * referring to a specific feature managed by a specific application and the type of reference.
     *
     * @apiParam (Attributes) {Array} [visibility_details] An Array of objects.<br/>
     * Every object contains two fields: <code>external_url</code> (String) and <code>hidden</code> (Boolean).<br/>
     * When the <code>visibility_details</code> field is included, the features corresponding to the <code>external_urls</code> included
     * (that must be managed by the application sending the event) are hidden/shown from the data retrieval results, based on the corresponding <code>hidden</code> value.<br/>
     * The <code>visibility_details</code> field is used when the action to be logged implies a change of visibility for some features
     * (for example, if a feature is deleted or made private/public).<br/>
     * The visibility change of a feature is applied only if the timestamp of the event is greater than or equal to the timestamp
     * of the last logged event containing a visibility change for that feature.
     *
     * @apiParam (Attributes) {Object} [details] An Object that can include further details regarding the user action to be logged.
     * This field is stored as-is.
     *
     * @apiUse malformed_json
     *
     * @apiError (Error 400 Bad Request) duplicated_activity_object An event contains two or more activity_objects with the same external_url.
     *
     * @apiUse missing_concept_mapping
     *
     * @apiUse missing_property_mapping
     *
     * @apiUse invalid_certificate
     *
     * @apiError (Error 409 Conflict) idempotency_conflict A different request using the same Idempotency Key was sent previously.
     *
     * @apiUse wrong_content_type
     *
     * @apiUse too_many_requests
     *
     * @apiUse internal_server_error
     *
     */
    @CertAuthenticatedAction.CertificateAuthentication
    @MultipleInstancesAction.MultipleInstances
    @BodyParser.Of(Json100Mb.class)
    public Result insertEvents() {
        if (!configuration.getBoolean("ontomap.logger.enabled")) return forbidden("Logger disabled"); // todo: message
        String application = (String) ctx().args.get("application");
        String instance = (String) ctx().args.get(INSTANCE_FIELD);
        String idempotencyKey = request().getHeader("Idempotency-Key");

        if (!request().contentType().orElse("").equals(Http.MimeTypes.JSON)) {
            return responseController.wrongContentType();
        }

        JsonNode requestBody = request().body().asJson();
        if (requestBody == null) return responseController.malformedJsonRequest("");
        ObjectMapper om = new ObjectMapper();

        // json schema validation
        ProcessingReport processingReport;
        try {
            JsonNode jsonSchemaNode = om.readTree(environment.getFile(configuration.getString("ontomap.logger.json_schema")));
            JsonSchema jsonSchema = JsonSchemaFactory.byDefault().getJsonSchema(jsonSchemaNode);
            processingReport = jsonSchema.validate(requestBody);
        }
        catch (IOException | ProcessingException e) {
            Logger.error("Exception in insertEvents:", e);
            return responseController.internalServerError2(ResponseController.PARSING_ERROR);
        }
        if (!processingReport.isSuccess()) { // request body not valid
            Iterator<ProcessingMessage> iter = processingReport.iterator();
            StringBuilder errorMessage = new StringBuilder();
            while (iter.hasNext()) {
                errorMessage.append(iter.next().toString());
            }
            return responseController.malformedJsonRequest(errorMessage.toString());
        }

        try {
            return loggerMongoController.insertEvents(idempotencyKey, application, instance, requestBody);
        }
        catch (JsonProcessingException e) {
            Logger.error("JsonProcessingException in insertEvents:", e);
            return responseController.internalServerError2(ResponseController.PARSING_ERROR);
        }
        catch (MongoException e) {
            Logger.error("MongoTimeoutException in insertEvents:", e);
            return responseController.badGateway(ResponseController.MONGODB_CONNECTION_FAILED);
        }
    }

    /**
     * @api {get} /logger/events Retrieve user activity events
     * @apiName RetrieveEvents
     * @apiGroup Logger APIs
     * @apiDescription Returns a JSON Object containing a list of all the events in the logger, sorted from the most recent to the least recent.<br/>
     * The events are returned in the same format used for their posting, with the add of an "application" (String) field
     * containing the name of the application that posted the event, extracted from the certificate sent by the application which inserted the event.<br/>
     * It is possible to combine multiple filters in a single request.<br/>
     * Every request made to this endpoint must include a client certificate signed by LiquidFeedback or contain the token as param.
     *
     * @apiParam (Filters) {Int} [actor] If specified, returns only the events related to actions performed by the user with the specified UWUM ID.
     * @apiParam (Filters) {String} [application] If specified, returns only the events related to the specified application.
     * @apiParam (Filters) {String} [activity_type] If specified, returns only the events related to actions of the specified type.
     * @apiParam (Filters) {String} [concept] If specified, returns only the events which contain activity_objects belonging to the specified concept (OnToMap terminology).
     * @apiParam (Filters) {String} [external_url] If specified, returns only the events which contain the activity_object having the specified external_url.
     * @apiParam (Filters) {String} [reference_concept] If specified, returns only the events which refer to activity_objects belonging to the specified concept (OnToMap terminology).
     * @apiParam (Filters) {String} [reference_external_url] If specified, returns only the events which refer to the activity_object having the specified external_url.
     * @apiParam (Filters) {Boolean} [subconcepts=false] If set to <code>true</code> and <code>concept</code> or
     * <code>reference_concept</code> are specified, returns only the events which contain or refer to activity_objects
     * belonging to the specified concept and its subconcepts. Has effect only if <code>concept</code> or
     * <code>reference_concept</code> are specified.
     * @apiParam (Filters) {Long} [start_time] If specified, returns only the events having a timestamp greater than or equal to the specified timestamp (Unix timestamp in ms).
     * @apiParam (Filters) {Long} [end_time] If specified, returns only the events having a timestamp less than or equal to the specified timestamp (Unix timestamp in ms).
     * @apiParam (Filters) {String} [boundingbox] A string representing the coordinates of the North-East and South-West
     * points of a bounding box (using EPSG:4326 as CRS).<br/>
     * If specified, returns only the events including or referring to activity_objects located in the specified bounding box.<br/>
     * String format: <code>NE_lng,NE_lat,SW_lng,SW_lat</code>
     * @apiParam (Filters) {Boolean} [aggregate]  If set to <code>true</code>, each event of the returned list will have at most one activity_object.
     * If an activity_object (identified by application/external_url) appears in more than one event, only the most recent version of it is returned.
     * @apiParam (Filters) {Boolean} [visibility]  If set to <code>true</code>, returns only the events which contain visible activity_objects (current_visibility = true).
     * @apiParam (Filters) {String} [additionalProperty_]  Complete this parameter with the additionalProperty you want to specify (Eg: <code>additionalProperty_moderation</code>). The request returns only the events which contain activity_objects have that additionalProperty equal to that value (Int/Double, String e Boolean only supported).
     * @apiUse token
     *
     *
     * @apiParamExample Retrieve events
     * GET https://api.ontomap.eu/api/v1/logger/events
     * {
     *   "event_list": [
     *     {
     *       "application": "app1.wegovnow.eu",
     *       "actor": 67890,
     *       "timestamp": 14967425487654,
     *       "activity_type": "profile_updated",
     *     },
     *     {
     *       "application": "app2.wegovnow.eu",
     *       "actor": 12345,
     *       "timestamp": 1485338648883,
     *       "activity_type": "object_created",
     *       "activity_objects": [
     *         {
     *           "type": "Feature",
     *           "geometry": {
     *             "type": "Point",
     *             "coordinates": [
     *               7.643340826034546,
     *               45.07558142970864
     *             ]
     *           },
     *           "properties": {
     *             "hasType": "Asili",
     *             "external_url": "http://app1.wegovnow.eu/Asili/1",
     *             "nome": "Asilo 1"
     *           }
     *         }
     *       ],
     *       "references": [
     *         {
     *           "application": "app1.wegovnow.eu",
     *           "external_url": "http://app1.wegovnow.eu/Asili/22"
     *         }
     *       ],
     *       "visibility_details": [
     *         {
     *           "external_url": "http://app1.wegovnow.eu/Asili/56",
     *           "hidden": true
     *         }
     *       ],
     *       "details": {
     *           "action_id": "76b66b1a-d846-4a56-8bfb-5aa5b4a3227f"
     *       }
     *     }
     *   ]
     * }
     *
     * @apiParam (Filters) {String} [reference_concept] If specified, returns only the events referring to activity_objects
     * belonging to the specified concept (OnToMap terminology).
     *
     * @apiParam (Filters) {String} [reference_external_url] If specified, returns only the events referring to the activity_object having the specified external_url.
     *
     * @apiUse pagination
     *
     * @apiUse prettyPrint
     *
     * @apiUse malformed_parameter
     *
     * @apiUse invalid_certificate
     *
     * @apiUse too_many_requests
     *
     * @apiUse internal_server_error
     *
     */
    @TokenAuthenticatedAction.TokenAuthentication
    @MultipleInstancesAction.MultipleInstances
    public Result retrieveEvents(long actor, long startTime, long endTime, String application, String boundingBoxString,
                                 String activityType, String concept, String externalUrl,
                                 String referenceConcept, String referenceExternalUrl,
                                 int limit, int page) {
        if (!configuration.getBoolean("ontomap.logger.enabled")) return forbidden("Logger disabled"); // todo: message

        String instance = (String) ctx().args.get(INSTANCE_FIELD);
        boolean excludeFeatures = Boolean.parseBoolean(request().getQueryString("exclude_features"));
        boolean prettyPrint = Boolean.parseBoolean(request().getQueryString("prettyprint"));
        boolean subConcepts = Boolean.parseBoolean(request().getQueryString("subconcepts"));
        boolean aggregate = Boolean.parseBoolean(request().getQueryString("aggregate"));
        String visibility = request().getQueryString("visibility");
        boolean tokenAuth = (Boolean) ctx().args.get("tokenAuth");

        if(actor != -1 && tokenAuth)
            return responseController.missingCertificate();

        HashMap<String,String> additionalProperties = new HashMap<>();
        for(String key :  request().queryString().keySet()){
            if(key.startsWith("additionalProperty_"))
            additionalProperties.put(key.replace("additionalProperty_",""), request().getQueryString(key));
        }

        ObjectMapper om = new ObjectMapper();
        try {
            ObjectNode result = loggerMongoController.retrieveEvents(instance,tokenAuth, actor, startTime, endTime, application, boundingBoxString,
                    activityType, concept, externalUrl, referenceConcept, referenceExternalUrl, limit,
                    page, excludeFeatures, subConcepts, aggregate,visibility,additionalProperties);
            return (environment.isProd() && !prettyPrint)
                    ? ok(result)
                    : ok(om.writerWithDefaultPrettyPrinter().writeValueAsString(result)).as(InstancesController.JSON_CONTENT_TYPE);
        }
        catch (FileNotFoundException e) {
            Logger.error("FileNotFoundException in retrieveEvents:", e);
            return responseController.internalServerError2(ResponseController.ONTOLOGY_FILE_NOT_FOUND);
        }
        catch (MongoTimeoutException e) {
            Logger.error("MongoTimeoutException in retrieveEvents:", e);
            return responseController.badGateway(ResponseController.MONGODB_CONNECTION_FAILED);
        }
        catch (JsonProcessingException e) {
            Logger.error("JsonProcessingException in retrieveEvents:", e);
            return responseController.internalServerError2(ResponseController.PARSING_ERROR);
        }
        catch (IllegalArgumentException e) {
            return responseController.malformedParameterError(e.getMessage());
        }
    }

    /**
     * @api {get} /api/v1/logger/events/<id> Retrieve a specific user activity event
     * @apiName RetrieveSingleEvent
     * @apiGroup Logger APIs
     * @apiDescription Returns a JSON Object containing the event from the logger having the specified <i>id</i>.
     * The event is returned in the same format used for its posting, with the addition of an "application" (String)
     * field containing the name of the application that posted the event.
     * The name of the application is extracted from the certificate sent by the application
     * which inserted the event.<br/>
     * Every request made to this endpoint must include a client certificate signed by LiquidFeedback or contain the token as param.
     * @apiParamExample Retrieve a single event
     * GET https://api.ontomap.eu/api/v1/logger/events/5b103de8a1d1cd2e98cf05ed
     * {
     *      "actor": 12345,
     *      "timestamp": 1485338648883,
     *      "activity_type": "object_created",
     *      "references": [
     *        {
     *           "application": "app1.wegovnow.eu",
     *           "external_url": "http://app1.wegovnow.eu/Asili/22"
     *        }
     *       ],
     *       "details": {},
     *       "application": "ontomap.eu",
     *       "id": "5b103de8a1d1cd2e98cf05ed",
     *       "applicationName": "OnToMap"
     * }
     *
     * @apiParam (Required parameter) {String} id <strong>Required.</strong> The id of the event that is requested.
     *
     * @apiUse prettyPrint
     *
     * @apiUse token
     *
     * @apiUse invalid_certificate
     *
     * @apiUse too_many_requests
     *
     * @apiUse internal_server_error
     */

    @TokenAuthenticatedAction.TokenAuthentication
    @MultipleInstancesAction.MultipleInstances
    public Result retrieveSingleEvent(String eventId) {
        if (!configuration.getBoolean("ontomap.logger.enabled")) return forbidden("Logger disabled"); // todo: message
        String instance = (String) ctx().args.get(INSTANCE_FIELD);
        boolean prettyPrint = Boolean.parseBoolean(request().getQueryString("prettyprint"));
        boolean tokenAuth = (Boolean) ctx().args.get("tokenAuth");
        ObjectMapper om = new ObjectMapper();
        try {
            ObjectNode result = loggerMongoController.retrieveSingleEvent(instance,tokenAuth,eventId);
            if (result == null) return responseController.eventNotFound();
            return (environment.isProd() && !prettyPrint)
                    ? ok(result)
                    : ok(om.writerWithDefaultPrettyPrinter().writeValueAsString(result)).as(InstancesController.JSON_CONTENT_TYPE);
        }
        catch (MongoTimeoutException e) {
            Logger.error("MongoTimeoutException in retrieveSingleEvent:", e);
            return responseController.badGateway(ResponseController.MONGODB_CONNECTION_FAILED);
        }
        catch (JsonProcessingException e) {
            Logger.error("JsonProcessingException in retrieveSingleEvent:", e);
            return responseController.internalServerError2(ResponseController.PARSING_ERROR);
        }
    }

    /**
     * @api {get} /logger/hiddenlist Retrieve hidden data objects
     * @apiName RetrieveHiddenList
     * @apiGroup Logger APIs
     * @apiDescription Returns a list of all of the data objects (activity_objects) managed from the application making the request which are currently hidden from the
     * <a href="#api-Instances_APIs-GetConceptInstances">data retrieval API</a> results.<br/>
     * Every request made to this endpoint must include a client certificate signed by LiquidFeedback.
     *
     * @apiParamExample Retrieve list of hidden data objects
     * GET https://api.ontomap.eu/api/v1/logger/hiddenlist
     * {
     *   "hidden_list": [
     *     "http://app1.wegovnow.eu/Asili/1",
     *     "http://app1.wegovnow.eu/Asili/2"
     *   ]
     * }
     *
     * @apiUse invalid_certificate
     *
     * @apiUse too_many_requests
     *
     * @apiUse internal_server_error
     *
     */
    @CertAuthenticatedAction.CertificateAuthentication
    @MultipleInstancesAction.MultipleInstances
    public Result getHiddenList() {
        if (!configuration.getBoolean("ontomap.logger.enabled")) return forbidden("Logger disabled"); // todo: message
        String application = (String) ctx().args.get("application");
        String instance = (String) ctx().args.get(INSTANCE_FIELD);

        try {
            ObjectNode result = loggerMongoController.getHiddenList(application, instance);
            return ok(result);
        }
        catch (MongoTimeoutException e) {
            Logger.error("MongoTimeoutException in getHiddenList:", e);
            return responseController.badGateway(ResponseController.MONGODB_CONNECTION_FAILED);
        }
    }

    public static class Json100Mb extends BodyParser.Json {
        @Inject
        public Json100Mb(HttpErrorHandler errorHandler) {
            super(100 * 1024 * 1024, errorHandler);
        }
    }
}
