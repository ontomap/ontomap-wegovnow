package controllers.api.v1;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.MongoTimeoutException;
import controllers.ResponseController;
import play.Logger;
import play.mvc.Controller;
import play.mvc.Result;
import support.actions.CertAuthenticatedAction;
import support.controllers.AccessTokenMongoController;

import javax.inject.Inject;

/**
 * This class contains the methods concerning the generation of access tokens for read-only use of the API.
 */

public class AccessTokenController extends Controller {

    private AccessTokenMongoController accessTokenMongoController;
    private ResponseController responseController;

    @Inject
    public AccessTokenController(AccessTokenMongoController accessTokenMongoController, ResponseController responseController) {
        this.accessTokenMongoController = accessTokenMongoController;
        this.responseController = responseController;
    }

    /**
     * @api {get} /api/v1/access_token  Get the access token
     * @apiName GetToken
     * @apiGroup Token APIs
     * @apiDescription Returns a JSON Object containing an access token for read-only use of the API. This token can be used when it is not possible or pratical to use a client certificate (e.g. AJAX requests).
     * This token can se used to GET events list, but every event will contain only non-sensitive information (e.g.: id, name, position of the feature).
     * Every request made to this endpoint must include a client certificate signed by LiquidFeedback.
     * @apiParamExample Example
     * GET https://api.ontomap.eu//api/v1/access_token
     * {
     *   "token" : "ZGNmZDk0MTYtZjQ5YS00YTQ4LTg4NzUtMDE5M2EyMDg0M2Yy"
     * }
     *
     * @apiUse invalid_certificate
     *
     */
    @CertAuthenticatedAction.CertificateAuthentication
    public Result getToken() { // todo:exceptions
        String application = (String) ctx().args.get("application");
        try {
            String token = accessTokenMongoController.getToken(application);
            ObjectMapper om = new ObjectMapper();
            return ok(om.createObjectNode().put("token", token));
        }
        catch (MongoTimeoutException e) {
            Logger.error("MongoTimeoutException in getToken:", e);
            return responseController.badGateway(ResponseController.MONGODB_CONNECTION_FAILED);
        }
    }

    /**
     * @api {post} /api/v1/access_token/renew  Renew the access token
     * @apiName RenewToken
     * @apiGroup Token APIs
     * @apiDescription Returns a JSON Object containing a new access token for read-only use of the API. This token can be used when it is not possible or pratical to use a client certificate (e.g. AJAX requests).
     * This token can se used to GET events list, but every event will contain only non-sensitive information(e.g.: id, name, position of the feature).
     * Every request made to this endpoint must include a client certificate signed by LiquidFeedback.
     *
     * @apiUse invalid_certificate
     *
     */
    @CertAuthenticatedAction.CertificateAuthentication
    public Result renewToken() { // todo:exceptions
        String application = (String) ctx().args.get("application");
        try {
            String newToken = accessTokenMongoController.generateToken(application);
            ObjectMapper om = new ObjectMapper();
            return ok(om.createObjectNode().put("token", newToken));
        }
        catch (MongoTimeoutException e) {
            Logger.error("MongoTimeoutException in renewToken:", e);
            return responseController.badGateway(ResponseController.MONGODB_CONNECTION_FAILED);
        }
    }
}
