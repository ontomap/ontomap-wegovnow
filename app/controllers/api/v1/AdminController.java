package controllers.api.v1;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.common.base.Strings;
import com.mongodb.MongoException;
import com.mongodb.MongoTimeoutException;
import controllers.ResponseController;
import play.Environment;
import play.Logger;
import play.mvc.Controller;
import play.mvc.Result;
import support.actions.AdminAction;
import support.actions.MultipleInstancesAction;
import support.controllers.*;

import javax.inject.Inject;

import static support.controllers.LoggerMongoController.INSTANCE_FIELD;

/**
 * This class contains the methods concerning the actions used to administer the system, such as refreshing the caches
 * or deleting events from the logger.
 * These actions can be performed only by using a "admin" client certificate when in production (see the AdminAction class).
 */
public class AdminController extends Controller {

    private Environment environment;
    private CacheController cacheController;
    private EntityRegistryController entityRegistryController;
    private MappingTranslationController mappingTranslationController;
    private ApplicationsController applicationsController;
    private LoggerMongoController loggerMongoController;
    private ResponseController responseController;


    @Inject
    public AdminController(Environment environment, CacheController cacheController,
                           EntityRegistryController entityRegistryController, MappingTranslationController mappingTranslationController,
                           ApplicationsController applicationsController, LoggerMongoController loggerMongoController,
                           ResponseController responseController) {
        this.environment = environment;
        this.cacheController = cacheController;
        this.entityRegistryController = entityRegistryController;
        this.mappingTranslationController = mappingTranslationController;
        this.applicationsController = applicationsController;
        this.loggerMongoController = loggerMongoController;
        this.responseController = responseController;
    }

    @AdminAction.AdminAuthentication
    public Result refreshCaches() {
        applicationsController.retrieveAppNames();
        entityRegistryController.cacheEntities();
        mappingTranslationController.cacheMappings();
        cacheController.buildCache();
        return ok("Caches updated"); // todo: message
    }

    @MultipleInstancesAction.MultipleInstances
    @AdminAction.AdminAuthentication
    public Result deleteUserEvents(long actor, String id, String application) {
        int paramCount = 0;
        paramCount += (actor == -1) ? 0 : 1;
        paramCount += (Strings.isNullOrEmpty(id)) ? 0 : 1;
        paramCount += (Strings.isNullOrEmpty(application)) ? 0 : 1;
        if (paramCount != 1) return badRequest("Only one parameter accepted"); // todo: message

        String instance = (String) ctx().args.get(INSTANCE_FIELD);
        try {
            return loggerMongoController.deleteUserEvents(instance, actor, id, application);
        }
        catch (JsonProcessingException e) {
            Logger.error("JsonProcessingException in deleteUserEvents:", e);
            return responseController.internalServerError2(ResponseController.PARSING_ERROR);
        }
    }

    /*
    @MultipleInstancesAction.MultipleInstances
    @AdminAction.AdminAuthentication
    public Result updateFeatureVisiblityIntoEvents() {
        try {
            loggerMongoController.updateFeatureVisiblityIntoEvents();
            return ok("Events updated with feature's visibility status");
        }catch (MongoException e){
            return responseController.internalServerError2(ResponseController.MONGODB_CONNECTION_FAILED);
        }

    }
    */
}
