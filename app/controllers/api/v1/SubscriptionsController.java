package controllers.api.v1;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.common.base.Strings;
import controllers.ResponseController;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import support.actions.CertAuthenticatedAction;
import support.actions.MultipleInstancesAction;
import support.controllers.SubscriptionsMongoController;

import javax.inject.Inject;

import static support.controllers.LoggerMongoController.INSTANCE_FIELD;


public class SubscriptionsController extends Controller {

    @Inject
    private SubscriptionsMongoController subscriptionsMongoController;

    @Inject
    private ResponseController responseController;

    /**
     *
     * @api {get} /api/v1/logger/subscriptions Get current subscriptions
     * @apiName GetSubscriptions
     * @apiGroup Subscriptions APIs
     * @apiDescription Returns a JSON object containing a list of all subscriptions from the requesting application.
     *
     * @apiParamExample Get subscriptions
     * GET https://api.ontomap.eu/api/v1/logger/subscriptions
     * {
     *      "subscriptions":
     *      [
     *       {
     *            "application": "ontomap.eu",
     *            "activity_type": "object_created1",
     *            "url": "https://webhook.site/4ce26b87-4252-4253-a163-9b8f8b48b8d4",
     *            "id": "5b72c4ef9c71fe288a1f829b"
     *       }
     *      ]
     * }
     *
     * @apiUse invalid_certificate
     *
     * @apiUse too_many_requests
     *
     */
    @CertAuthenticatedAction.CertificateAuthentication
    @MultipleInstancesAction.MultipleInstances
    public Result getSubscriptions() {
        String application = (String) ctx().args.get("application");
        String instance = (String) ctx().args.get(INSTANCE_FIELD);
        return ok(subscriptionsMongoController.getSubscriptions(instance, application));
    }

    /**
     *
     * @api {post} /api/v1/logger/subscriptions Create a new subscription
     * @apiName InsertSubscription
     * @apiGroup Subscriptions APIs
     * @apiDescription Create a new subscription to an event type.
     * The subscription enables the application to receive a notification when a user performs
     * a specific action type in a specific application.
     * In this way an application does not need to make multiple requests to the logger
     * in order to check if there are new events.<br/>
     * The request must have a <code>application/json</code> Content-Type and it must include a JSON representation
     * of the subscription in its body (see the example below).<br/> The notication received by the subscription system
     * will contain the <i>id</i> and the <i>timestamp</i> of the event that triggered it.
     * Every request made to this endpoint must include a client certificate signed by LiquidFeedback.
     *
     *
     * @apiParamExample Add a subscription
     * POST https://api.ontomap.eu/api/v1/logger/subscriptions
     * {
     *      "application": "ontomap.eu",
     *      "activity_type": "object_created",
     *      "url": "https://webhook.site/4ce26b87-4252-4253-a163-9b8f8b48b8d4"
     * }
     *
     * @apiParamExample Response of a subscription trigged
     * POST (from) https://api.ontomap.eu/
     * {
     *      "id": "5b740a414620ac240c142173",
     *      "application": "ontomap.eu",
     *      "activity_type": "object_created",
     *      "timestamp": 1485338648885
     * }
     *
     * @apiParam (Required attributes) {String} application <strong>Required.</strong> The application where the event takes place.
     * @apiParam (Required attributes) {String} activity_type <strong>Required.</strong> The user activity type.
     * @apiParam (Required attributes) {String} url  <strong>Required.</strong> The url where OnToMap will send the notification (POST)
     *
     *
     * @apiUse invalid_certificate
     *
     * @apiUse too_many_requests
     *
     * @apiUse missing_parameter
     *
     * @apiUse internal_server_error
     *
     */
    @CertAuthenticatedAction.CertificateAuthentication
    @MultipleInstancesAction.MultipleInstances
    public Result insertSubscription() {
        String application = (String) ctx().args.get("application");
        String instance = (String) ctx().args.get("instance");

        if (!request().contentType().orElse("").equals(Http.MimeTypes.JSON)) {
            return responseController.wrongContentType();
        }
        JsonNode requestBody = request().body().asJson();
        if (requestBody == null) return responseController.malformedJsonRequest("");
        ObjectMapper om = new ObjectMapper();
        //applicazione di provenienza degli eventi ai quali mi voglio sottoscrivere, non quella degli header che richiede
        if (requestBody.get("application") == null || Strings.isNullOrEmpty(requestBody.get("application").asText())) {
            return responseController.missingParameterError("application");
        }
        if (requestBody.get("activity_type") == null || Strings.isNullOrEmpty(requestBody.get("activity_type").asText())) {
            return responseController.missingParameterError("activity_type");
        }
        if (requestBody.get("url") == null || Strings.isNullOrEmpty(requestBody.get("url").asText())) {
            return responseController.missingParameterError("url");
        }

        ObjectNode res = subscriptionsMongoController.addSubscription(instance, requestBody.get("application").asText(), requestBody.get("activity_type").asText(), application, requestBody.get("url").asText());
        if (res != null) return ok(res);
        else return responseController.internalServerError2(ResponseController.MONGODB_CONNECTION_FAILED);
    }

    /**
     *
     * @api {DELETE} /logger/subscriptions/<id> Delete a subscription
     * @apiName DeleteSubscription
     * @apiGroup Subscriptions APIs
     * @apiDescription Delete a subscription with the given id.</br>
     * Every request made to this endpoint must include a client certificate signed by LiquidFeedback.
     *
     * @apiParam (Required parameter) {String} id <strong>Required.</strong> The id of the subscription that has to be deleted.
     *
     * @apiUse invalid_certificate
     *
     * @apiUse too_many_requests
     */

    @CertAuthenticatedAction.CertificateAuthentication
    @MultipleInstancesAction.MultipleInstances
    public Result deleteSubscription(String id) {
        String application = (String) ctx().args.get("application");
        String instance = (String) ctx().args.get("instance");
        boolean res = subscriptionsMongoController.deleteSubscription(instance, application, id);
        if (res) return getSubscriptions();
        else return responseController.subscriptionNotFound();
    }
}
