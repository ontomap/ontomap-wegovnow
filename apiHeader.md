**Description and notes**
---------------------
This is the list of APIs currently available for the interaction with OnToMap.
The list will be updated with new APIs when they will be implemented.

Development/testing API endpoint: https://api.ontomap.eu (dev/test Client Certificate)

Prototype 1 API endpoint: https://p1.api.ontomap.eu (PT1 Client Certificate)

Prototype 2 API endpoint: https://p2.api.ontomap.eu (PT2 Client Certificate)

Prototype 3 API endpoint: https://p3.api.ontomap.eu (PT3 Client Certificate)

Torino Instance API endpoint: https://torino.api.ontomap.eu (Torino instance Client Certificate)

San Donà Instance API endpoint: https://sandona.api.ontomap.eu (San Donà instance Client Certificate)

Southwark Instance API endpoint: https://southwark.api.ontomap.eu (Southwark instance Client Certificate)

<span style="color:firebrick;">**It should be noted that the ontology currently used in OnToMap is under heavy development;
this means that you should not rely on the specification of concepts available so far:
concepts and relations might be modified in the next future in order to accommodate the types of information to be handled in the WeGovNow pilots.
As soon as the ontology will be in a stable enough state, a link to it will be published in this page.**</span>

A set of data based mainly on Geo-Data provided by the Cities of Turin and San Donà di Piave and the UK Government is currently used in OnToMap; the concepts available are:
- `LawEnforcement` (Law Enforcement, Police etc.)
- `Cinema` (Cinema/Movie theaters)
- `PlaceOfWorship` (Places of worship, churches etc.)
- `Drugstore` (Drugstores, pharmacies etc.)
- `School` (Schools)
- `StreetMarket` (Street Markets)
- `UrbanPark` (Urban/public parks)
- `Accommodation` (Accommodation facilities, hotels etc.)
- `Hospital` (Hospitals)
- `WaterAndWasteDisposal` (Water distribution and waste disposal activities)
- `HealthSocialService` (Health and social service activities)
- `ArtGallery` (Art galleries)
- `RealEstateActivity` (Real estate activities)
- `Library` (Libraries)
- `Cafe` (Cafés/Bars)
- `Logistics` (Logistics activities)
- `Store` (Stores)
- `Leisure` (Artistic, sport, entertainment activities)
- `Restaurant` (Restaurants)
- `Museum` (Museums)
- `FinancialService` (Financial/insurance activities)
- `Monument` (Monuments)
- `InformationCommunicationService` (Information/communication/advertising/etc. activities)
- `PowerDistribution` (Electricity/gas/steam/etc. distribution activities)
- `ProfessionalScientificTechnicalActivity` (Professional, technical, scientific activities/laboratories)
- `Club` (Private clubs)
- `CulturalCenter` (Cultural centers)
- `FoodAndAccommodation` (Activities regarding food production/distribution and accommodation)
- `Manufacturing` (Factiories, manufacturing activities)
- `Business` (Business activities)
- `BicyclePath` (Bicycle paths)
- `ParkingLot` (Parking lots)
- `TrainStation` (Train stations)
- `HistoricalCenter` (Historical centers)
- `Highway` (Highways)