define({ "api": [
  {
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "optional": false,
            "field": "varname1",
            "description": "<p>No type.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "varname2",
            "description": "<p>With type.</p>"
          }
        ]
      }
    },
    "type": "",
    "url": "",
    "version": "0.0.0",
    "filename": "./conf/apiDoc_template/main.js",
    "group": "C__wegovnow_data_ontomap_wegovnow_conf_apiDoc_template_main_js",
    "groupTitle": "C__wegovnow_data_ontomap_wegovnow_conf_apiDoc_template_main_js",
    "name": ""
  },
  {
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "optional": false,
            "field": "varname1",
            "description": "<p>No type.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "varname2",
            "description": "<p>With type.</p>"
          }
        ]
      }
    },
    "type": "",
    "url": "",
    "version": "0.0.0",
    "filename": "./public/docs/main.js",
    "group": "C__wegovnow_data_ontomap_wegovnow_public_docs_main_js",
    "groupTitle": "C__wegovnow_data_ontomap_wegovnow_public_docs_main_js",
    "name": ""
  },
  {
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "optional": false,
            "field": "varname1",
            "description": "<p>No type.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "varname2",
            "description": "<p>With type.</p>"
          }
        ]
      }
    },
    "type": "",
    "url": "",
    "version": "0.0.0",
    "filename": "./target/scala-2.11/classes/apiDoc_template/main.js",
    "group": "C__wegovnow_data_ontomap_wegovnow_target_scala_2_11_classes_apiDoc_template_main_js",
    "groupTitle": "C__wegovnow_data_ontomap_wegovnow_target_scala_2_11_classes_apiDoc_template_main_js",
    "name": ""
  },
  {
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "optional": false,
            "field": "varname1",
            "description": "<p>No type.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "varname2",
            "description": "<p>With type.</p>"
          }
        ]
      }
    },
    "type": "",
    "url": "",
    "version": "0.0.0",
    "filename": "./target/web/classes/main/META-INF/resources/webjars/ontomap_backend/1.0/docs/main.js",
    "group": "C__wegovnow_data_ontomap_wegovnow_target_web_classes_main_META_INF_resources_webjars_ontomap_backend_1_0_docs_main_js",
    "groupTitle": "C__wegovnow_data_ontomap_wegovnow_target_web_classes_main_META_INF_resources_webjars_ontomap_backend_1_0_docs_main_js",
    "name": ""
  },
  {
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "optional": false,
            "field": "varname1",
            "description": "<p>No type.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "varname2",
            "description": "<p>With type.</p>"
          }
        ]
      }
    },
    "type": "",
    "url": "",
    "version": "0.0.0",
    "filename": "./target/web/public/main/docs/main.js",
    "group": "C__wegovnow_data_ontomap_wegovnow_target_web_public_main_docs_main_js",
    "groupTitle": "C__wegovnow_data_ontomap_wegovnow_target_web_public_main_docs_main_js",
    "name": ""
  },
  {
    "type": "get",
    "url": "/concepts",
    "title": "Get all ontology concepts",
    "name": "GetAllConcepts",
    "group": "Concepts_APIs",
    "description": "<p>Returns a JSON Object, representing complete information about all the concepts in the ontology.<br> The field <code>concepts</code> is a JSON Array of objects, each representing an ontology concept.<br> If <code>relations</code> is set to <code>true</code>, a Json Array of objects called <code>relations</code> is included in the result; each object of the Array represents a semantic relation among two concepts of the ontology. All the relations defined in the ontology are included.</p>",
    "parameter": {
      "examples": [
        {
          "title": "Example: Concepts and relations",
          "content": "GET https://api.ontomap.eu/api/v1/concepts?relations=true\n{\n  \"concepts\": [\n    {\n      \"label\": {\n        \"en\": \"Event\",\n        \"it\": \"Evento\"\n      },\n      \"description\": {\n        \"en\": \"Event definition\",\n        \"it\": \"Definizione di Evento\"\n      },\n      \"uri\": \"http://ontomap.eu/ontology/Event\"\n    },\n    {\n      \"label\": {\n        \"en\": \"Place\",\n        \"it\": \"Luogo\"\n      },\n      \"description\": {\n        \"en\": \"Place definition\",\n        \"it\": \"Definizione di Luogo\"\n      },\n      \"uri\": \"http://ontomap.eu/ontology/Place\"\n    }\n  ],\n  \"relations\": [\n    {\n      \"subject\": \"http://ontomap.eu/ontology/Event\",\n      \"object\": \"http://ontomap.eu/ontology/Place\",\n      \"predicate\": \"hasPlace\"\n    }\n  ]\n}",
          "type": "json"
        }
      ],
      "fields": {
        "Information type": [
          {
            "group": "Information type",
            "type": "Boolean",
            "optional": true,
            "field": "relations",
            "defaultValue": "false",
            "description": "<p>If set to <code>true</code>, the relations among the concepts are included in the result.</p>"
          },
          {
            "group": "Information type",
            "type": "String",
            "optional": true,
            "field": "lang",
            "description": "<p>A <a href=\"https://en.wikipedia.org/wiki/ISO_639-1\" target=\"_blank\">ISO 639-1 language code</a>.<br/> If specified, the <code>label</code> field of the JSON Objects representing the concepts will be a String containing the label of the requested concept in the specified language, if present.<br/> If not set, the <code>label</code> field of the JSON Objects representing the concepts will be a JSON Object containing the labels of each concept in all the available languages.</p>"
          }
        ],
        "Presentation": [
          {
            "group": "Presentation",
            "type": "Boolean",
            "optional": true,
            "field": "prettyprint",
            "defaultValue": "false",
            "description": "<p>If set to <code>true</code>, the JSON string served is indented.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./app/controllers/api/v1/ConceptsController.java",
    "groupTitle": "Concepts_APIs",
    "error": {
      "fields": {
        "Error 401 Unauthorized": [
          {
            "group": "Error 401 Unauthorized",
            "optional": false,
            "field": "invalid_certificate",
            "description": "<p>The client certificate was not provided or it is invalid.</p>"
          }
        ],
        "Error 429 Too Many Requests": [
          {
            "group": "Error 429 Too Many Requests",
            "optional": false,
            "field": "too_many_requests",
            "description": "<p>The API has received too many requests in a short time. Exponential backoff is suggested for the retrying of the requests.</p>"
          }
        ],
        "Error 5xx Internal Server Error": [
          {
            "group": "Error 5xx Internal Server Error",
            "optional": false,
            "field": "internal_server_error",
            "description": "<p>There was an error during the elaboration of the request.</p>"
          }
        ]
      }
    }
  },
  {
    "type": "get",
    "url": "/concepts/<concept>",
    "title": "Get ontology concept",
    "name": "GetOntologyConcept",
    "group": "Concepts_APIs",
    "description": "<p>Returns a JSON Object, representing complete information about the concept passed as parameter in the request.<br> The field <code>relations</code> is a JSON Array of objects; each object represents a semantic relation in which <code>concept</code> is involved.</p>",
    "parameter": {
      "examples": [
        {
          "title": "Example: Specify language",
          "content": "GET https://api.ontomap.eu/api/v1/concepts/River?lang=en\n{\n  \"label\" : \"River\",\n  \"description\" : \"A large natural stream of water\",\n  \"uri\" : \"http://ontomap.eu/ontology/River\",\n  \"relations\" : [\n    {\n      \"subject\" : \"http://ontomap.eu/ontology/River\",\n      \"object\" : \"http://ontomap.eu/ontology/Place\",\n      \"predicate\" : \"subClassOf\"\n    }\n  ]\n}",
          "type": "json"
        }
      ],
      "fields": {
        "Required parameters": [
          {
            "group": "required_parameters",
            "type": "String",
            "optional": false,
            "field": "concept",
            "description": "<p><strong>Required.</strong> The concept of which the information is requested.</p>"
          }
        ],
        "Information type": [
          {
            "group": "Information type",
            "type": "String",
            "optional": true,
            "field": "lang",
            "description": "<p>A <a href=\"https://en.wikipedia.org/wiki/ISO_639-1\" target=\"_blank\">ISO 639-1 language code</a>.<br/> If specified, the <code>label</code> field of the JSON Object will be a String containing the label of the requested concept in the specified language, if present.<br/> If not set, the <code>label</code> field of the JSON Object will be a JSON Object containing the labels of the requested concept in all the available languages.</p>"
          }
        ],
        "Presentation": [
          {
            "group": "Presentation",
            "type": "Boolean",
            "optional": true,
            "field": "prettyprint",
            "defaultValue": "false",
            "description": "<p>If set to <code>true</code>, the JSON string served is indented.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 404 Not Found": [
          {
            "group": "Error 404 Not Found",
            "optional": false,
            "field": "instance_not_found",
            "description": "<p>The instance requested does not exist.</p>"
          }
        ],
        "Error 401 Unauthorized": [
          {
            "group": "Error 401 Unauthorized",
            "optional": false,
            "field": "invalid_certificate",
            "description": "<p>The client certificate was not provided or it is invalid.</p>"
          }
        ],
        "Error 429 Too Many Requests": [
          {
            "group": "Error 429 Too Many Requests",
            "optional": false,
            "field": "too_many_requests",
            "description": "<p>The API has received too many requests in a short time. Exponential backoff is suggested for the retrying of the requests.</p>"
          }
        ],
        "Error 5xx Internal Server Error": [
          {
            "group": "Error 5xx Internal Server Error",
            "optional": false,
            "field": "internal_server_error",
            "description": "<p>There was an error during the elaboration of the request.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./app/controllers/api/v1/ConceptsController.java",
    "groupTitle": "Concepts_APIs"
  },
  {
    "type": "get",
    "url": "/concepts/root",
    "title": "Get root concept of the ontology",
    "name": "GetRootConcept",
    "group": "Concepts_APIs",
    "description": "<p>Returns a JSON Object, representing complete information about the root concept in the ontology.<br> The field <code>relations</code> is a JSON Array of objects; each object represents a semantic relation in which the root concept is involved.</p>",
    "parameter": {
      "examples": [
        {
          "title": "Example: Specify language",
          "content": "GET https://api.ontomap.eu/api/v1/concepts/root?lang=en\n{\n  \"uri\" : \"http://ontomap.eu/ontology/SchemaThing\",\n  \"relations\" : [\n    {\n      \"subject\" : \"http://ontomap.eu/ontology/AtomicThing\",\n      \"object\" : \"http://ontomap.eu/ontology/SchemaThing\",\n      \"predicate\" : \"subClassOf\"\n    },\n    {\n      \"subject\" : \"http://ontomap.eu/ontology/Place\",\n      \"object\" : \"http://ontomap.eu/ontology/SchemaThing\",\n      \"predicate\" : \"subClassOf\"\n    },\n    {\n      \"subject\" : \"http://ontomap.eu/ontology/Event\",\n      \"object\" : \"http://ontomap.eu/ontology/SchemaThing\",\n      \"predicate\" : \"subClassOf\"\n    }\n  ],\n  \"label\" : \"SchemaThing\",\n  \"description\" : \"The root concept of the ontology.\"\n}",
          "type": "json"
        }
      ],
      "fields": {
        "Information type": [
          {
            "group": "Information type",
            "type": "String",
            "optional": true,
            "field": "lang",
            "description": "<p>A <a href=\"https://en.wikipedia.org/wiki/ISO_639-1\" target=\"_blank\">ISO 639-1 language code</a>.<br/> If specified, the <code>label</code> field of the JSON Object will be a String containing the label of the root concept in the specified language, if present.<br/> If not set, the <code>label</code> field of the JSON Object will be a JSON Object containing the labels of the root concept in all the available languages.</p>"
          }
        ],
        "Presentation": [
          {
            "group": "Presentation",
            "type": "Boolean",
            "optional": true,
            "field": "prettyprint",
            "defaultValue": "false",
            "description": "<p>If set to <code>true</code>, the JSON string served is indented.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./app/controllers/api/v1/ConceptsController.java",
    "groupTitle": "Concepts_APIs",
    "error": {
      "fields": {
        "Error 401 Unauthorized": [
          {
            "group": "Error 401 Unauthorized",
            "optional": false,
            "field": "invalid_certificate",
            "description": "<p>The client certificate was not provided or it is invalid.</p>"
          }
        ],
        "Error 429 Too Many Requests": [
          {
            "group": "Error 429 Too Many Requests",
            "optional": false,
            "field": "too_many_requests",
            "description": "<p>The API has received too many requests in a short time. Exponential backoff is suggested for the retrying of the requests.</p>"
          }
        ],
        "Error 5xx Internal Server Error": [
          {
            "group": "Error 5xx Internal Server Error",
            "optional": false,
            "field": "internal_server_error",
            "description": "<p>There was an error during the elaboration of the request.</p>"
          }
        ]
      }
    }
  },
  {
    "type": "get",
    "url": "/instances/<concept>",
    "title": "Get concept instances",
    "name": "GetConceptInstances",
    "group": "Instances_APIs",
    "description": "<p>Returns a <a href=\"http://geojson.org/geojson-spec.html#feature-collection-objects\" target=\"_blank\">GeoJSON FeatureCollection</a>, containing a list of <a href=\"http://geojson.org/geojson-spec.html#feature-objects\" target=\"_blank\">Features</a>. <br/> If <code>applications</code> is specified, only the Features from the specified source applications are returned. <br/> Each <a href=\"http://geojson.org/geojson-spec.html#feature-objects\" target=\"_blank\">Feature</a> of the collection represents an instance of the specified concept.<br/> The <code>provenance</code> field of each Feature, if present, is a URL that can be invoked to retrieve the description of the provenance of the Feature, in JSON format.<br/> The <code>application</code> field of each Feature, if present, is a string containing the domain name of the application managing the Feature.<br/> By default, only the labels and direct URLs to the instances are included in the <code>properties</code> field of each Feature.<br/> If <code>count</code> is set to true, returns a JSON object representing, for each selected source application, the number of instances of the selected concept which correspond to the filters specified.</p>",
    "parameter": {
      "examples": [
        {
          "title": "Example: Descriptions, Geometries, Source",
          "content": "GET https://api.ontomap.eu/api/v1/instances/Park?descriptions=true&geometries=true&source=ontomap.eu,app1.wegovnow.eu\n{\n \"type\": \"FeatureCollection\",\n \"features\": [\n {\n   \"type\": \"Feature\",\n   \"geometry\": {\"type\": \"Point\", \"coordinates\": [45.5, 7]},\n   \"id\": \"http://ontomap.eu/ontology/Park/1\",\n   \"application\": \"ontomap.eu\",\n   \"provenance\": \"http://ontomap.eu/ontology/Provenance/1\",\n   \"properties\": {\n     \"external_url\": \"https://api.ontomap.eu/api/v1/instances/Park/1\",\n     \"hasType\": \"Park\",\n     \"hasAddress\": \"Address for Park 1\",\n     \"label\": {\n       \"en\": \"Park 1\",\n       \"it\": \"Parco 1\"\n     }\n   }\n },\n {\n   \"type\": \"Feature\",\n   \"geometry\": {\"type\": \"Point\", \"coordinates\": [45, 7.5]},\n   \"id\": \"https://app1.wegovnow.eu/Parks/5\",\n   \"application\": \"app1.wegovnow.eu\",\n   \"properties\": {\n     \"external_url\": \"https://app1.wegovnow.eu/Parks/5\",\n     \"hasType\": \"Park\",\n     \"hasAddress\": \"Address for Park 5\",\n     \"label\": \"Park 5\"\n   }\n }]\n}",
          "type": "json"
        },
        {
          "title": "Example: Count",
          "content": "GET https://api.ontomap.eu/api/v1/instances/Park?count=true\n{\"count\": 1}",
          "type": "json"
        },
        {
          "title": "Example: Bounding box, language",
          "content": "GET https://api.ontomap.eu/api/v1/instances/Park?lang=en&boundingbox=7.4,45.1,7.6,44.9\n{\n \"type\": \"FeatureCollection\",\n \"features\": [\n {\n   \"type\": \"Feature\",\n   \"geometry\": null,\n   \"id\": \"http://ontomap.eu/ontology/Park/2\",\n   \"application\": \"ontomap.eu\",\n   \"provenance\": \"http://ontomap.eu/ontology/Provenance/1\",\n   \"properties\": {\n     \"external_url\": \"https://api.ontomap.eu/api/v1/instances/Park/2\",\n     \"hasType\": \"Park\"\n     \"label\": \"Park 2\"\n   }\n }]\n}",
          "type": "json"
        },
        {
          "title": "Example: Distance",
          "content": "GET https://api.ontomap.eu/api/v1/instances/Park?distance=7.4,45.1,200\n{\n \"type\": \"FeatureCollection\",\n \"features\": [\n {\n   \"type\": \"Feature\",\n   \"geometry\": null,\n   \"id\": \"http://ontomap.eu/ontology/Park/2\",\n   \"application\": \"ontomap.eu\",\n   \"provenance\": \"http://ontomap.eu/ontology/Provenance/1\",\n   \"properties\": {\n     \"external_url\": \"https://api.ontomap.eu/api/v1/instances/Park/2\",\n     \"hasType\": \"Park\"\n     \"label\": \"Park 2\"\n   }\n }]\n}",
          "type": "json"
        }
      ],
      "fields": {
        "Required parameters": [
          {
            "group": "required_parameters",
            "type": "String",
            "optional": false,
            "field": "concept",
            "description": "<p><strong>Required.</strong> The concept whose instances are requested.</p>"
          }
        ],
        "Information type": [
          {
            "group": "Information type",
            "type": "Boolean",
            "optional": true,
            "field": "count",
            "defaultValue": "false",
            "description": "<p>If set to <code>true</code>, returns the number of instances of the selected concept.</p>"
          },
          {
            "group": "Information type",
            "type": "Boolean",
            "optional": true,
            "field": "descriptions",
            "defaultValue": "false",
            "description": "<p>If set to <code>true</code>, all the properties of the instances are included in the result.</p>"
          },
          {
            "group": "Information type",
            "type": "Boolean",
            "optional": true,
            "field": "geometries",
            "defaultValue": "false",
            "description": "<p>If set to <code>true</code>, the <a href=\"http://geojson.org/geojson-spec.html#geometry-objects\" target=\"_blank\">geometries</a> of the instances (using EPSG:4326 as CRS) are included in the result.</p>"
          },
          {
            "group": "Information type",
            "type": "String",
            "optional": true,
            "field": "lang",
            "description": "<p>A <a href=\"https://en.wikipedia.org/wiki/ISO_639-1\" target=\"_blank\">ISO 639-1 language code</a>.<br/> If specified, the <code>label</code> property of each Feature will be a String containing the label of the corresponding instance in the specified language, if present.<br/> If not set, the <code>label</code> property of each Feature will be a JSON Object containing the labels of the corresponding instance in all the available languages.</p>"
          }
        ],
        "Filters": [
          {
            "group": "Filters",
            "type": "Boolean",
            "optional": true,
            "field": "subconcepts",
            "defaultValue": "false",
            "description": "<p>If set to <code>true</code>, all instances of the specified concept and its subconcepts are returned.</p>"
          },
          {
            "group": "Filters",
            "type": "String",
            "optional": true,
            "field": "boundingbox",
            "description": "<p>A string representing the coordinates of the North-East and South-West points of a bounding box (using EPSG:4326 as CRS).<br/> If specified, only the instances within the bounding box are returned.<br/> String format: <code>NE_lng,NE_lat,SW_lng,SW_lat</code> (see example below).</p>"
          },
          {
            "group": "Filters",
            "type": "String",
            "optional": true,
            "field": "distance",
            "description": "<p>A string representing the center coordinates (using EPSG:4326 as CRS) and radius (in meters) of a circle.<br/> If specified, only the instances within the circle are returned.<br/> String format: <code>lng,lat,distance</code> (see example below).</p>"
          },
          {
            "group": "Filters",
            "type": "String",
            "optional": true,
            "field": "applications",
            "defaultValue": "All applications",
            "description": "<p>A string containing one or more application domain names, separated by commas.<br/> If specified, only the instances managed by the specified applications are returned.<br/></p>"
          },
          {
            "group": "Filters",
            "type": "String",
            "optional": true,
            "field": "validFrom",
            "description": "<p>A <a href=\"https://en.wikipedia.org/wiki/ISO_8601\" target=\"_blank\">ISO 8601 date/time string</a>.<br/> If specified, only the instances created/valid after the specified instant are returned.<br/> Warning: this feature is not yet implemented.</p>"
          },
          {
            "group": "Filters",
            "type": "String",
            "optional": true,
            "field": "validTo",
            "description": "<p>A <a href=\"https://en.wikipedia.org/wiki/ISO_8601\" target=\"_blank\">ISO 8601 date/time string</a>.<br/> If specified, only the instances created/valid before the specified instant are returned.<br/> Warning: this feature is not yet implemented.</p>"
          }
        ],
        "Pagination": [
          {
            "group": "Pagination",
            "type": "Number",
            "size": ">=0",
            "optional": true,
            "field": "page",
            "description": "<p>If specified, the results are divided in pages and the requested page is returned.<br/> The page size is set using the <code>limit</code> parameter (default: 200 instances).</p>"
          },
          {
            "group": "Pagination",
            "type": "Number",
            "size": "1-500",
            "optional": true,
            "field": "limit",
            "defaultValue": "200",
            "description": "<p>Sets the page size. Has effect only if <code>page</code> is set.</p>"
          }
        ],
        "Presentation": [
          {
            "group": "Presentation",
            "type": "Boolean",
            "optional": true,
            "field": "prettyprint",
            "defaultValue": "false",
            "description": "<p>If set to <code>true</code>, the JSON string served is indented.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./app/controllers/api/v1/InstancesController.java",
    "groupTitle": "Instances_APIs",
    "error": {
      "fields": {
        "Error 400 Bad Request": [
          {
            "group": "Error 400 Bad Request",
            "optional": false,
            "field": "missing_parameter",
            "description": "<p>The parameter indicated is missing.</p>"
          },
          {
            "group": "Error 400 Bad Request",
            "optional": false,
            "field": "malformed_parameter",
            "description": "<p>The parameter indicated is malformed.</p>"
          },
          {
            "group": "Error 400 Bad Request",
            "optional": false,
            "field": "missing_concept_mapping",
            "description": "<p>There is no corresponding mapping for the concept indicated.</p>"
          }
        ],
        "Error 401 Unauthorized": [
          {
            "group": "Error 401 Unauthorized",
            "optional": false,
            "field": "invalid_certificate",
            "description": "<p>The client certificate was not provided or it is invalid.</p>"
          }
        ],
        "Error 429 Too Many Requests": [
          {
            "group": "Error 429 Too Many Requests",
            "optional": false,
            "field": "too_many_requests",
            "description": "<p>The API has received too many requests in a short time. Exponential backoff is suggested for the retrying of the requests.</p>"
          }
        ],
        "Error 5xx Internal Server Error": [
          {
            "group": "Error 5xx Internal Server Error",
            "optional": false,
            "field": "internal_server_error",
            "description": "<p>There was an error during the elaboration of the request.</p>"
          }
        ]
      }
    }
  },
  {
    "type": "post",
    "url": "/logger/events",
    "title": "Insert user activity events",
    "name": "InsertEvents",
    "group": "Logger_APIs",
    "description": "<p>Creates and inserts into the logger one or more user activity events.<br/> The request must have a <code>application/json</code> Content-Type and must include the JSON representation of the event(s) in its body (see provided example).<br/> Every request made to this endpoint must include a client certificate signed by LiquidFeedback.<br/> In order to be able to insert events into the logger, every application must insert its mapping rules into the logger (see the <a href=\"#api-Mapping_APIs-InsertMappingRules\">Insert mapping rules API</a>).</p>",
    "header": {
      "fields": {
        "Request Headers": [
          {
            "group": "Request Headers",
            "optional": false,
            "field": "Idempotency-Key",
            "description": "<p>If specified, the request is considered idempotent, so it can be safely retried in case of network errors without saving duplicated events, as long as the request is resubmitted using the same key.<br/> The key is a client-generated random string; good examples of possible keys are <a href=\"https://en.wikipedia.org/wiki/Universally_unique_identifier#Version_4_.28random.29\" target=\"_blank\">V4 UUIDs</a>.<br/> Keys expire after 24 hours.<br/> It is strongly advised to always include an Idempotency Key, so that the events can be resubmitted safely in case of network problems.</p>"
          }
        ]
      }
    },
    "parameter": {
      "examples": [
        {
          "title": "Request body (example)",
          "content": "POST https://api.ontomap.eu/api/v1/logger/events\n{\n  \"event_list\": [\n    {\n      \"actor\": 12345,\n      \"timestamp\": 1485338648883,\n      \"activity_type\": \"object_created\",\n      \"activity_objects\": [\n        {\n          \"type\": \"Feature\",\n          \"geometry\": {\n            \"type\": \"Point\",\n            \"coordinates\": [\n              7.643340826034546,\n              45.07558142970864\n            ]\n          },\n          \"properties\": {\n            \"hasType\": \"Asili\",\n            \"external_url\": \"http://app1.wegovnow.eu/Asili/1\",\n            \"nome\": \"Asilo 1\"\n          }\n        }\n      ],\n      \"references\": [\n        {\n          \"application\": \"app1.wegovnow.eu\",\n          \"external_url\": \"http://app1.wegovnow.eu/Asili/22\"\n        }\n      ],\n      \"visibility_details\": [\n        {\n          \"external_url\": \"http://app1.wegovnow.eu/Asili/56\",\n          \"hidden\": true\n        }\n      ],\n      \"details\": { ... }\n    },\n    { ... }\n  ]\n}",
          "type": "json"
        }
      ],
      "fields": {
        "Required attributes": [
          {
            "group": "Required attributes",
            "type": "Int",
            "optional": false,
            "field": "actor",
            "description": "<p><strong>Required.</strong> The UWUM ID of the user performing the action.</p>"
          },
          {
            "group": "Required attributes",
            "type": "Long",
            "optional": false,
            "field": "timestamp",
            "description": "<p><strong>Required.</strong> The Unix timestamp (in ms) of the action.</p>"
          },
          {
            "group": "Required attributes",
            "type": "String",
            "optional": false,
            "field": "activity_type",
            "description": "<p><strong>Required.</strong> The user activity type.</p>"
          }
        ],
        "Attributes": [
          {
            "group": "Attributes",
            "type": "Array",
            "optional": true,
            "field": "activity_objects",
            "description": "<p>An Array of <a href=\"http://geojson.org/geojson-spec.html#feature-objects\" target=\"_blank\">GeoJSON Features</a>.<br/> The feature properties must include a <code>hasType</code> field (the concept to which the feature belongs) and a <code>external_url</code> field (deep link to the feature). The remaining properties (as well as the feature concept) are expressed in the terminology of the client application, and must be included in the mapping previously sent to the logger by the application.<br/> The properties can contain a <code>additionalProperties</code> object: every field contained in this object will not be translated and stored as-is.<br/> The same event must not contain multiple activity_objects having the same external_url.</p>"
          },
          {
            "group": "Attributes",
            "type": "Array",
            "optional": true,
            "field": "references",
            "description": "<p>An Array of objects.<br/> Every object contains two fields: <code>external_url</code> (String) and <code>application</code> (String), referring to a specific feature managed by a specific application.</p>"
          },
          {
            "group": "Attributes",
            "type": "Array",
            "optional": true,
            "field": "visibility_details",
            "description": "<p>An Array of objects.<br/> Every object contains two fields: <code>external_url</code> (String) and <code>hidden</code> (Boolean).<br/> When the <code>visibility_details</code> field is included, the features corresponding to the <code>external_urls</code> included (that must be managed by the application sending the event) are hidden/shown from the data retrieval results, based on the corresponding <code>hidden</code> value.<br/> The <code>visibility_details</code> field is used when the action to be logged implies a change of visibility for some features (for example, if a feature is deleted or made private/public).<br/> The visibility change of a feature is applied only if the timestamp of the event is greater than or equal to the timestamp of the last logged event containing a visibility change for that feature.</p>"
          },
          {
            "group": "Attributes",
            "type": "Object",
            "optional": true,
            "field": "details",
            "description": "<p>An Object that can include further details regarding the user action to be logged. This field is stored as-is.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 400 Bad Request": [
          {
            "group": "Error 400 Bad Request",
            "optional": false,
            "field": "duplicated_activity_object",
            "description": "<p>An event contains two or more activity_objects with the same external_url.</p>"
          },
          {
            "group": "Error 400 Bad Request",
            "optional": false,
            "field": "malformed_json",
            "description": "<p>The payload sent is not a valid JSON string or it doesn't adhere to the JSON Schema.</p>"
          },
          {
            "group": "Error 400 Bad Request",
            "optional": false,
            "field": "missing_concept_mapping",
            "description": "<p>There is no corresponding mapping for the concept indicated.</p>"
          },
          {
            "group": "Error 400 Bad Request",
            "optional": false,
            "field": "missing_property_mapping",
            "description": "<p>There is no corresponding mapping for the property indicated.</p>"
          }
        ],
        "Error 409 Conflict": [
          {
            "group": "Error 409 Conflict",
            "optional": false,
            "field": "idempotency_conflict",
            "description": "<p>A different request using the same Idempotency Key was sent previously.</p>"
          }
        ],
        "Error 401 Unauthorized": [
          {
            "group": "Error 401 Unauthorized",
            "optional": false,
            "field": "invalid_certificate",
            "description": "<p>The client certificate was not provided or it is invalid.</p>"
          }
        ],
        "Error 415 Unsupported Media Type": [
          {
            "group": "Error 415 Unsupported Media Type",
            "optional": false,
            "field": "wrong_content_type",
            "description": "<p>The request has an unexpected Content-Type.<br/> The Content-Type for this request must be <code>application/json</code>.</p>"
          }
        ],
        "Error 429 Too Many Requests": [
          {
            "group": "Error 429 Too Many Requests",
            "optional": false,
            "field": "too_many_requests",
            "description": "<p>The API has received too many requests in a short time. Exponential backoff is suggested for the retrying of the requests.</p>"
          }
        ],
        "Error 5xx Internal Server Error": [
          {
            "group": "Error 5xx Internal Server Error",
            "optional": false,
            "field": "internal_server_error",
            "description": "<p>There was an error during the elaboration of the request.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./app/controllers/api/v1/LoggerController.java",
    "groupTitle": "Logger_APIs"
  },
  {
    "type": "get",
    "url": "/logger/events",
    "title": "Retrieve user activity events",
    "name": "RetrieveEvents",
    "group": "Logger_APIs",
    "description": "<p>Returns a JSON Object containing a list of all the events in the logger, sorted from the most recent to the least recent.<br/> The events are returned in the same format used for their posting, with the add of an &quot;application&quot; (String) field containing the name of the application that posted the event, extracted from the certificate sent by the application which inserted the event.<br/> It is possible to combine multiple filters in a single request.</p>",
    "parameter": {
      "fields": {
        "Filters": [
          {
            "group": "Filters",
            "type": "Int",
            "optional": true,
            "field": "actor",
            "description": "<p>If specified, returns only the events related to actions performed by the user with the specified UWUM ID.</p>"
          },
          {
            "group": "Filters",
            "type": "String",
            "optional": true,
            "field": "application",
            "description": "<p>If specified, returns only the events related to the specified application.</p>"
          },
          {
            "group": "Filters",
            "type": "String",
            "optional": true,
            "field": "activity_type",
            "description": "<p>If specified, returns only the events related to actions of the specified type.</p>"
          },
          {
            "group": "Filters",
            "type": "String",
            "optional": true,
            "field": "concept",
            "description": "<p>If specified, returns only the events which contain activity_objects belonging to the specified concept (OnToMap terminology).</p>"
          },
          {
            "group": "Filters",
            "type": "String",
            "optional": true,
            "field": "external_url",
            "description": "<p>If specified, returns only the events which contain the activity_object having the specified external_url.</p>"
          },
          {
            "group": "Filters",
            "type": "String",
            "optional": true,
            "field": "reference_concept",
            "description": "<p>If specified, returns only the events which refer to activity_objects belonging to the specified concept (OnToMap terminology).</p>"
          },
          {
            "group": "Filters",
            "type": "String",
            "optional": true,
            "field": "reference_external_url",
            "description": "<p>If specified, returns only the events which refer to the activity_object having the specified external_url.</p>"
          },
          {
            "group": "Filters",
            "type": "Boolean",
            "optional": true,
            "field": "subconcepts",
            "defaultValue": "false",
            "description": "<p>If set to <code>true</code> and <code>concept</code> or <code>reference_concept</code> are specified, returns only the events which contain or refer to activity_objects belonging to the specified concept and its subconcepts. Has effect only if <code>concept</code> or <code>reference_concept</code> are specified.</p>"
          },
          {
            "group": "Filters",
            "type": "Long",
            "optional": true,
            "field": "start_time",
            "description": "<p>If specified, returns only the events having a timestamp greater than or equal to the specified timestamp (Unix timestamp in ms).</p>"
          },
          {
            "group": "Filters",
            "type": "Long",
            "optional": true,
            "field": "end_time",
            "description": "<p>If specified, returns only the events having a timestamp less than or equal to the specified timestamp (Unix timestamp in ms).</p>"
          },
          {
            "group": "Filters",
            "type": "String",
            "optional": true,
            "field": "boundingbox",
            "description": "<p>A string representing the coordinates of the North-East and South-West</p>"
          },
          {
            "group": "Filters",
            "type": "Boolean",
            "optional": true,
            "field": "aggregate",
            "description": "<p>If set to <code>true</code>, the returned list of events will have at most one activity_object. points of a bounding box (using EPSG:4326 as CRS).<br/> If specified, returns only the events including or referring to activity_objects located in the specified bounding box.<br/> String format: <code>NE_lng,NE_lat,SW_lng,SW_lat</code></p>"
          }
        ],
        "Pagination": [
          {
            "group": "Pagination",
            "type": "Number",
            "size": ">=0",
            "optional": true,
            "field": "page",
            "description": "<p>If specified, the results are divided in pages and the requested page is returned.<br/> The page size is set using the <code>limit</code> parameter (default: 200 instances).</p>"
          },
          {
            "group": "Pagination",
            "type": "Number",
            "size": "1-500",
            "optional": true,
            "field": "limit",
            "defaultValue": "200",
            "description": "<p>Sets the page size. Has effect only if <code>page</code> is set.</p>"
          }
        ],
        "Presentation": [
          {
            "group": "Presentation",
            "type": "Boolean",
            "optional": true,
            "field": "prettyprint",
            "defaultValue": "false",
            "description": "<p>If set to <code>true</code>, the JSON string served is indented.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Retrieve events",
          "content": "GET https://api.ontomap.eu/api/v1/logger/events\n{\n  \"event_list\": [\n    {\n      \"application\": \"app1.wegovnow.eu\",\n      \"actor\": 67890,\n      \"timestamp\": 14967425487654,\n      \"activity_type\": \"profile_updated\",\n    },\n    {\n      \"application\": \"app2.wegovnow.eu\",\n      \"actor\": 12345,\n      \"timestamp\": 1485338648883,\n      \"activity_type\": \"object_created\",\n      \"activity_objects\": [\n        {\n          \"type\": \"Feature\",\n          \"geometry\": {\n            \"type\": \"Point\",\n            \"coordinates\": [\n              7.643340826034546,\n              45.07558142970864\n            ]\n          },\n          \"properties\": {\n            \"hasType\": \"Asili\",\n            \"external_url\": \"http://app1.wegovnow.eu/Asili/1\",\n            \"nome\": \"Asilo 1\"\n          }\n        }\n      ],\n      \"references\": [\n        {\n          \"application\": \"app1.wegovnow.eu\",\n          \"external_url\": \"http://app1.wegovnow.eu/Asili/22\"\n        }\n      ],\n      \"visibility_details\": [\n        {\n          \"external_url\": \"http://app1.wegovnow.eu/Asili/56\",\n          \"hidden\": true\n        }\n      ],\n      \"details\": {\n          \"action_id\": \"76b66b1a-d846-4a56-8bfb-5aa5b4a3227f\"\n      }\n    }\n  ]\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./app/controllers/api/v1/LoggerController.java",
    "groupTitle": "Logger_APIs",
    "error": {
      "fields": {
        "Error 400 Bad Request": [
          {
            "group": "Error 400 Bad Request",
            "optional": false,
            "field": "malformed_parameter",
            "description": "<p>The parameter indicated is malformed.</p>"
          }
        ],
        "Error 401 Unauthorized": [
          {
            "group": "Error 401 Unauthorized",
            "optional": false,
            "field": "invalid_certificate",
            "description": "<p>The client certificate was not provided or it is invalid.</p>"
          }
        ],
        "Error 429 Too Many Requests": [
          {
            "group": "Error 429 Too Many Requests",
            "optional": false,
            "field": "too_many_requests",
            "description": "<p>The API has received too many requests in a short time. Exponential backoff is suggested for the retrying of the requests.</p>"
          }
        ],
        "Error 5xx Internal Server Error": [
          {
            "group": "Error 5xx Internal Server Error",
            "optional": false,
            "field": "internal_server_error",
            "description": "<p>There was an error during the elaboration of the request.</p>"
          }
        ]
      }
    }
  },
  {
    "type": "get",
    "url": "/logger/hiddenlist",
    "title": "Retrieve hidden data objects",
    "name": "RetrieveHiddenList",
    "group": "Logger_APIs",
    "description": "<p>Returns a list of all of the data objects (activity_objects) managed from the application making the request which are currently hidden from the <a href=\"#api-Instances_APIs-GetConceptInstances\">data retrieval API</a> results.</p>",
    "parameter": {
      "examples": [
        {
          "title": "Retrieve list of hidden data objects",
          "content": "GET https://api.ontomap.eu/api/v1/logger/hiddenlist\n{\n  \"hidden_list\": [\n    \"http://app1.wegovnow.eu/Asili/1\",\n    \"http://app1.wegovnow.eu/Asili/2\"\n  ]\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./app/controllers/api/v1/LoggerController.java",
    "groupTitle": "Logger_APIs",
    "error": {
      "fields": {
        "Error 401 Unauthorized": [
          {
            "group": "Error 401 Unauthorized",
            "optional": false,
            "field": "invalid_certificate",
            "description": "<p>The client certificate was not provided or it is invalid.</p>"
          }
        ],
        "Error 429 Too Many Requests": [
          {
            "group": "Error 429 Too Many Requests",
            "optional": false,
            "field": "too_many_requests",
            "description": "<p>The API has received too many requests in a short time. Exponential backoff is suggested for the retrying of the requests.</p>"
          }
        ],
        "Error 5xx Internal Server Error": [
          {
            "group": "Error 5xx Internal Server Error",
            "optional": false,
            "field": "internal_server_error",
            "description": "<p>There was an error during the elaboration of the request.</p>"
          }
        ]
      }
    }
  },
  {
    "type": "post",
    "url": "/logger/mappings",
    "title": "Insert mapping rules",
    "name": "InsertMappingRules",
    "group": "Mapping_APIs",
    "description": "<p>Inserts into the logger the mapping rules for translating concepts and properties to the OnToMap terminology.<br/> The request must have a <code>application/json</code> Content-Type and must include the JSON representation of the mapping rules in its body (see provided example).<br/> Every request made to this endpoint must include a client certificate signed by LiquidFeedback.<br/> The mapping rules consist in a list of concept mappings, which associate a concept in the application terminology to one in the OnToMap terminology. For each concept it is possible to include a list of property mappings, each one associating a property in the app terminology to one in the OTM terminology.<br/> All the concept and property mapping must refer to concepts and properties in the OnToMap ontology.<br/> When a property represents a measure, it is possible to include its measure unit, in order to avoid ambiguities.<br/> When new mapping rules are submitted, any previous rules associated to the application performing the request are overwritten.</p>",
    "parameter": {
      "examples": [
        {
          "title": "Insert mapping rules",
          "content": "POST https://api.ontomap.eu/api/v1/logger/mappings\n{\n  \"mappings\": [\n    {\n      \"app_concept\": \"ChildCare\",\n      \"ontomap_concept\": \"Kindergarten\",\n      \"properties\": [\n        {\n          \"app_property\": \"denominazione\",\n          \"ontomap_property\": \"hasName\"\n        },\n        {\n          \"app_property\": \"indirizzo\",\n          \"ontomap_property\": \"hasAddress\"\n        },\n        {\n          \"app_property\": \"telefono\",\n          \"ontomap_property\": \"hasPhoneNumber\"\n        },\n        {\n          \"app_property\": \"retta_mensile\",\n          \"ontomap_property\": \"hasMonthlyRate\",\n          \"unit\": \"EUR\"\n        }\n      ]\n    },\n    {\n      \"app_concept\": \"Ristoranti\",\n      \"ontomap_concept\": \"Restaurant\"\n    }\n  ]\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./app/controllers/api/v1/MappingController.java",
    "groupTitle": "Mapping_APIs",
    "error": {
      "fields": {
        "Error 400 Bad Request": [
          {
            "group": "Error 400 Bad Request",
            "optional": false,
            "field": "malformed_json",
            "description": "<p>The payload sent is not a valid JSON string or it doesn't adhere to the JSON Schema.</p>"
          },
          {
            "group": "Error 400 Bad Request",
            "optional": false,
            "field": "missing_concept_mapping",
            "description": "<p>There is no corresponding mapping for the concept indicated.</p>"
          },
          {
            "group": "Error 400 Bad Request",
            "optional": false,
            "field": "missing_property_mapping",
            "description": "<p>There is no corresponding mapping for the property indicated.</p>"
          }
        ],
        "Error 415 Unsupported Media Type": [
          {
            "group": "Error 415 Unsupported Media Type",
            "optional": false,
            "field": "wrong_content_type",
            "description": "<p>The request has an unexpected Content-Type.<br/> The Content-Type for this request must be <code>application/json</code>.</p>"
          }
        ],
        "Error 401 Unauthorized": [
          {
            "group": "Error 401 Unauthorized",
            "optional": false,
            "field": "invalid_certificate",
            "description": "<p>The client certificate was not provided or it is invalid.</p>"
          }
        ],
        "Error 429 Too Many Requests": [
          {
            "group": "Error 429 Too Many Requests",
            "optional": false,
            "field": "too_many_requests",
            "description": "<p>The API has received too many requests in a short time. Exponential backoff is suggested for the retrying of the requests.</p>"
          }
        ],
        "Error 5xx Internal Server Error": [
          {
            "group": "Error 5xx Internal Server Error",
            "optional": false,
            "field": "internal_server_error",
            "description": "<p>There was an error during the elaboration of the request.</p>"
          }
        ]
      }
    }
  },
  {
    "type": "get",
    "url": "/logger/mappings",
    "title": "Retrieve mapping rules",
    "name": "RetrieveMappingRules",
    "group": "Mapping_APIs",
    "description": "<p>Returns a JSON Object containing a list of all the concept mappings associated with the application performing the request.<br/> Every request made to this endpoint must include a client certificate signed by LiquidFeedback.</p>",
    "parameter": {
      "examples": [
        {
          "title": "Retrieve mapping rules",
          "content": "GET https://api.ontomap.eu/api/v1/logger/mappings\n{\n  \"application\": \"app1.wegovnow.eu\",\n  \"mappings\": [\n    {\n      \"app_concept\": \"ChildCare\",\n      \"ontomap_concept\": \"Kindergarten\",\n      \"properties\": [\n        {\n          \"app_property\": \"denominazione\",\n          \"ontomap_property\": \"hasName\"\n        },\n        {\n          \"app_property\": \"indirizzo\",\n          \"ontomap_property\": \"hasAddress\"\n        },\n        {\n          \"app_property\": \"telefono\",\n          \"ontomap_property\": \"hasPhoneNumber\"\n        },\n        {\n          \"app_property\": \"retta_mensile\",\n          \"ontomap_property\": \"hasMonthlyRate\",\n          \"unit\": \"EUR\"\n        }\n      ]\n    },\n    {\n      \"app_concept\": \"Ristoranti\",\n      \"ontomap_concept\": \"Restaurant\"\n    }\n  ]\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 404 Not Found": [
          {
            "group": "Error 404 Not Found",
            "optional": false,
            "field": "mapping_not_found",
            "description": "<p>There is no mapping associated with the application yet.</p>"
          }
        ],
        "Error 401 Unauthorized": [
          {
            "group": "Error 401 Unauthorized",
            "optional": false,
            "field": "invalid_certificate",
            "description": "<p>The client certificate was not provided or it is invalid.</p>"
          }
        ],
        "Error 429 Too Many Requests": [
          {
            "group": "Error 429 Too Many Requests",
            "optional": false,
            "field": "too_many_requests",
            "description": "<p>The API has received too many requests in a short time. Exponential backoff is suggested for the retrying of the requests.</p>"
          }
        ],
        "Error 5xx Internal Server Error": [
          {
            "group": "Error 5xx Internal Server Error",
            "optional": false,
            "field": "internal_server_error",
            "description": "<p>There was an error during the elaboration of the request.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./app/controllers/api/v1/MappingController.java",
    "groupTitle": "Mapping_APIs"
  },
  {
    "type": "get",
    "url": "/logger/subscriptions",
    "title": "Get current subscriptions",
    "name": "GetSubscriptions",
    "group": "Subscriptions_APIs",
    "description": "<p>Returns a JSON object containing a list of all subscriptions from the requesting application.</p>",
    "version": "0.0.0",
    "filename": "./app/controllers/api/v1/SubscriptionsController.java",
    "groupTitle": "Subscriptions_APIs"
  }
] });
