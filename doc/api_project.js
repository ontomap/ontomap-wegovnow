define({
  "name": "OnToMap",
  "version": "1.0.0",
  "description": "OnToMap API for WeGovNow",
  "title": "OnToMap API",
  "url": "https://api.ontomap.eu/api/v1",
  "template": {
    "withCompare": false,
    "withGenerator": false,
    "forceLanguage": "en"
  },
  "header": {
    "title": "Description and notes",
    "content": "<h2><strong>Description and notes</strong></h2>\n<p>This is the list of APIs currently available for the interaction with OnToMap.\nThe list will be updated with new APIs when they will be implemented.</p>\n<p>Development/testing API endpoint: https://api.ontomap.eu (dev/test Client Certificate)</p>\n<p>Prototype 1 API endpoint: https://p1.api.ontomap.eu (PT1 Client Certificate)</p>\n<p>Prototype 2 API endpoint: https://p2.api.ontomap.eu (PT2 Client Certificate)</p>\n<p>Prototype 3 API endpoint: https://p3.api.ontomap.eu (PT3 Client Certificate)</p>\n<p>Torino Instance API endpoint: https://torino.api.ontomap.eu (Torino instance Client Certificate)</p>\n<p>San Donà Instance API endpoint: https://sandona.api.ontomap.eu (San Donà instance Client Certificate)</p>\n<p><span style=\"color:firebrick;\"><strong>It should be noted that the ontology currently used in OnToMap is under heavy development;\nthis means that you should not rely on the specification of concepts available so far:\nconcepts and relations might be modified in the next future in order to accommodate the types of information to be handled in the WeGovNow pilots.\nAs soon as the ontology will be in a stable enough state, a link to it will be published in this page.</strong></span></p>\n<p>A set of data based mainly on Geo-Data provided by the Cities of Turin and San Donà di Piave and the UK Government is currently used in OnToMap; the concepts available are:</p>\n<ul>\n<li><code>LawEnforcement</code> (Law Enforcement, Police etc.)</li>\n<li><code>Cinema</code> (Cinema/Movie theaters)</li>\n<li><code>PlaceOfWorship</code> (Places of worship, churches etc.)</li>\n<li><code>Drugstore</code> (Drugstores, pharmacies etc.)</li>\n<li><code>School</code> (Schools)</li>\n<li><code>StreetMarket</code> (Street Markets)</li>\n<li><code>UrbanPark</code> (Urban/public parks)</li>\n<li><code>Accommodation</code> (Accommodation facilities, hotels etc.)</li>\n<li><code>Hospital</code> (Hospitals)</li>\n<li><code>WaterAndWasteDisposal</code> (Water distribution and waste disposal activities)</li>\n<li><code>HealthSocialService</code> (Health and social service activities)</li>\n<li><code>ArtGallery</code> (Art galleries)</li>\n<li><code>RealEstateActivity</code> (Real estate activities)</li>\n<li><code>Library</code> (Libraries)</li>\n<li><code>Cafe</code> (Cafés/Bars)</li>\n<li><code>Logistics</code> (Logistics activities)</li>\n<li><code>Store</code> (Stores)</li>\n<li><code>Leisure</code> (Artistic, sport, entertainment activities)</li>\n<li><code>Restaurant</code> (Restaurants)</li>\n<li><code>Museum</code> (Museums)</li>\n<li><code>FinancialService</code> (Financial/insurance activities)</li>\n<li><code>Monument</code> (Monuments)</li>\n<li><code>InformationCommunicationService</code> (Information/communication/advertising/etc. activities)</li>\n<li><code>PowerDistribution</code> (Electricity/gas/steam/etc. distribution activities)</li>\n<li><code>ProfessionalScientificTechnicalActivity</code> (Professional, technical, scientific activities/laboratories)</li>\n<li><code>Club</code> (Private clubs)</li>\n<li><code>CulturalCenter</code> (Cultural centers)</li>\n<li><code>FoodAndAccommodation</code> (Activities regarding food production/distribution and accommodation)</li>\n<li><code>Manufacturing</code> (Factiories, manufacturing activities)</li>\n<li><code>Business</code> (Business activities)</li>\n<li><code>BicyclePath</code> (Bicycle paths)</li>\n<li><code>ParkingLot</code> (Parking lots)</li>\n<li><code>TrainStation</code> (Train stations)</li>\n<li><code>HistoricalCenter</code> (Historical centers)</li>\n<li><code>Highway</code> (Highways)</li>\n</ul>\n"
  },
  "sampleUrl": false,
  "defaultVersion": "0.0.0",
  "apidoc": "0.3.0",
  "generator": {
    "name": "apidoc",
    "time": "2018-07-18T10:45:03.604Z",
    "url": "http://apidocjs.com",
    "version": "0.17.6"
  }
});
