name := "ontomap_backend"

version := "1.0"

lazy val `ontomap_backend` = (project in file(".")).enablePlugins(PlayJava)

scalaVersion := "2.11.7"

libraryDependencies ++= Seq( javaJdbc , cache , javaWs )

unmanagedResourceDirectories in Test <+=  baseDirectory ( _ /"target/web/public/test" )  

resolvers += "scalaz-bintray" at "https://dl.bintray.com/scalaz/releases"

routesGenerator := InjectedRoutesGenerator

libraryDependencies += filters

libraryDependencies ++= Seq(
  "org.apache.jena" % "apache-jena-libs" % "3.4.0" pomOnly(),
  "com.esri.geometry" % "esri-geometry-api" % "1.2.1",
  "com.github.fge" % "json-schema-validator" % "2.2.6",
  "org.mongodb" % "mongo-java-driver" % "3.8.1",
  "com.github.kevinsawicki" % "http-request" % "6.0",
  "org.jsoup" % "jsoup" % "1.11.1"

)
